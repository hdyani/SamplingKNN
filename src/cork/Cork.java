package cork;


//import java.io.IOException;
//import io.IO;


import algo.Algo;
import parameters.Parameters;
import parameters.ParametersStats;
import util.KNNGraph;

public final class Cork implements Algo {
	
	private final Algo algo1;
	private final Algo algo2;
	
//	private final ParametersCork parameters;

	public Cork(Algo algo1, Algo algo2) {
		this.algo1 = algo1;
		this.algo2 = algo2;
	}
	

	@Override
	public KNNGraph doKNN() {
		KNNGraph anng = algo1.doKNN();
//		try {
//			IO.KNNGraphtoJSON(anng, "./Files/results/", "knngaux.data");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		KNNGraph knng = algo2.doKNN(anng);
		
		return knng;
	}
	
	@Override
	public KNNGraph doKNN(int[] users) {
		KNNGraph anng = algo1.doKNN(users);
		KNNGraph knng = algo2.doKNN(anng);
		
		return knng;
	}

	@Override
	public KNNGraph doKNN(KNNGraph anng) {
		KNNGraph anng1 = algo1.doKNN(anng);
		KNNGraph knng = algo2.doKNN(anng1);
		return knng;
	}

//	@Override
//	public void SetParameters(Parameters params) {
//		this.parameters = (ParametersCork) params;
//	}

	@Override
	public Parameters GetParameters() {
		return null;
	}


	@Override
	public ParametersStats GetStatsParameters() {
		return null;
	}
}
