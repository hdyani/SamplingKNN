package measures;

import dataset.Dataset;
import util.Counter;

public class NumberCompZeroRunnable implements Runnable{

	Dataset dataset;
	int[] users;
	Counter counter;
	Counter counter_comp;
	double min_sim;
	
	public NumberCompZeroRunnable(Dataset dataset, int[] users, Counter counter, Counter counter_comp, double min_sim) {
		this.dataset = dataset;
		this.users = users;
		this.counter = counter;
		this.counter_comp = counter_comp;
		this.min_sim = min_sim;
	}
	
	@Override
	public void run() {
//		for(int test = 0 ; test < nb_test; test++) {
//			dataset.sim(users1[test], users2[test]);
//		}
//		System.out.println(users.length + " " + dataset.getUsers().size());
		for(int user1: users) {
			for(int user2: dataset.getUsers()) {
				if (user1 < user2) {
					double sim = dataset.sim(user1, user2);
					counter_comp.inc();
					if (sim <= min_sim) {
						counter.inc();
					}
				}
			}
		}
	}

}
