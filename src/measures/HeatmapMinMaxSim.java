package measures;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import dataset.Dataset;

public final class HeatmapMinMaxSim {

	public final static void heatmap(Dataset dataset1, String add, String fileName, boolean b) throws IOException {
		heatmap(dataset1, 1000, 1000, add, fileName, b);
	}

	public final static void heatmap(Dataset dataset1, int xsize, int ysize, String add, String fileName, boolean b) throws IOException {

		long[][] heatmap = new long[ysize][xsize];
		for(int i = 0; i < xsize; i++) {
			for(int j = 0; j < ysize; j++) {
				heatmap[i][j] = 0;
			}
		}


		int nb_sim = 0;
		int nb_sim_in0101 = 0;

		for(int user : dataset1.getUsers()) {
			for(int user2: dataset1.getUsers()) {
				//				if(user != user2) {
				if(user < user2) {
					double score1 = dataset1.sim(user, user2);
					nb_sim++;
					int p1 = dataset1.getRatedItems(user).size();
					int p2 = dataset1.getRatedItems(user2).size();
					int min = 0;
					int max = 0;
					if (p1 <= p2) {
						min = p1;
						max = p2;
					}
					else {
						min = p2;
						max = p1;
					}

					double score2 = 0;
					if (max !=0) {
						score2 = ((double) min)/((double) max);
					}
					
					if(score1 < 0.1 && score2<0.1) {
						nb_sim_in0101++;
					}
					
					if(score1 > score2) {
						System.out.println(user + " " + user2 + " " + score1 + " " + score2);
					}

					int x = (int) (score1 * (xsize-1));
					int y = (int) (score2 * (ysize-1));
					if(x > y) {
						System.out.println("error2" + user + " " + user2 + " " + score1 + " " + score2 + " " + x + " " + y);
					}

					heatmap[x][y] = heatmap[x][y] + 1;
				}
			}
		}

		System.out.println(nb_sim_in0101 + " out of " + nb_sim + " : " + (((double) (nb_sim_in0101 * 100))/((double) nb_sim)) + "%");
		
		if(b) {
			File file = new File(add);
			file.mkdirs();
			BufferedWriter mbrWriterOutput = new BufferedWriter(new FileWriter(add+fileName));
			boolean first_line = true;
			for(int i = 0; i < xsize; i++) {
				if (!first_line) {
					mbrWriterOutput.newLine();
				}
				else {
					first_line = false;
				}

				mbrWriterOutput.write("(");
				boolean first_column = true;
				for (int j = 0; j < ysize; j++) {
					if (!first_column) {
						mbrWriterOutput.write(",");
					}
					else {
						first_column = false;
					}
					mbrWriterOutput.write(Long.toString(heatmap[i][j]));
				}
				mbrWriterOutput.write(")");
			}
			mbrWriterOutput.close();
		}
	}
}
