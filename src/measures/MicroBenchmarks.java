package measures;

import java.io.IOException;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import dataset.Dataset;
import io.IO;

public final class MicroBenchmarks {
	
	public final static void density(Dataset dataset, String Add, String fileName) throws IOException {
		int nb_user = dataset.getUsers().size();
		
		int nb_ratings = 0;
		
		HashSet<Integer> items = new HashSet<Integer>();
		
		for(int user: dataset.getUsers()) {
			Set<Integer> items_user = dataset.getRatedItems(user);
			nb_ratings = nb_ratings + items_user.size();
			items.addAll(items_user);
		}

		int nb_items = items.size();
		int nb_max_ratings = nb_user * nb_items;
		String s = "{\n\"users\": " + Integer.toString(nb_user) + ",\n" + 
		"\"items\": " + Integer.toString(nb_items) + ",\n" +
		"\"ratings\": " + Integer.toString(nb_ratings) + ",\n" +
		"\"density\": " + Double.toString(((double) nb_ratings)/((double) nb_max_ratings)) +
		"}";
		IO.toFile(s, Add, fileName);
	}

	
	public final static void benchmarkTimeSim(Dataset dataset1, Dataset dataset2, int nb_test, int nb_times, String Add, String fileName) throws IOException {
		long time1 = 0;
		long time2 = 0;

		Object[] users_object = dataset1.getUsers().toArray();
		int nb_users = users_object.length;
		int[] users = new int[nb_users];
		for(int i = 0; i < nb_users; i++) {
			users[i] = (int) users_object[i];
		}
		
		int[] users1 = new int[nb_test];
		int[] users2 = new int[nb_test];
		
	    Random randomGenerator = new Random();
		for(int i = 0; i < nb_times; i++) {
			for(int test = 0 ; test < nb_test; test++) {
				users1[test] = users[randomGenerator.nextInt(nb_users)];
				users2[test] = users[randomGenerator.nextInt(nb_users)];
			}
			long time_aux1 = testDatasetTime(dataset1, users1, users2, nb_test);
			long time_aux2 = testDatasetTime(dataset2, users1, users2, nb_test);
//			System.out.println("Dataset1 done in: " + time_aux1 + "ms");
//			System.out.println("Dataset2 done in: " + time_aux2 + "ms");
			time1 = time1 + time_aux1;
			time2 = time2 + time_aux2;
		}
		
		System.out.println(time1);
		System.out.println(time2);
		double ratio = ((double) time1) / ((double) time2);
		String s = Double.toString(ratio);
		IO.toFile(s, Add, fileName);
	}
	public static long testDatasetTime(Dataset dataset, int[] users1, int[] users2, int nb_test) {
//		long time = 0;
		
		long startdt1 = System.currentTimeMillis();
		for(int test = 0 ; test < nb_test; test++) {
			dataset.sim(users1[test], users2[test]);
		}
		long enddt1 = System.currentTimeMillis();
		return (enddt1-startdt1);
//		time = time + (enddt1-startdt1);
//		return time;
	}
	
	
	public static long testDatasetTimePara(Dataset dataset, int[] users1, int[] users2, int nb_test, int nb_thread) {

		Thread[] threads = new Thread[nb_thread];
		
		
		for (int i=0; i<nb_thread; i++) {
//			int[] users1_aux = util.ThreadRepartition.pivot_repartition(users1, nb_thread, i);
			int[] users2_aux = util.ThreadRepartition.pivot_repartition(users2, nb_thread, i);
			threads[i] = new Thread(new MicroBenchmarksRunnable(dataset,users1,users2_aux));
		}
		
		long startdt1 = System.currentTimeMillis();
		for (int i=0; i<nb_thread; i++) {
			threads[i].start();
		}
		for (int i=0; i<nb_thread; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		long enddt1 = System.currentTimeMillis();
		return (enddt1-startdt1);
	}
}
