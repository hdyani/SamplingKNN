package measures;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import dataset.Dataset;

public final class CCDF {
	
	public final static void generateCCDF(Dataset dataset, String Add, String outputFile) throws IOException {
		int nb_users_tot = dataset.getUsers().size();
		HashMap<Integer, Integer> nb_pos_ratings_map = new HashMap<Integer, Integer>();
		
		int max_ratings = 0;
		int min_ratings = 0;
		for(int user: dataset.getUsers()) {
			min_ratings = dataset.getRatedItems(user).size();
			break;
		}
		for(int user: dataset.getUsers()) {
			int nb_ratings = 0;
			for(int item:dataset.getRatedItems(user)) {
				if (dataset.getRating(user, item) > dataset.getInitValue()) {
					nb_ratings++;
				}
			}
			nb_pos_ratings_map.put(user, nb_ratings);
			if (max_ratings < nb_ratings) {
				max_ratings = nb_ratings;
			}
			if (min_ratings > nb_ratings) {
				min_ratings = nb_ratings;
			}
		}

		File file = new File(Add);
		file.mkdirs();
		BufferedWriter mbrWriter = new BufferedWriter(new FileWriter(Add + outputFile));
		boolean first_lineTrainingSet = true;
		int nb_users_aux = -1;
		
		for(int nb = min_ratings-1; nb<max_ratings+1; nb++) {
			int nb_users = 0;
			for(int user: dataset.getUsers()) {
				int nb_ratings = nb_pos_ratings_map.get(user);
				if (nb_ratings <= nb) {
					nb_users++;
				}
			}
			
			if(nb_users_aux != nb_users) {
				double nb_users_percent = (((double) (nb_users * 100))/((double) nb_users_tot));
				double nb_users_percent_comp = 100 - nb_users_percent;
				double nb_users_ratio_comp = nb_users_percent_comp/(100);

				String ligne = nb + " " + nb_users + " " + nb_users_percent + " " + nb_users_percent_comp + " " + nb_users_ratio_comp;
				if (first_lineTrainingSet){mbrWriter.write(ligne); first_lineTrainingSet = false;}
				else{mbrWriter.write("\n"+ligne);}
				nb_users_aux = nb_users;
			}
		}
		
		
		mbrWriter.close();
	}
}
