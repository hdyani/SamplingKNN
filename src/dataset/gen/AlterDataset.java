package dataset.gen;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import dataset.Dataset;
import dataset.DatasetHM;

public class AlterDataset {
	
	/**
	 * Remove a ratio of items of a given dataset (from files u.data, TestSet0.data... TestSet4.data)
	 * @param input_add add of the input files u.data...
	 * @param output_add where to write the new dataset
	 * @param ratio of items kept: 1 means that we keep all items, 0 none.
	 * @throws IOException
	 */
	public static void removeItems(String input_add, String output_add, double ratio) throws IOException {
		String input = input_add + "u.data";
		String[] files = {"u.data","TestSet0.data","TestSet1.data","TestSet2.data","TestSet3.data","TestSet4.data"};
		Dataset dataset = new DatasetHM(input,0.);
		Random random = new Random();

		Set<Integer> total_items = new HashSet<Integer>();
		Set<Integer> removed_items = new HashSet<Integer>();
		Set<Integer> kept_items = new HashSet<Integer>();
		
		for(int user: dataset.getUsers()) {
			for(int item: dataset.getRatedItems(user)) {
				if (!total_items.contains(item)) {
					total_items.add(item);
					if(random.nextDouble() < ratio) {
						kept_items.add(item);
					}
					else {
						removed_items.add(item);
					}
				}
			}
		}
		
		String fileName;
		InputStream ips;
		InputStreamReader ipsr;
		BufferedReader io1;
		File file;
		BufferedWriter mbrWriterOutput;
		String ligne;
		String[] rating_results;
		int item;
		boolean first_line;
		for(String file_name : files) {
			fileName = input_add+file_name;
			ips = new FileInputStream(fileName);
			ipsr = new InputStreamReader(ips);
			io1 = new BufferedReader(ipsr);
			
			file = new File(output_add);
			file.mkdirs();
			mbrWriterOutput = new BufferedWriter(new FileWriter(output_add+file_name));
			first_line = true;
			while ((ligne=io1.readLine())!=null){
				rating_results = ligne.split(util.Names.datasetSplit);
				item = Integer.parseInt(rating_results[1]);
				if(kept_items.contains(item)) {
					if(first_line) {
						first_line = false;
					}
					else {
						mbrWriterOutput.write("\n");
					}
					mbrWriterOutput.write(ligne);
				}
			}
			mbrWriterOutput.close();
			io1.close();
		}
	}
	

	/**
	 * Melt a ratio of items of a given dataset (from files u.data, TestSet0.data... TestSet4.data)
	 * If two items i1 and i2 are "melt", only i1 will subsist and all the ratings associated to i2 will be associated to i1 instead.
	 * @param input_add add of the input files u.data...
	 * @param output_add where to write the new dataset
	 * @param ratio of items kept: 1 means that we keep all items, 0 none.
	 * @throws IOException
	 */
	public static void meltItems(String input_add, String output_add, double ratio) throws IOException {
		String input = input_add + "u.data";
		String[] files = {"u.data","TestSet0.data","TestSet1.data","TestSet2.data","TestSet3.data","TestSet4.data"};
		Dataset dataset = new DatasetHM(input,0.);
		Random random = new Random();
		
		Map<Integer,Integer> melting_map = new HashMap<Integer,Integer>();
		int aux_item;
		boolean b;
		int[] items;
		int index;
		for(int user: dataset.getUsers()) {
			for(int item: dataset.getRatedItems(user)) {
				melting_map.put(item, item); //at first, all items are associated to themselves
			}
			items = new int[melting_map.keySet().size()];
			index = 0;
			for(int item: dataset.getRatedItems(user)) {
				items[index] = item;
				index++;
			}
			for(int item: dataset.getRatedItems(user)) {
				if(!(random.nextDouble() < ratio)) {
					b = true;
					aux_item = items[Math.abs(random.nextInt())%items.length];
					while(b) {
						while (aux_item == item) {
							aux_item = items[Math.abs(Math.abs(random.nextInt()) % items.length)% items.length];
						}
						while (melting_map.get(aux_item)!= aux_item) {
							aux_item = melting_map.get(aux_item);
						}
						b = aux_item == item;
					}
					melting_map.put(item, aux_item);
				}
			}
		}
		
		String fileName;
		InputStream ips;
		InputStreamReader ipsr;
		BufferedReader io1;
		File file;
		BufferedWriter mbrWriterOutput;
		String ligne;
		String[] rating_results;
		int user;
		int item;
		double rating;
		boolean first_line;
		for(String file_name : files) {
			fileName = input_add+file_name;
			ips = new FileInputStream(fileName);
			ipsr = new InputStreamReader(ips);
			io1 = new BufferedReader(ipsr);
			
			file = new File(output_add);
			file.mkdirs();
			mbrWriterOutput = new BufferedWriter(new FileWriter(output_add+file_name));
			first_line = true;
			while ((ligne=io1.readLine())!=null){
				rating_results = ligne.split(util.Names.datasetSplit);
				user = Integer.parseInt(rating_results[0]);
				item = Integer.parseInt(rating_results[1]);
				rating = (Double.parseDouble(rating_results[2]));
				if(first_line) {
					first_line = false;
				}
				else {
					mbrWriterOutput.write("\n");
				}
				aux_item = melting_map.get(item);
				while (melting_map.get(aux_item)!= aux_item) {
					aux_item = melting_map.get(aux_item);
				}
				mbrWriterOutput.write(user + util.Names.datasetSplit + aux_item + util.Names.datasetSplit + rating);
//				mbrWriterOutput.write(user + util.Names.datasetSplit + melting_map.get(item) + util.Names.datasetSplit + rating);				
			}
			mbrWriterOutput.close();
			io1.close();
		}
	}
	

	/**
	 * Melt a ratio of items of a given dataset (from files u.data, TestSet0.data... TestSet4.data)
	 * If two items i1 and i2 are "melt", only i1 will subsist and all the ratings associated to i2 will be associated to i1 instead.
	 * @param input_add add of the input files u.data...
	 * @param output_add where to write the new dataset
	 * @param ratio of items kept: 1 means that we keep all items, 0 none.
	 * @throws IOException
	 */
	public static void meltItems_(String input_add, String output_add, double ratio) throws IOException {
		String input = input_add + "u.data";
		String[] files = {"u.data","TestSet0.data","TestSet1.data","TestSet2.data","TestSet3.data","TestSet4.data"};
		Dataset dataset = new DatasetHM(input,0.);
		Random random = new Random();
		
		Map<Integer,Integer> melting_map = new HashMap<Integer,Integer>();
//		int[] new_items;
		int aux_item;
		boolean b;
		int[] items;
		int index;
		for(int user: dataset.getUsers()) {
			for(int item: dataset.getRatedItems(user)) {
				melting_map.put(item, item); //at first, all items are associated to themselves
			}
		}
		items = new int[melting_map.keySet().size()];
		index = 0;
		for(int item: melting_map.keySet()) {
			items[index] = item;
			index++;
		}
		for(int item: melting_map.keySet()) {
			if(!(random.nextDouble() < ratio)) {
				b = true;
				aux_item = items[Math.abs(random.nextInt())%items.length];
				while(b) {
					while (aux_item == item) {
						aux_item = items[Math.abs(Math.abs(random.nextInt()) % items.length)% items.length];
					}
					while (melting_map.get(aux_item)!= aux_item) {
						aux_item = melting_map.get(aux_item);
					}
					b = aux_item == item;
				}
				melting_map.put(item, aux_item);
			}
		}
		
		String fileName;
		InputStream ips;
		InputStreamReader ipsr;
		BufferedReader io1;
		File file;
		BufferedWriter mbrWriterOutput;
		String ligne;
		String[] rating_results;
		int user;
		int item;
		double rating;
		boolean first_line;
		for(String file_name : files) {
			fileName = input_add+file_name;
			ips = new FileInputStream(fileName);
			ipsr = new InputStreamReader(ips);
			io1 = new BufferedReader(ipsr);
			
			file = new File(output_add);
			file.mkdirs();
			mbrWriterOutput = new BufferedWriter(new FileWriter(output_add+file_name));
			first_line = true;
			while ((ligne=io1.readLine())!=null){
				rating_results = ligne.split(util.Names.datasetSplit);
				user = Integer.parseInt(rating_results[0]);
				item = Integer.parseInt(rating_results[1]);
				rating = (Double.parseDouble(rating_results[2]));
				if(first_line) {
					first_line = false;
				}
				else {
					mbrWriterOutput.write("\n");
				}
				aux_item = melting_map.get(item);
				while (melting_map.get(aux_item)!= aux_item) {
					aux_item = melting_map.get(aux_item);
				}
				mbrWriterOutput.write(user + util.Names.datasetSplit + aux_item + util.Names.datasetSplit + rating);
//				mbrWriterOutput.write(user + util.Names.datasetSplit + melting_map.get(item) + util.Names.datasetSplit + rating);				
			}
			mbrWriterOutput.close();
			io1.close();
		}
	}
	
	
	
	
	
	
	
	/**
	 * Remove a ratio of users of a given dataset (from files u.data, TestSet0.data... TestSet4.data)
	 * @param input_add add of the input files u.data...
	 * @param output_add where to write the new dataset
	 * @param ratio of users kept: 1 means that we keep all the users, 0 none.
	 * @throws IOException
	 */
	public static void removeUsers(String input_add, String output_add, double ratio) throws IOException {
		String input = input_add + "u.data";
		String[] files = {"u.data","TestSet0.data","TestSet1.data","TestSet2.data","TestSet3.data","TestSet4.data"};
		Dataset dataset = new DatasetHM(input,0.);
		Random random = new Random();

		Set<Integer> total_users = new HashSet<Integer>();
		Set<Integer> removed_users = new HashSet<Integer>();
		Set<Integer> kept_users = new HashSet<Integer>();
		
		for(int user: dataset.getUsers()) {
			if (!total_users.contains(user)) {
				total_users.add(user);
				if(random.nextDouble() < ratio) {
					kept_users.add(user);
				}
				else	 {
					removed_users.add(user);
				}
			}
		}
		
		String fileName;
		InputStream ips;
		InputStreamReader ipsr;
		BufferedReader io1;
		File file;
		BufferedWriter mbrWriterOutput;
		String ligne;
		String[] rating_results;
		int user;
		boolean first_line;
		for(String file_name : files) {
			fileName = input_add+file_name;
			ips = new FileInputStream(fileName);
			ipsr = new InputStreamReader(ips);
			io1 = new BufferedReader(ipsr);
			
			file = new File(output_add);
			file.mkdirs();
			mbrWriterOutput = new BufferedWriter(new FileWriter(output_add+file_name));
			first_line = true;
			while ((ligne=io1.readLine())!=null){
				rating_results = ligne.split(util.Names.datasetSplit);
				user = Integer.parseInt(rating_results[0]);
				if(kept_users.contains(user)) {
					if(first_line) {
						first_line = false;
					}
					else {
						mbrWriterOutput.write("\n");
					}
					mbrWriterOutput.write(ligne);
				}
			}
			mbrWriterOutput.close();
			io1.close();
		}
	}
	

	/**
	 * Melt a ratio of users of a given dataset (from files u.data, TestSet0.data... TestSet4.data)
	 * If two users u1 and u2 are "melt", only u1 will subsist and all the ratings associated to u2 will be associated to u1 instead.
	 * @param input_add add of the input files u.data...
	 * @param output_add where to write the new dataset
	 * @param ratio of users kept: 1 means that we keep all the users, 0 none.
	 * @throws IOException
	 */
	public static void meltUsers(String input_add, String output_add, double ratio) throws IOException {
		String input = input_add + "u.data";
		String[] files = {"u.data","TestSet0.data","TestSet1.data","TestSet2.data","TestSet3.data","TestSet4.data"};
		Dataset dataset = new DatasetHM(input,0.);
		Random random = new Random();
		
		Map<Integer,Integer> melting_map = new HashMap<Integer,Integer>();
		int aux_user;
		boolean b;
		int[] users;
		int index;
		for(int user: dataset.getUsers()) {
			melting_map.put(user, user); //at first, all users are associated to themselves
		}
		users = new int[melting_map.keySet().size()];
		index = 0;
		for(int user: dataset.getUsers()) {
			users[index] = user;
			index++;
		}
		for(int user: dataset.getUsers()) {
			if(!(random.nextDouble() < ratio)) {
				b = true;
				aux_user = users[Math.abs(random.nextInt())%users.length];
				while(b) {
					while (aux_user == user) {
						aux_user = users[Math.abs(random.nextInt())%users.length];
						}
					while (melting_map.get(aux_user)!= aux_user) {
						aux_user = melting_map.get(aux_user);
					}
					b = aux_user == user;
				}
				melting_map.put(user, aux_user);
			}
		}
		
		String fileName;
		InputStream ips;
		InputStreamReader ipsr;
		BufferedReader io1;
		File file;
		BufferedWriter mbrWriterOutput;
		String ligne;
		String[] rating_results;
		int user;
		int item;
		double rating;
		boolean first_line;
		for(String file_name : files) {
			fileName = input_add+file_name;
			ips = new FileInputStream(fileName);
			ipsr = new InputStreamReader(ips);
			io1 = new BufferedReader(ipsr);
			
			file = new File(output_add);
			file.mkdirs();
			mbrWriterOutput = new BufferedWriter(new FileWriter(output_add+file_name));
			first_line = true;
			while ((ligne=io1.readLine())!=null){
				rating_results = ligne.split(util.Names.datasetSplit);
				user = Integer.parseInt(rating_results[0]);
				item = Integer.parseInt(rating_results[1]);
				rating = (Double.parseDouble(rating_results[2]));
				if(first_line) {
					first_line = false;
				}
				else {
					mbrWriterOutput.write("\n");
				}

				aux_user = melting_map.get(user);
				while (melting_map.get(aux_user)!= aux_user) {
					aux_user = melting_map.get(aux_user);
				}
				
				mbrWriterOutput.write(aux_user + util.Names.datasetSplit + item + util.Names.datasetSplit + rating);
//				mbrWriterOutput.write(melting_map.get(user) + util.Names.datasetSplit + item + util.Names.datasetSplit + rating);		
			}
			mbrWriterOutput.close();
			io1.close();
		}
	}
	
	
	
	

	
	/**
	 * Remove a ratio of ratings of a given dataset (from files u.data, TestSet0.data... TestSet4.data)
	 * @param input_add add of the input files u.data...
	 * @param output_add where to write the new dataset
	 * @param ratio of users kept: 1 means that we keep all the ratings, 0 none.
	 * @throws IOException
	 */
	public static void removeRatings(String input_add, String output_add, double ratio) throws IOException {
		String[] files = {"u.data","TestSet0.data","TestSet1.data","TestSet2.data","TestSet3.data","TestSet4.data"};
		Random random = new Random();

		
		String fileName;
		InputStream ips;
		InputStreamReader ipsr;
		BufferedReader io1;
		File file;
		BufferedWriter mbrWriterOutput;
		String ligne;
		boolean first_line;
		for(String file_name : files) {
			fileName = input_add+file_name;
			ips = new FileInputStream(fileName);
			ipsr = new InputStreamReader(ips);
			io1 = new BufferedReader(ipsr);
			
			file = new File(output_add);
			file.mkdirs();
			mbrWriterOutput = new BufferedWriter(new FileWriter(output_add+file_name));
			first_line = true;
			while ((ligne=io1.readLine())!=null){
				if(random.nextDouble() < ratio) {
					if(first_line) {
						first_line = false;
					}
					else {
						mbrWriterOutput.write("\n");
					}
					mbrWriterOutput.write(ligne);
				}
			}
			mbrWriterOutput.close();
			io1.close();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static void printDataset(Dataset dataset, String Add, String fileName) throws IOException {
		File file = new File(Add);
		file.mkdirs();
		BufferedWriter mbrWriterOutput = new BufferedWriter(new FileWriter(Add+fileName));
		double rating;
		String line;
		boolean first_line = true;
		for (int user: dataset.getUsers()) {
			for (int item: dataset.getRatedItems(user)) {
				rating = dataset.getRating(user, item);
				line = user + " " + item + " " + rating;
				if(first_line) {
					first_line = false;
				}
				else {
					line = "\n" + line ;
				}
				mbrWriterOutput.write(line);
			}
		}
		mbrWriterOutput.close();
	}
	
}
