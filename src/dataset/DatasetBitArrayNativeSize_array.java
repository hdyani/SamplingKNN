package dataset;

//import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class DatasetBitArrayNativeSize_array implements Dataset {

	private final int size_sketchs;
	
//	private final Map<Integer,Integer> indexes;
	private final int[] indexes;
	
	private final long[][] dataset;
//	private final Map<Integer,long[]> dataset;
	
//	private final Set<Integer> users;
	
	private final double initValue;
	

	public String structure() {
		return util.Names.BitArrayArray;
	}
	
	public int get_id(int user) {
//		return indexes.get(user);
		return indexes[user];
	}
	
	
	public Dataset subDataset(int[] users) {
		return null;
	}
	
	public DatasetBitArrayNativeSize_array(DatasetBitArrayNativeSize bigdataset) {
		initValue = bigdataset.getInitValue();
		size_sketchs = bigdataset.get_size_sketchs();
//		indexes = new HashMap<Integer,Integer>(bigdataset.getUsers().size());
		indexes = new int[bigdataset.getUsers().size()];
		dataset = new long[bigdataset.getUsers().size()][((int) Math.ceil(((double) size_sketchs)/((double)64))) + 1];
//		users = new HashSet<Integer>();
		int index=0;
		for (int user: bigdataset.get_dataset().keySet()) {
//			indexes.put(user, index);
			indexes[index] = user;
			dataset[index] = bigdataset.get_dataset().get(user);
//			users.add(index);
			index++;
		}
	}
	
	
	

	@Override
	public double getRating(int user, int item) {
		boolean b = dataset.length>user;
		if(b) {
			int hash = util.HashFunctions.hash(item, size_sketchs);
			int index = (hash / 64) + 1;
			long[] sketch = dataset[user];
			b = b && ((sketch[index] & (((long) 1) << (hash % 64))) != 0);
		}
		if(b) {
			return 1;
		}
		return 0;
	}

	@Override
	public boolean hasRated(int user, int item) {
		boolean b = dataset.length > user;
		if(b) {
			int hash = util.HashFunctions.hash(item, size_sketchs);
			int index = (hash / 64)+1;
			long[] sketch = dataset[user];
			b = b && ((sketch[index] & (((long) 1) << (hash % 64))) != 0);
		}
		return b;
	}

	@Override
	public double sim(int user1, int user2) {

		
		long[] bitset1 = dataset[user1];
		long[] bitset2 = dataset[user2];
		int intersize = getIntersectionSize(bitset1,bitset2);
		

//		if(user1 == 1 && user2 == 456) {
//			System.out.println(user1);
//			System.out.println(user2);
//			System.out.println(((double) intersize) / ((double) (bitset1[0] + bitset2[0] - intersize)));
//		}
		
		return (((double) intersize) / ((double) (bitset1[0] + bitset2[0] - intersize)));
//		return (jaccard(dataset.get(user1),dataset.get(user2)));
	}
	
	
	@Override
	public Set<Integer> getUsers() {
//		return indexes.keySet();
		HashSet<Integer> users = new HashSet<Integer>();
		for(int i = 0; i<dataset.length; i++) {
			users.add(i);
		}
		return users;
	}

	@Override
	public Set<Integer> getRatedItems(int user) {
		Set<Integer> set = new HashSet<Integer>();
		if (dataset.length>user) {
			long[] bitset = dataset[user];
			for(int indexbitset = 1; indexbitset < bitset.length; indexbitset++) {
				for(int indexlong = 0; indexlong < 64; indexlong++) {
					if((bitset[indexbitset] & (((long) 1)<<indexlong)) != 0) {
						set.add((indexbitset * 64) + indexlong);
					}
				}
			}
		}
		return set;
	}
	
	@Override
	public double getInitValue() {
		return initValue;
	}
	
	
	

	
	@Override
	public String ParametersToString() {
		String s = "{";
		Map<String,String> map = ParametersToMap();
		boolean b = true;
		for(String name: map.keySet()) {
			if (b) {
				s = s + "\"" + name + "\": \"" + map.get(name) + "\"";
				b = false;
			}
			else {
				s = s + ",\n\"" + name + "\": \"" + map.get(name) + "\"";
			}
		}
		s = s + "}";
		return s;
	}
	
	@Override
	public Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetStructure, util.Names.datasetBitArrayNativeArray);
		dic.put(util.Names.nb_bits, Integer.toString(size_sketchs));
		dic.put(util.Names.initValue, Double.toString(initValue));
//		dic.put(util.Names.nb_users, Integer.toString(dataset.size()));
		return dic;
	}
	
	
	private final static int getIntersectionSize(long[] bits1, long[] bits2) {
	    int nBits = 0;
	    for (int i=1; i<bits1.length; i++)
	        nBits += Long.bitCount(bits1[i] & bits2[i]);
	    return nBits;
	}
	
//	public final long get_card(int user) {
//		return (dataset.get(user)[0]);
//	}
	
	
}
