package dataset;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public final class DatasetLexi implements Dataset {
	
	private final Dataset dataset_exact;
	private final Dataset dataset_approx;
	private final double ratio;
	

	public String structure() {
		return util.Names.datasetLexi + dataset_approx.structure() + dataset_exact.structure();
	}
	
	public DatasetLexi(Dataset dataset_exact, Dataset dataset_approx, double ratio) {
		this.dataset_exact = dataset_exact;
		this.dataset_approx = dataset_approx;
		this.ratio = ratio;
	}
	
	public Dataset subDataset(int[] users) {
		return (new DatasetLexi(dataset_exact.subDataset(users),dataset_approx.subDataset(users), ratio));
	}

	@Override
	public double getRating(int user, int item) {
		return dataset_exact.getRating(user, item);
	}

	@Override
	public boolean hasRated(int user, int item) {
		return dataset_exact.hasRated(user, item);
	}

	@Override
	public double sim(int user1, int user2) {
		double sim_approx = dataset_approx.sim(user1, user2);
		if (sim_approx < ratio) {
			return sim_approx;
		}
		else {
			return (ratio + (dataset_exact.sim(user1, user2)*(1-ratio)));
		}
	}

	@Override
	public Set<Integer> getUsers() {
		return dataset_exact.getUsers();
	}

	@Override
	public Set<Integer> getRatedItems(int user) {
		return dataset_exact.getRatedItems(user);
	}

	@Override
	public double getInitValue() {
		return dataset_exact.getInitValue();
	}

	
	
	@Override
	public Map<String, String> ParametersToMap() {
		Map<String, String> map = alterMapParametersExact(dataset_exact.ParametersToMap());
		map.putAll(alterMapParametersApprox(dataset_approx.ParametersToMap()));
		map.put(util.Names.datasetStructure, util.Names.datasetLexi);
		map.put(util.Names.ratio, Double.toString(ratio));
		return map;
	}

	@Override
	public String ParametersToString() {
		String s = "{";
		Map<String,String> map = ParametersToMap();
		boolean b = true;
		for(String name: map.keySet()) {
			if (b) {
				s = s + "\"" + name + "\": \"" + map.get(name) + "\"";
				b = false;
			}
			else {
				s = s + ",\n\"" + name + "\": \"" + map.get(name) + "\"";
			}
		}
		s = s + "}";
		return s;
	}


	private final static Map<String,String> alterMapParametersExact(Map<String,String> map) {
		Map<String,String> new_map = new HashMap<String,String>();
		for (String param: map.keySet()) {
			if(!param.equals(util.Names.datasetName))
				new_map.put(param+"_exact", map.get(param));
		}
		return new_map;
	}
	

	private final static Map<String,String> alterMapParametersApprox(Map<String,String> map) {
		Map<String,String> new_map = new HashMap<String,String>();
		for (String param: map.keySet()) {
			if(!param.equals(util.Names.datasetName))
				new_map.put(param+"_approx", map.get(param));
		}
		return new_map;
	}
	
	
}
