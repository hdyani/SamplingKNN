package dataset;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import util.KNNGraph;

public final class DatasetABC implements Dataset {

	private final HashMap<Integer,BitSet[]> dataset;
	private final HashMap<Integer,int[]> dataset_size;
//	private final HashMap<Integer,int[]> dataset_cardinal;
	private final HashMap<Integer,Integer> profiles_size;
	private final int nb_bitset; /* number of bitset stored for each user, define the difference of size under which two users will be compared: 2^(nb_bitset-1)*/
//	private final int initSizeBitset;
	private final boolean estimNbBitset;
	
	private final int min_bitset_size;/* log of the minimum size for a bitset. e.g. 6 => 2^6 = 64*/
	private final int add_size; /*The size of the minimum bitset for an user is 2^(log(Profile)+add_size)  e.g. add_size = 3 mean that the profile will fill at most 1/(1+3) = 1/4 of the bitset*/

	private final double initValue;
	

	public String structure() {
		return util.Names.ABC;
	}
	
	
	public DatasetABC(HashMap<Integer,BitSet[]> dataset, HashMap<Integer,int[]> dataset_size, HashMap<Integer,Integer> profiles_size, int nb_bitset, int min_bitset_size, int add_size, boolean estimNbBitset, double initValue, int[] users) {
		this.nb_bitset = nb_bitset;
		this.add_size = add_size;
		this.min_bitset_size = min_bitset_size;
		this.estimNbBitset = estimNbBitset;
		this.initValue = initValue;
		this.dataset = new HashMap<Integer,BitSet[]>();
		this.dataset_size = new HashMap<Integer,int[]>();
//		this.dataset_cardinal = new HashMap<Integer,int[]>();
		this.profiles_size = new HashMap<Integer,Integer>();
		for(int user: users) {
			this.dataset.put(user, dataset.get(user));
			this.dataset_size.put(user, dataset_size.get(user));
//			this.dataset_cardinal.put(user, dataset_cardinal.get(user));
			this.profiles_size.put(user, profiles_size.get(user));
		}
	}
	public Dataset subDataset(int[] users) {
		return (new DatasetABC(dataset,dataset_size,profiles_size,nb_bitset,min_bitset_size,add_size,estimNbBitset,initValue,users));
	}
	

	
	public DatasetABC(Dataset dataset_, Dataset subdataset_, KNNGraph knng) throws IOException {
//		this(dataset_, subdataset_, knng, knng.get_users().size()/10);
		this(dataset_,subdataset_,knng,6,3);
	}
	
	public DatasetABC(Dataset dataset_, Dataset subdataset_, KNNGraph knng, int size_sampling) throws IOException {
//		this(dataset_, subdataset_, knng, knng.get_users().size()/10);
		this(dataset_, subdataset_, knng, size_sampling,6,3);
	}
	
	public DatasetABC(Dataset dataset_, Dataset subdataset_, KNNGraph knng, int min_bitset_size, int add_size) throws IOException {
//		this(dataset_, subdataset_, knng, knng.get_users().size()/10);
		this(dataset_, subdataset_, knng, 1, min_bitset_size, add_size);
	}
	
	public DatasetABC(Dataset dataset_, Dataset subdataset_, KNNGraph knng, int size_sampling, int min_bitset_size, int add_size) throws IOException {
		this.estimNbBitset = true;
		this.min_bitset_size = min_bitset_size;
		this.add_size = add_size;
		dataset = new HashMap<Integer,BitSet[]>();
		dataset_size = new HashMap<Integer,int[]>();
//		dataset_cardinal = new HashMap<Integer,int[]>();
		profiles_size = new HashMap<Integer,Integer>();
		this.initValue = dataset_.getInitValue();
//		this.nb_bitset = (int) util.Estimator.estimMinMinMax(dataset_, knng, size_sampling);
		double ratio = (((double) dataset_.getUsers().size()) / ((double) subdataset_.getUsers().size()));
		double minmax = util.Estimator.estimMinMinMax(subdataset_, knng, size_sampling) / ratio;
//		System.out.println("minmax: " + minmax);
		this.nb_bitset = (int) (Math.ceil((Math.log(1/minmax)/Math.log(2))));
//		this.nb_bitset = Math.max(10,(int) (Math.ceil((Math.log(1/minmax)/Math.log(2)))));

		int profile_size;
		int log_2_size;
		int nb_of_bitset;
		int bitset_size;
		int hash;
		for(int user: dataset_.getUsers()) {
			profile_size = dataset_.getRatedItems(user).size();
			profiles_size.put(user, profile_size);
			log_2_size = ((int) (Math.log(profile_size)/Math.log(2)))+add_size;
			
			nb_of_bitset = nb_bitset;
			/*
			 * The minimum size of bitset is 2^min_bitset_size
			 * We do not need bitset of size below, and we need at least one bitset
			 * */
			if(log_2_size < min_bitset_size) {
				nb_of_bitset = log_2_size + nb_bitset - min_bitset_size;
				if(nb_of_bitset < 1) {
					nb_of_bitset = 1;
				}
				log_2_size = min_bitset_size;
			}
			
			dataset.put(user,(new BitSet[nb_of_bitset]));
			dataset_size.put(user,(new int[nb_of_bitset]));
//			dataset_cardinal.put(user, (new int[nb_of_bitset]));
			
			for(int i = 0; i < nb_of_bitset; i++) {
//				int bitset_size = 2^(log_2_size + i);
				bitset_size = power(2,(log_2_size + i));
				dataset.get(user)[i] = new BitSet(bitset_size);
				dataset_size.get(user)[i] = bitset_size;
				
				for(int item: dataset_.getRatedItems(user)) {
//					int hash = util.HashFunctions.get_hash(item, bitset_size);
					hash = item % bitset_size;
					dataset.get(user)[i].set(hash);
				}
//				dataset_cardinal.get(user)[i] = dataset.get(user)[i].cardinality();
			}	
		}
	}
	
	
	public DatasetABC(String fileName, double initValue, Dataset dataset_, KNNGraph knng, int size_sampling) throws IOException {
		this(fileName,initValue,dataset_,knng,size_sampling,6,3);
	}
	
	public DatasetABC(String fileName, double initValue, Dataset dataset_, KNNGraph knng, int size_sampling, int min_bitset_size, int add_size) throws IOException {
		this.estimNbBitset = true;
		this.add_size = add_size;
		this.min_bitset_size = min_bitset_size;
		dataset = new HashMap<Integer,BitSet[]>();
		dataset_size = new HashMap<Integer,int[]>();
//		dataset_cardinal = new HashMap<Integer,int[]>();
		profiles_size = new HashMap<Integer,Integer>();
		this.initValue = initValue;

		double minmax = util.Estimator.estimMinMinMax(dataset_, knng, size_sampling);
		this.nb_bitset = (int) (Math.ceil((Math.log(1/minmax)/Math.log(2))));
		
		HashMap<Integer,HashMap<Integer,Double>> temporary_dataset= new HashMap<Integer,HashMap<Integer,Double>>();
		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		
		int user;
		int item;
		double rating;
		String[] rating_results;
		while ((ligne=io.readLine())!=null){
//			ligne = ligne.replace(separator, "");
			rating_results = ligne.split(util.Names.datasetSplit);
			user = Integer.parseInt(rating_results[0]);
			item = Integer.parseInt(rating_results[1]);
			rating = (Double.parseDouble(rating_results[2])) + this.initValue;
			if(rating > 3 + this.initValue) {
				if (temporary_dataset.get(user) == null) {
					temporary_dataset.put(user, new HashMap<Integer,Double>());
				}
				temporary_dataset.get(user).put(item,rating);
			}
		}
		io.close();
		rating_results = null;
		

		int profile_size;
		int log_2_size;
		int nb_of_bitset;
		int bitset_size;
		int hash;
		for(int user_id: temporary_dataset.keySet()) {
			profile_size = temporary_dataset.get(user_id).keySet().size();
			profiles_size.put(user_id, profile_size);
			log_2_size = (int) (Math.log(profile_size)/Math.log(2))+add_size;
			
			nb_of_bitset = nb_bitset;
			/*
			 * The minimum size of bitset is 2^min_bitset_size=64
			 * We do not need bitset of size below, and we need at least one bitset
			 * */
			if(log_2_size < min_bitset_size) {
				nb_of_bitset = log_2_size + nb_bitset - min_bitset_size;
				if(nb_of_bitset < 1) {
					nb_of_bitset = 1;
				}
				log_2_size = min_bitset_size;
			}
			
			dataset.put(user_id,(new BitSet[nb_of_bitset]));
			dataset_size.put(user_id,(new int[nb_of_bitset]));
//			dataset_cardinal.put(user,(new int[nb_of_bitset]));
			
			for(int i = 0; i < nb_of_bitset; i++) {
//				int bitset_size = 2^(log_2_size + i);
				bitset_size = power(2,(log_2_size + i));
				dataset.get(user_id)[i] = new BitSet(bitset_size);
				dataset_size.get(user_id)[i] = bitset_size;
				
				for(int item_id: temporary_dataset.get(user_id).keySet()) {
//					int hash = util.HashFunctions.get_hash(item, bitset_size);
					hash = item_id % bitset_size;
					dataset.get(user_id)[i].set(hash);
				}
//				dataset_cardinal.get(user)[i] = dataset.get(user)[i].cardinality();
			}
			
		}
		
		temporary_dataset = null;
	}
	
	
	public DatasetABC(String fileName, double initValue) throws IOException {
		this(fileName,10,initValue);
	}
	
	public DatasetABC(String fileName, int nb_bitset, double initValue) throws IOException {
		this(fileName,nb_bitset,6,3,initValue);
	}
	
	public DatasetABC(String fileName, int nb_bitset, int min_bitset_size, int size_add, double initValue) throws IOException {
		dataset = new HashMap<Integer,BitSet[]>();
		dataset_size = new HashMap<Integer,int[]>();
//		dataset_cardinal = new HashMap<Integer,int[]>();
		profiles_size = new HashMap<Integer,Integer>();

		this.estimNbBitset = false;
		this.initValue = initValue;
		this.nb_bitset = nb_bitset;
		this.min_bitset_size = min_bitset_size;
		this.add_size = size_add;
		
		HashMap<Integer,HashMap<Integer,Double>> temporary_dataset= new HashMap<Integer,HashMap<Integer,Double>>();
		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		
		int user;
		int item;
		double rating;
		String[] rating_results;
		while ((ligne=io.readLine())!=null){
//			ligne = ligne.replace(separator, "");
			rating_results = ligne.split(util.Names.datasetSplit);
			user = Integer.parseInt(rating_results[0]);
			item = Integer.parseInt(rating_results[1]);
			rating = (Double.parseDouble(rating_results[2])) + this.initValue;
			if(rating > 3 + this.initValue) {
				if (temporary_dataset.get(user) == null) {
					temporary_dataset.put(user, new HashMap<Integer,Double>());
				}
				temporary_dataset.get(user).put(item,rating);
			}
		}
		io.close();
		rating_results = null;
		

		int profile_size;
		int log_2_size;
		int nb_of_bitset;
		int bitset_size;
		int hash;
		for(int user_id: temporary_dataset.keySet()) {
			profile_size = temporary_dataset.get(user_id).keySet().size();
			profiles_size.put(user_id, profile_size);
			log_2_size = (int) (Math.ceil(Math.log(profile_size)/Math.log(2)))+add_size;
			
			nb_of_bitset = nb_bitset;
			/*
			 * The minimum size of bitset is 2^min_bitset_size=64
			 * We do not need bitset of size below, and we need at least one bitset
			 * */
			if(log_2_size < min_bitset_size) {
				nb_of_bitset = log_2_size + nb_bitset - min_bitset_size;
				if(nb_of_bitset < 1) {
					nb_of_bitset = 1;
				}
				log_2_size = min_bitset_size;
			}
			
			dataset.put(user_id,(new BitSet[nb_of_bitset]));
			dataset_size.put(user_id,(new int[nb_of_bitset]));
//			dataset_cardinal.put(user,(new int[nb_of_bitset]));
			
			for(int i = 0; i < nb_of_bitset; i++) {
//				int bitset_size = 2^(log_2_size + i);
				bitset_size = power(2,(log_2_size + i+1));
				dataset.get(user_id)[i] = new BitSet(bitset_size);
				dataset_size.get(user_id)[i] = bitset_size;
				
				for(int item_id: temporary_dataset.get(user_id).keySet()) {
//					int hash = util.HashFunctions.get_hash(item, bitset_size);
					hash = item_id % bitset_size;
					dataset.get(user_id)[i].set(hash);
				}
//				dataset_cardinal.get(user)[i] = dataset.get(user)[i].cardinality();
			}
			
		}
		
		temporary_dataset = null;
	}
	
	@Override
	public double getRating(int user, int item) {
		BitSet[] bs = dataset.get(user);
//		boolean b = bs[bs.length-1].get(util.HashFunctions.hash(item, bs.length));
		if (bs[bs.length-1].get(util.HashFunctions.hash(item, bs.length))) return 1;
		else return 0;
	}

	@Override
	public boolean hasRated(int user, int item) {
		BitSet[] bs = dataset.get(user);
//		boolean b = bs[bs.length-1].get(util.HashFunctions.hash(item, bs.length));
		return bs[bs.length-1].get(util.HashFunctions.hash(item, bs.length));
	}

	@Override
	public double sim(int user1, int user2) {
		
		int size1 = profiles_size.get(user1);
		int size2 = profiles_size.get(user2);
		
		int min;
		int max;
		boolean b = size1<size2;
		if(b){
			min = size1;
			max = size2;
		}
		else{
			min = size2;
			max = size1;
		}
		if(min * (2^nb_bitset) < max) {
//		if(min < (2^nb_bitset)*max) {
//			return 0;
			return (((double) min)/((double) max));
		}
		
		BitSet[] bs1 = dataset.get(user1);
		BitSet[] bs2 = dataset.get(user2);
		int[] sizes1 = dataset_size.get(user1);
		int[] sizes2 = dataset_size.get(user2);
		double score = 0;
		BitSet inter;
		int size;
		if(b) {
//			int bs1_index = bs1.length - 1;
//			inter = (BitSet) (bs1[bs1_index]).clone();
			inter = (BitSet) (bs1[bs1.length-1]).clone();
//			size1 = dataset_cardinal.get(user1)[bs1_index];
			size = sizes1[bs1.length-1/*bs1_index*/];
			for(int i=0;i<bs1.length/*bs1_index+1*/;i++) {
				if(sizes2[i]==size){
					inter.and(bs2[i]);
//					size2 = dataset_cardinal.get(user2)[i];
					break;
				}
			}
		}
		else {
//			int bs2_index = bs2.length - 1;
//			inter = (BitSet) (bs2[bs2_index]).clone();
			inter = (BitSet) (bs2[bs2.length-1]).clone();
//			size1 = dataset_cardinal.get(user1)[bs2_index];
			size = sizes2[bs2.length-1];
			for(int i=0;i<bs2.length;i++) {
				if(sizes1[i]==size){
					inter.and(bs1[i]);
//					size2 = dataset_cardinal.get(user2)[i];
					break;
				}
			}
		}
		int hamming = inter.cardinality();
//		System.out.println(size + " " + ((double) hamming) / ((double) (size1 + size2 - hamming)));
//		score = ((double) hamming) / ((double) (size1 + size2 - hamming));
		score = ((double) hamming) / ((double) (size1 + size2 - hamming));
		return score;
	}
	
	
	
	public int common_size(int user1, int user2) {
		int size1 = profiles_size.get(user1);
		int size2 = profiles_size.get(user2);
		int min;
		int max;
		boolean b = size1<size2;
		if(b){
			min = size1;
			max = size2;
		}
		else{
			min = size2;
			max = size1;
		}
		if(min * (2^nb_bitset) < max) {
			return 0;
		}
		else {
			if(b) {
				int[] sizes1 = dataset_size.get(user1);
				return (sizes1[sizes1.length-1/*bs1_index*/]);
			}
			else {
				int[] sizes2 = dataset_size.get(user2);
				return (sizes2[sizes2.length-1]);
			}
		}
	}

	@Override
	public Set<Integer> getUsers() {
		return dataset.keySet();
	}

	@Override
	public Set<Integer> getRatedItems(int user) {
		BitSet bitset = dataset.get(user)[dataset.get(user).length-1];
		Set<Integer> set = new HashSet<Integer>();
		for(int i=0; i<bitset.length();i++){
			if (bitset.get(i)){
				set.add(i);
			}
		}
		return set;
	}

	@Override
	public double getInitValue() {
		return initValue;
	}
	
	@Override
	public String ParametersToString() {
		String s = "{";
		Map<String,String> map = ParametersToMap();
		boolean b = true;
		for(String name: map.keySet()) {
			if (b) {
				s = s + "\"" + name + "\": \"" + map.get(name) + "\"";
				b = false;
			}
			else {
				s = s + ",\n\"" + name + "\": \"" + map.get(name) + "\"";
			}
		}
		s = s + "}";
		return s;
	}
	
	@Override
	public Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetStructure, util.Names.datasetABC);
		dic.put("nb_bitset", Integer.toString(nb_bitset));
//		dic.put(util.Names.nb_bitset, Integer.toString(nb_bitset));
		dic.put("add_size", Integer.toString(add_size));
//		dic.put(util.Names.add_size, Integer.toString(add_size));
		dic.put("min_bitset_size", Integer.toString(min_bitset_size));
//		dic.put(util.Names.min_bitset_size, Integer.toString(min_bitset_size));
		dic.put("estimNbBitset", Boolean.toString(estimNbBitset));
//		dic.put(util.Names.optionEstimNbBitsetShort, Boolean.toString(estimNbBitset));
//		dic.put(util.Names.nb_bits, Integer.toString(initSizeBitset));
		dic.put(util.Names.initValue, Double.toString(initValue));
		return dic;
	}
	
	private static int power(int base, int power){
		return (int) Math.pow(base, power);
	}

}
