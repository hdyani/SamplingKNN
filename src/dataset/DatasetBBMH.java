package dataset;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import util.Permutations;

public final class DatasetBBMH implements Dataset{
	private final Map<Integer, Double> norm;
	private final Map<Integer, Double> A_values;
	private final Permutations[] permutations;
	
	private final int b; //we will assume that b is a power of 2
	private final int nb_hash;
	
	private final Map<Integer, long[]> dataset_;
	
	private final long[] masks;
	
	private final double initValue;
	private final int size_array;
	
//	private final Set<Integer> item_set;
	

	public String structure() {
		return util.Names.BBMH;
	}
	

//	public DatasetBBMH(Map<Integer, Integer> norm, Map<Integer, Set<Integer>> dataset_, double initValue, int[] users/*, Map<Integer,Integer> mapID*/) {
//		this.norm = new HashMap<Integer, Double>();
//		this.dataset_ = new HashMap<Integer, Long[]>();
//		this.initValue = initValue;
//		for(int user: users) {
//			this.norm.put(user, norm.get(user));
//			this.dataset_.put(user, dataset_.get(user));
//		}
////		this.mapID=mapID;
////		this.mapID_ = new HashMap<Integer,Integer>();
////		for(int user: mapID.keySet()) {
////			this.mapID_.put(mapID.get(user), user);
////		}
//	}
	
	public DatasetBBMH(Map<Integer, Double> norm, Map<Integer, Double> A_values, Permutations[] permutations, int b, int nb_hash, Map<Integer, long[]> dataset_, double initValue, int[] users) {
		this.norm = new HashMap<Integer, Double>();
		this.A_values = new HashMap<Integer, Double>();
		this.permutations = permutations;
		this.b = b;
		this.nb_hash = nb_hash;
		this.dataset_ = new HashMap<Integer,long[]>();
		this.initValue = initValue;
		
		for(int user:users) {
			this.norm.put(user,norm.get(user));
			this.A_values.put(user, A_values.get(user));
			this.dataset_.put(user, dataset_.get(user));
		}
		masks = new long[64/b];
		for(int j=0; j<(64/b); j++) {
			masks[j] = ((long) Math.pow(2,b)-1) << (b*j);
		}
		size_array = this.getBBHMsize();
		
	}
	
	
	public Dataset subDataset(int[] users) {
		return (new DatasetBBMH(norm,A_values,permutations,b,nb_hash,dataset_,initValue,users));
	}
	

	public DatasetBBMH(String fileName, double initValue, int b, int nb_hash) throws IOException { 
		this.dataset_ = new HashMap<Integer, long[]>();
		this.norm = new HashMap<Integer, Double>();
		this.A_values = new HashMap<Integer, Double>();
		this.b = b;
		this.nb_hash = nb_hash;
//		this.item_set = new HashSet<Integer>();
		this.permutations = new Permutations[nb_hash];
		
		this.initValue = initValue;
		
		masks = new long[64/b];
		for(int j=0; j<(64/b); j++) {
			masks[j] = ((long) Math.pow(2,b)-1) << (b*j);
		}
		if (b==64) {
			masks[0] = ~ 0;
		}
//		masks[0] = 1;
		
//		for(int j=0; j<(64/b); j++) {
//			System.out.println(Long.toBinaryString(masks[j]));
//		}
		
		size_array = this.getBBHMsize();
		
		
		HashSet<Integer> item_set = new HashSet<Integer>();
		
		//CREATING THE TOTAL PROFILES
		HashMap<Integer, Set<Integer>> dataset_aux = new HashMap<Integer, Set<Integer>>();
		
		
		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io1 = new BufferedReader(ipsr);
		String ligne;
		String[] rating_results;
		int user;
		int item;
		double rating;
		while ((ligne=io1.readLine())!=null){
//			ligne = ligne.replace(separator, "");
			rating_results = ligne.split(util.Names.datasetSplit);
			rating = (Double.parseDouble(rating_results[2])) + this.initValue;
			if (rating > initValue + 3.0) {

				user = Integer.parseInt(rating_results[0]);
				
				item = Integer.parseInt(rating_results[1]);
				if (dataset_aux.get(user) == null) {
					dataset_aux.put(user, new HashSet<Integer>());
				}
				dataset_aux.get(user).add(item);
				item_set.add(item);
			}
			
		}
		io1.close();
		
		for(int i=0; i < nb_hash; i++) {
			permutations[i] = new Permutations(item_set,i);
		}
		int item_set_size = item_set.size();
		//CREATING THE BBHM
		double r;
		double A_value;
		int longindex;
		int bitindex;
		long hash;
		long mask;
//		long second_mask;
//		int first_b;
//		int second_b;
		
		double powb = Math.pow(2, b);
		
		for(int user_id: dataset_aux.keySet()) {
			long[] profile = new long[getBBHMsize()];
			Set<Integer> items = dataset_aux.get(user_id);
			r = ((double) (items.size()))/((double) item_set_size);
			norm.put(user_id, r);
			A_value = (r*(Math.pow(1-r, powb-1))/(1-(Math.pow(1-r, 2*b))));
			A_values.put(user_id, A_value);
			longindex=0;
			bitindex=0;
			for(int i=0; i < size_array; i++) {
				profile[i] = 0;
			}
			
			for(int i=0; i < nb_hash; i++) {
				hash = permutations[i].getHash(items);
//				long mask1 = masks[0];
//				long mask2 = masks[0] & hash;
//				mask = mask2 << bitindex;
				mask = (masks[0] & hash) << bitindex;
//				mask = (((long) powb) & hash) << bitindex;
				profile[longindex] = profile[longindex] ^ mask;
				
				
				
//				if(user_id == 1) {
//					System.out.println(hash);
//					System.out.println(Long.toBinaryString(hash));
//					System.out.println(Long.toBinaryString(mask));
////					System.out.println(Long.toBinaryString(mask1));
////					System.out.println(Long.toBinaryString(mask2));
//					System.out.println(Long.toBinaryString(profile[longindex]));
//				}
				
				
				bitindex = bitindex + b;
				if(bitindex >=64) {
					longindex++;
					bitindex = bitindex % 64;
				}
				
				
				
//				if(bitindex + b <= 63) {
//					mask = (((long) Math.pow(2,b)) & hash) << bitindex;
//					profile[longindex] = profile[longindex] ^ mask;
//					
//					bitindex = bitindex + b;
//				}
//				else {
//					first_b = 64 - bitindex;
//					second_b = b - first_b;
//					
//					
//					mask = (((long) Math.pow(2,first_b)) & hash) << bitindex;
//					profile[longindex] = profile[longindex] ^ mask;
//					
//					longindex++;
//					bitindex = second_b;
//
//					second_mask = (((long) Math.pow(2,second_b)) & hash); // << 0; we start at 0 since we change the long.
//					profile[longindex] = profile[longindex] ^ second_mask;	
//				}
				
				
//				bitindex = bitindex + b;
//				if(bitindex >=64) {
//					longindex++;
//					bitindex = bitindex % 64;
//				}
			}
			dataset_.put(user_id, profile);
		}
		
//		System.out.println("number of long :" + this.getBBHMsize());
//		System.out.println("b: "+b);
//		System.out.println("nb_hash: "+nb_hash);

	}

	public DatasetBBMH(String fileName) throws IOException { //Add double initValue = 0?
		this(fileName,0.0,1,1024);
	}


	@Override
	public double getRating(int user, int item) {
//		if(dataset_.get(user).contains(item)) {
//			return 5;
//		}
//		else {
//			return initValue;
//		}
		return initValue;
	}

	@Override
	public boolean hasRated(int user, int item) {
//		return (dataset.get(user) != null && dataset.get(user).get(item) != null);
//		return (dataset_.get(user) != null && dataset_.get(user).contains(item));
		return true;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	JACCARD

	@Override
	public double sim(int user1, int user2) {
		double A1 = A_values.get(user1);
		double A2 = A_values.get(user2);
		double r1 = norm.get(user1);
		double r2 = norm.get(user2);
		double C1 = (A1*r2)/(r1+r2) + (A2*r1)/(r1+r2);
		double C2 = (A1*r1)/(r1+r2) + (A2*r2)/(r1+r2);
		
		long[] profile1 = dataset_.get(user1);
		long[] profile2 = dataset_.get(user2);
		long[] profileNXOR = new long[profile1.length];
		for(int i=0; i < size_array; i++) {
//			profileNXOR[i] = ~ (profile1[i] ^ profile2[i]);
			profileNXOR[i] = (profile1[i] ^ profile2[i]);
		}
		
		int longindex=0;
		int bitindex=0;
		int intersection=0;
//		int miss=0;
//		int intersection_=0;
//		long mask1;
//		long mask2;
//		long second_mask1;
//		long second_mask2;
//		int error=0;
		
		long mask;
//		long mask_;
//		int bitcount;
		
		for(int i=0; i < nb_hash; i++) {
//			mask = masks[bitindex/b];
//			mask_ = mask & profileNXOR[longindex];
			mask = (masks[bitindex/b]) & profileNXOR[longindex];
//			bitcount = Long.bitCount(mask);
//			if(bitcount == b) {
//				intersection++;
//			}
			
			if(mask == 0) {
				intersection++;
			}
			
			
//			else {
//				miss++;
//			}
			
//			System.out.println(Long.toBinaryString(profile1[longindex]));
//			System.out.println(Long.toBinaryString(profile2[longindex]));
//			System.out.println(Long.toBinaryString(profileNXOR[longindex]));
//			System.out.println(Long.toBinaryString(mask));
//			System.out.println(Long.toBinaryString(mask_));
//			System.out.println(bitcount);
//			System.out.println(b);
			
			
//			mask1 = (((long) Math.pow(2,b))) << bitindex;
////			mask1 = mask;
//			mask2 = mask1;
			
//			mask1 = masks[bitindex/b];
//			mask2 = masks[bitindex/b];
			
////			mask1 = mask & profile1[longindex];
////			mask2 = mask & profile2[longindex];
			
			
//			long mask__ = mask & (~ (profile1[longindex] ^ profile2[longindex]));
//			if (mask_ != mask__) {
//				error++;
//				System.out.println("error: "+ error);
//			}
//			if((mask1 & mask2) != mask_ && (mask1 & mask2) != 0) {
//				System.out.println(false);
//			}
			
//			if (mask1 == mask2) {
//				intersection_++;
//			}
			
			
			
			
			
			bitindex = bitindex + b;
			if(bitindex >=64) {
				longindex++;
				bitindex = bitindex % 64;
			}
			
//			if(bitindex + b <= 63) {
////				mask = (((long) Math.pow(2,b))) << bitindex;
////				if(Long.bitCount(mask & profileAnd[longindex]) == b) {
////					intersection++;
////				}
//				
//				mask1 = (((long) Math.pow(2,b))) << bitindex;
//				mask2 = mask1;
//				mask1 = mask1 & profile1[longindex];
//				mask2 = mask2 & profile2[longindex];
//				
//				if (mask1 == mask2) {
//					intersection++;
//				}
//				
//				bitindex = bitindex + b;
//			}
//			else {
//				int first_b = 64 - bitindex;
//				int second_b = b - first_b;
//				
//				
//				mask1 = (((long) Math.pow(2,first_b))) << bitindex;
//				mask2 = mask1;
//				mask1 = mask1 & profile1[longindex] ;
//				mask2 = mask2 & profile2[longindex] ;
//				
//				longindex++;
//				bitindex = second_b;
//
//				second_mask1 = (((long) Math.pow(2,second_b))); // << 0; we start at 0 since we change the long.
//				second_mask2 = second_mask1;
//				second_mask1 = second_mask1 & profile1[longindex];
//				second_mask2 = second_mask2 & profile2[longindex];
//				
//				if(mask1 == mask2 && second_mask1 == second_mask2) {
//					intersection++;
//				}
				
//			}
		}
		
//		System.out.println(intersection);
//		System.out.println(miss);
//		System.out.println(intersection_);
//		System.out.println(((((double) intersection_)/((double) nb_hash))-C1)/(1-C2));
		
		
		
//		return ((((double) intersection)/((double) nb_hash))-C1)/(1-C2);
		
		
		
//		double sim = ((((double) intersection)/((double) nb_hash))-C1)/(1-C2);
//		if (sim < 0) {
//			return 0.;
//		}
//		else return sim;

		return ((((double) intersection)/((double) nb_hash))-C1)/(1-C2);
	}
	
	
	
	
	
	
	
	
	
	
	private final int getBBHMsize() {
//		return 64*(((b*nb_hash)/64)+1);
//		System.out.println("size: " + 64*((b*nb_hash)/64));
//		System.out.println("size: " + (64*((b*nb_hash)/64))/64);
		return ((b*nb_hash)/64);
	}
//	
//	private final int getLongIndex(int hash) {
//		return b*hash/64;
//	}
//	private final int getBitIndex(int hash) {
//		return b*hash%64;
//	}
//	
//	private final boolean twoLongs(int hash) {
//		return (64-(b*hash%64))<b;
//	}
//	
//	private final long getbBits(long min) {
//		long mask=0;
//		return mask;
//	}
	
	
	

	
	
	
	
	
	
	
	
	@Override
	public Set<Integer> getUsers() {
//		return dataset.keySet();
		return dataset_.keySet();
	}

	@Override
	public Set<Integer> getRatedItems(int user) {
//		if(dataset.get(user) == null) {
//		if(dataset_.get(user) == null) {
		return (new HashSet<Integer>());
//		}
//		else {
////			return dataset.get(user).keySet();
//			return dataset_.get(user);
//		}
	}

	
	@Override
	public double getInitValue() {
		return initValue;
	}
	

	
	@Override
	public String ParametersToString() {
		String s = "{";
		Map<String,String> map = ParametersToMap();
		boolean b = true;
		for(String name: map.keySet()) {
			if (b) {
				s = s + "\"" + name + "\": \"" + map.get(name) + "\"";
				b = false;
			}
			else {
				s = s + ",\n\"" + name + "\": \"" + map.get(name) + "\"";
			}
		}
		s = s + "}";
		return s;
	}
	
	@Override
	public Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetStructure, util.Names.BBMH);
		dic.put(util.Names.BBMHb, Integer.toString(b));
		dic.put(util.Names.BBMHhash, Integer.toString(nb_hash));
		dic.put(util.Names.initValue, Double.toString(initValue));
//		dic.put(util.Names.nb_users, Integer.toString(dataset.size()));
		return dic;
	}
	
	
	
	
	
	
	
	
	
	public DatasetBBMH(int init_value, int b, int nb_hash, String add) throws IOException {
		this.dataset_ = new HashMap<Integer, long[]>();
		this.norm = new HashMap<Integer, Double>();
		this.A_values = new HashMap<Integer, Double>();
		this.b = b;
		this.nb_hash = nb_hash;
//		this.item_set = new HashSet<Integer>();
		this.permutations = new Permutations[nb_hash];
		
		this.initValue = init_value;
		
		masks = new long[64/b];
		for(int j=0; j<(64/b); j++) {
			masks[j] = ((long) Math.pow(2,b)-1) << (b*j);
		}
		if (b==64) {
			masks[0] = ~ 0;
		}
		
		size_array = this.getBBHMsize();
		

		InputStream ips = new FileInputStream(add+"norm");
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io1 = new BufferedReader(ipsr);
		String ligne;
		String[] rating_results;
		int user;
		double value;
		String[] table_;
		long[] table;
		while ((ligne=io1.readLine())!=null){
			rating_results = ligne.split(" ");
			user = Integer.parseInt(rating_results[0]);
			value = Double.parseDouble(rating_results[1]);
			norm.put(user, value);
		}
		io1.close();
		
		ips = new FileInputStream(add+"A_values");
		ipsr = new InputStreamReader(ips);
		io1 = new BufferedReader(ipsr);
		while ((ligne=io1.readLine())!=null){
			rating_results = ligne.split(" ");
			user = Integer.parseInt(rating_results[0]);
			value = Double.parseDouble(rating_results[1]);
			A_values.put(user, value);
		}
		io1.close();
		
		ips = new FileInputStream(add+"dataset");
		ipsr = new InputStreamReader(ips);
		io1 = new BufferedReader(ipsr);
		while ((ligne=io1.readLine())!=null){
			rating_results = ligne.split(" ");
			user = Integer.parseInt(rating_results[0]);
			table_ = (ligne.replace(rating_results[0] + " [", "").replace("]", "")).split(" ");
			table = new long[table_.length];

			for(int i=0;i < table_.length; i++) {
				table[i] = Long.parseLong(table_[i]);
			}
			dataset_.put(user, table);
		}
		io1.close();
	}
	
	
	
	
	
	
	
	
	
	public void toFile(String add) throws IOException {
		BufferedWriter mbrWriterOutput = new BufferedWriter(new FileWriter("./"+add+"norm"));
		boolean b = true;
		for(int user: norm.keySet()) {
			if(b) {
				b = false;
			}
			else {
				mbrWriterOutput.newLine();
			}
			mbrWriterOutput.write(Integer.toString(user) + " " + Double.toString(norm.get(user)));
		}
		mbrWriterOutput.close();
		
		mbrWriterOutput = new BufferedWriter(new FileWriter("./"+add+"A_values"));
		b = true;
		for(int user: A_values.keySet()) {
			if(b) {
				b = false;
			}
			else {
				mbrWriterOutput.newLine();
			}
			mbrWriterOutput.write(Integer.toString(user) + " " + Double.toString(A_values.get(user)));
		}
		mbrWriterOutput.close();
		
		mbrWriterOutput = new BufferedWriter(new FileWriter("./"+add+"dataset"));
		b = true;
		for(int user: dataset_.keySet()) {
			if(b) {
				b = false;
			}
			else {
				mbrWriterOutput.newLine();
			}
			mbrWriterOutput.write(Integer.toString(user) + " " + arrayToString(dataset_.get(user)));
		}
		mbrWriterOutput.close();
	}

	
	private String arrayToString(long[] array) {
		String s = "[";
		
		boolean b = true;
		for (long d: array) {

			if(b) {
				b = false;
			}
			else {
				s = s + " ";
			}
			s = s + Long.toString(d);
		}
		
		s = s+"]";
		return s;
	}

}
