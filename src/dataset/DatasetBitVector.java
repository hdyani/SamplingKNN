package dataset;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import util.BitVector;

public final class DatasetBitVector implements Dataset {

	private final int size_sketchs;
	
	private final Map<Integer,BitVector> dataset;
	private final Map<Integer,Integer> dataset_size;
	
	private final double initValue;
	

	public String structure() {
		return util.Names.BitArray;
	}
	
	public DatasetBitVector(int size_sketchs,Map<Integer,BitVector> dataset,Map<Integer,Integer> dataset_size,double initValue, int[] users) {
		this.size_sketchs = size_sketchs;
		this.dataset = new HashMap<Integer, BitVector>();
		this.dataset_size = new HashMap<Integer,Integer>();
		this.initValue = initValue;
		for(int user: users) {
			this.dataset.put(user, dataset.get(user));
			this.dataset_size.put(user, dataset_size.get(user));
		}
	}
	public Dataset subDataset(int[] users) {
		return (new DatasetBitVector(size_sketchs,dataset,dataset_size,initValue,users));
	}
	
	
	
	public DatasetBitVector(String fileName, int size) throws IOException {
		this(fileName, size, 0);
	}
	
	public DatasetBitVector(String fileName, int size, double initValue) throws IOException {
		
		this.initValue = initValue;
		
		dataset = new HashMap<Integer, BitVector>();
		dataset_size= new HashMap<Integer, Integer>();
		this.size_sketchs = size;
		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		String[] rating_results;
		int user;
		int item;
		double rating;
		BitVector sketch;
		while ((ligne=io.readLine())!=null){
			rating_results = ligne.split(util.Names.datasetSplit);
			user = Integer.parseInt(rating_results[0]);
			item = Integer.parseInt(rating_results[1]);
			rating = Double.parseDouble(rating_results[2]);
			if (dataset.get(user) == null && rating > initValue + 3.0) {
				sketch = new BitVector(size_sketchs);
				dataset.put(user, sketch);
			}
			if(rating > initValue + 3.0) {
				BitVector bitset = dataset.get(user);
				bitset.put(item);
				
			}
		}
		io.close();
		for(int user_id: dataset.keySet()) {
			dataset_size.put(user_id, dataset.get(user_id).get_cardinality());
		}
	}

	@Override
	public double getRating(int user, int item) {
		boolean b = dataset.get(user) != null;
		if(b) {
			int hash = util.HashFunctions.hash(item, size_sketchs);
			int index = hash / 64;
			long[] sketch = dataset.get(user).get_bitset();
			b = b && ((sketch[index] & (((long) 1) << (hash % 64))) != 0);
		}
		if(b) {
			return 1;
		}
		return 0;
	}

	@Override
	public boolean hasRated(int user, int item) {
		boolean b = dataset.get(user) != null;
		if(b) {
			int hash = util.HashFunctions.hash(item, size_sketchs);
			int index = hash / 64;
			long[] sketch = dataset.get(user).get_bitset();
			b = b && ((sketch[index] & (((long) 1) << (hash % 64))) != 0);
		}
		return b;
	}

	@Override
	public double sim(int user1, int user2) {
		return (BitVector.jaccard(dataset.get(user1).get_bitset(),dataset.get(user2).get_bitset(),dataset_size.get(user1),dataset_size.get(user2)));
	}
//	public double sim(int user1, int user2) {
//		return (jaccard(dataset.get(user1),dataset.get(user2)));
//	}
	
	
	@Override
	public Set<Integer> getUsers() {
		return dataset.keySet();
	}

	@Override
	public Set<Integer> getRatedItems(int user) {
		Set<Integer> set = new HashSet<Integer>();
		if (dataset.containsKey(user)) {
			long[] bitset = dataset.get(user).get_bitset();
			for(int indexbitset = 0; indexbitset < bitset.length; indexbitset++) {
				for(int indexlong = 0; indexlong < 64; indexlong++) {
					if((bitset[indexbitset] & (((long) 1)<<indexlong)) != 0) {
						set.add((indexbitset * 64) + indexlong);
					}
				}
			}
		}
		return set;
	}
	
	@Override
	public double getInitValue() {
		return initValue;
	}
	
	
	
	

	
	@Override
	public String ParametersToString() {
		String s = "{";
		Map<String,String> map = ParametersToMap();
		boolean b = true;
		for(String name: map.keySet()) {
			if (b) {
				s = s + "\"" + name + "\": \"" + map.get(name) + "\"";
				b = false;
			}
			else {
				s = s + ",\n\"" + name + "\": \"" + map.get(name) + "\"";
			}
		}
		s = s + "}";
		return s;
	}
	
	@Override
	public Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetStructure, util.Names.datasetBitArray);
		dic.put(util.Names.nb_bits, Integer.toString(size_sketchs));
		dic.put(util.Names.initValue, Double.toString(initValue));
//		dic.put(util.Names.nb_users, Integer.toString(dataset.size()));
		return dic;
	}
	
	
	
}
