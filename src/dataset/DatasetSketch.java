package dataset;

import java.io.*;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public final class DatasetSketch implements Dataset {

	private final int nb_sketchs;
	private final int size_sketchs;
	
	private final Map<Integer,BitSet[]> dataset;
	private final Map<Integer,Integer> dataset_size;
	
	private final double initValue;
	

	public String structure() {
		return util.Names.Sketch;
	}
	
	public DatasetSketch(int nb_sketchs,int size_sketchs,Map<Integer,BitSet[]> dataset,Map<Integer,Integer> dataset_size,double initValue, int[] users) {
		this.nb_sketchs = nb_sketchs;
		this.size_sketchs = size_sketchs;
		this.dataset = new HashMap<Integer, BitSet[]>();
		this.dataset_size = new HashMap<Integer,Integer>();
		this.initValue = initValue;
		for(int user: users) {
			this.dataset.put(user, dataset.get(user));
			this.dataset_size.put(user, dataset_size.get(user));
		}
	}
	public Dataset subDataset(int[] users) {
		return (new DatasetSketch(nb_sketchs,size_sketchs,dataset,dataset_size,initValue,users));
	}
	
	
	
	public DatasetSketch(String fileName, int size, int nb_hash) throws IOException {
		this(fileName, size, nb_hash, 0);
	}
	
	public DatasetSketch(String fileName, int size, int nb_hash, double initValue) throws IOException {
		
		this.initValue = initValue;
		
		dataset = new HashMap<Integer, BitSet[]>();
		dataset_size= new HashMap<Integer, Integer>();
		this.nb_sketchs = nb_hash;
		this.size_sketchs = size;
		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		String[] rating_results;
		int user;
		int item;
		double rating;
		BitSet[] sketch;
		while ((ligne=io.readLine())!=null){
			rating_results = ligne.split(util.Names.datasetSplit);
			user = Integer.parseInt(rating_results[0]);
			item = Integer.parseInt(rating_results[1]);
			rating = Double.parseDouble(rating_results[2]);
			if (dataset.get(user) == null && rating > initValue + 3.0) {
				sketch = new BitSet[nb_hash];
				for(int i = 0; i<sketch.length; i++){
					sketch[i] = new BitSet(size_sketchs);
				}
				dataset.put(user, sketch);
//				dataset.put(user, new FilterBuilder(average_number_ratings, percent_false_positives).buildBloomFilter());
			}
			if(rating > initValue + 3.0) {
				sketch = dataset.get(user);
				for(int i = 0; i<sketch.length; i++){
					sketch[i].set(get_hash(item,i));
				}
			}
//			System.out.println(dataset.get(user).getBitSet().cardinality());
		}
		io.close();
		BitSet[] bitset;
		for(int user_id: dataset.keySet()) {
			bitset = dataset.get(user_id);
			int max_size = 0;
			for(int i = 0; i < nb_sketchs; i++){
				int size_aux = (bitset[i]).cardinality();
				if (size_aux > max_size){
					max_size = size_aux;
				}
			}
			dataset_size.put(user_id, max_size);
		}
	}

	@Override
	public double getRating(int user, int item) {
		boolean b = dataset.get(user) != null;
		if(b) {
			BitSet[] sketch = dataset.get(user);
			for(int i = 0; i<sketch.length; i++){
				b = b && sketch[i].get(get_hash(item,i));
			}
		}
		if(b) {
			return 1;
		}
		return 0;
	}

	@Override
	public boolean hasRated(int user, int item) {
		boolean b = dataset.get(user) != null;
		if(b) {
			BitSet[] sketch = dataset.get(user);
			for(int i = 0; i<sketch.length; i++){
				b = b && sketch[i].get(get_hash(item,i));
			}
		}
		return b;
	}

	@Override
	public double sim(int user1, int user2) {
		BitSet[] p1 = dataset.get(user1);
		BitSet[] p2 = dataset.get(user2);
//		p1.intersect(p2);
		int min_inter = size_sketchs+1;
		BitSet bs1;
		BitSet bs2;
		BitSet bs_inter;
		for(int i = 0 ; i < p1.length ; i++) {
			bs1 = p1[i];
			bs2 = p2[i];
			bs_inter = (BitSet) bs1.clone();
			bs_inter.and(bs2);
			int inter = bs_inter.cardinality();
			if (inter < min_inter) {
				min_inter = inter;
			}
		}
		return ((double) min_inter)/((double) (dataset_size.get(user1) + dataset_size.get(user2) - min_inter));
	}
	
	
	@Override
	public Set<Integer> getUsers() {
		return dataset.keySet();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<Integer> getRatedItems(int user) {
		return (Set<Integer>) dataset.get(user)[0];
	}
	
	@Override
	public double getInitValue() {
		return initValue;
	}
	
	//Robert Jenkins' 96 bit Mix Function
	public int get_hash(int item, int hash) {
		int a = hash * 0xcc9e2d51;
		int b = hash * (0x1b873593)^(hash-1);
		int c = item;
		a=a-b;  a=a-c;  a=a^(c >>> 13);
		b=b-c;  b=b-a;  b=b^(a << 8);
		c=c-a;  c=c-b;  c=c^(b >>> 13);
		a=a-b;  a=a-c;  a=a^(c >>> 12);
		b=b-c;  b=b-a;  b=b^(a << 16);
		c=c-a;  c=c-b;  c=c^(b >>> 5);
		a=a-b;  a=a-c;  a=a^(c >>> 3);
		b=b-c;  b=b-a;  b=b^(a << 10);
		c=c-a;  c=c-b;  c=c^(b >>> 15);
		c = c % size_sketchs;
		c = c + size_sketchs;
		c = c % size_sketchs;
		return c;
	}
	
	
	

	
	@Override
	public String ParametersToString() {
		String s = "{";
		Map<String,String> map = ParametersToMap();
		boolean b = true;
		for(String name: map.keySet()) {
			if (b) {
				s = s + "\"" + name + "\": \"" + map.get(name) + "\"";
				b = false;
			}
			else {
				s = s + ",\n\"" + name + "\": \"" + map.get(name) + "\"";
			}
		}
		s = s + "}";
		return s;
	}
	
	@Override
	public Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetStructure, util.Names.datasetSketch);
		dic.put(util.Names.nb_hash, Integer.toString(nb_sketchs));
		dic.put(util.Names.nb_bits, Integer.toString(size_sketchs));
		dic.put(util.Names.initValue, Double.toString(initValue));
//		dic.put(util.Names.nb_users, Integer.toString(dataset.size()));
		return dic;
	}
	
	
	
}
