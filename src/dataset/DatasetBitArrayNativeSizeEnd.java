package dataset;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class DatasetBitArrayNativeSizeEnd implements Dataset {

	private final int size_sketchs;
	private final int nb_long;
	
	private final Map<Integer,long[]> dataset;
	
	private final double initValue;
	

	public String structure() {
		return util.Names.BitArray;
	}
	
	public DatasetBitArrayNativeSizeEnd(int size_sketchs,Map<Integer,long[]> dataset,double initValue, int[] users) {
		this.size_sketchs = size_sketchs;
		this.dataset = new HashMap<Integer, long[]>();
		this.initValue = initValue;
		this.nb_long = ((int) Math.ceil(((double) size_sketchs)/((double)64)));
		for(int user: users) {
			this.dataset.put(user, dataset.get(user));
		}
	}
	public Dataset subDataset(int[] users) {
		return (new DatasetBitArrayNativeSizeEnd(size_sketchs,dataset,initValue,users));
	}
	
	
	
	public DatasetBitArrayNativeSizeEnd(String fileName, int size) throws IOException {
		this(fileName, size, 0);
	}
	
	public DatasetBitArrayNativeSizeEnd(String fileName, int size, double initValue) throws IOException {
		
		this.initValue = initValue;
		
		dataset = new HashMap<Integer, long[]>();
		this.size_sketchs = size;
		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		String[] rating_results;
		nb_long = ((int) Math.ceil(((double) size_sketchs)/((double)64)));
		int user;
		int item;
		double rating;
		long[] sketch;
		while ((ligne=io.readLine())!=null){
			rating_results = ligne.split(util.Names.datasetSplit);
			user = Integer.parseInt(rating_results[0]);
			item = Integer.parseInt(rating_results[1]);
			rating = Double.parseDouble(rating_results[2]);
			if (dataset.get(user) == null && rating > initValue + 3.0) {
				sketch = new long[nb_long+1];
				dataset.put(user, sketch);
			}
			if(rating > initValue + 3.0) {
				long[] bitset = dataset.get(user);
				int hash = util.HashFunctions.hash(item, size_sketchs);
				int index = (hash / 64);
				bitset[index] = bitset[index] | (((long) 1) << (hash % 64));
				
			}
		}
		io.close();
		for(int user_id: dataset.keySet()) {
			dataset.get(user_id)[nb_long] = cardinality(dataset.get(user_id));
		}
	}

	@Override
	public double getRating(int user, int item) {
		boolean b = dataset.get(user) != null;
		if(b) {
			int hash = util.HashFunctions.hash(item, size_sketchs);
			int index = (hash / 64);
			long[] sketch = dataset.get(user);
			b = b && ((sketch[index] & (((long) 1) << (hash % 64))) != 0);
		}
		if(b) {
			return 1;
		}
		return 0;
	}

	@Override
	public boolean hasRated(int user, int item) {
		boolean b = dataset.get(user) != null;
		if(b) {
			int hash = util.HashFunctions.hash(item, size_sketchs);
			int index = (hash / 64);
			long[] sketch = dataset.get(user);
			b = b && ((sketch[index] & (((long) 1) << (hash % 64))) != 0);
		}
		return b;
	}

	@Override
	public double sim(int user1, int user2) {
//		long[] bitset1 = dataset.get(user1);
//		long[] bitset2 = dataset.get(user2);
		int intersize = getIntersectionSize(dataset.get(user1),dataset.get(user2));
		return (((double) intersize) / ((double) (dataset.get(user1)[nb_long] + dataset.get(user2)[nb_long] - intersize)));
//		return (jaccard(dataset.get(user1),dataset.get(user2)));
	}
	
	
	@Override
	public Set<Integer> getUsers() {
		return dataset.keySet();
	}

	@Override
	public Set<Integer> getRatedItems(int user) {
		Set<Integer> set = new HashSet<Integer>();
		if (dataset.containsKey(user)) {
			long[] bitset = dataset.get(user);
			for(int indexbitset = 0; indexbitset < bitset.length-1; indexbitset++) {
				for(int indexlong = 0; indexlong < 64; indexlong++) {
					if((bitset[indexbitset] & (((long) 1)<<indexlong)) != 0) {
						set.add((indexbitset * 64) + indexlong);
					}
				}
			}
		}
		return set;
	}
	
	@Override
	public double getInitValue() {
		return initValue;
	}
	
	
	

	
	@Override
	public String ParametersToString() {
		String s = "{";
		Map<String,String> map = ParametersToMap();
		boolean b = true;
		for(String name: map.keySet()) {
			if (b) {
				s = s + "\"" + name + "\": \"" + map.get(name) + "\"";
				b = false;
			}
			else {
				s = s + ",\n\"" + name + "\": \"" + map.get(name) + "\"";
			}
		}
		s = s + "}";
		return s;
	}
	
	@Override
	public Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetStructure, util.Names.datasetBitArray);
		dic.put(util.Names.nb_bits, Integer.toString(size_sketchs));
		dic.put(util.Names.initValue, Double.toString(initValue));
//		dic.put(util.Names.nb_users, Integer.toString(dataset.size()));
		return dic;
	}
	
//	private final static double jaccard(long[] bitset1, long[] bitset2){
//		int intersize = getIntersectionSize(bitset1,bitset2);
//		return (intersize / (bitset1[0] + bitset2[0] - intersize));
//	}
//	
	
	
	
	private final int cardinality(long[] bitset) {
		int cardinality = 0;
		for(int i=0; i < nb_long ; i++) {
			cardinality = cardinality + Long.bitCount(bitset[i]);
		}
		return cardinality;
	}
	
	private final int getIntersectionSize(long[] bits1, long[] bits2) {
	    int nBits = 0;
	    for (int i=0; i<nb_long; i++)
	        nBits += Long.bitCount(bits1[i] & bits2[i]);
	    return nBits;
	}
	
	
	
}
