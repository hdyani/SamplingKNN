package dataset.extend;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import dataset.Dataset;

public final class DatasetItemSamplingConstantProbIter implements Dataset{
	private final Map<Integer, Integer> norm;
	
//	private final Map<Integer, Double> ratio;

	private final Map<Integer, Set<Integer>> dataset_;
	
//	private final Set<Integer> total_items;
	private final Set<Integer> items;
	private final double prob;
	
	private final double initValue;

	/*
	 * We do NOT store the negative ratings
	 * We sample the items, we keep an item i with prob equals to (prob)^(|P_i|) so that the most popular items are kept with high probability.
	 * We go through the dataset twice, once to determine the items we keep.
	 * Once to create the actual dataset which only contains ratings involving kept items.
	 */
	

	public String structure() {
		return util.Names.ISCPI;
	}
	

	public DatasetItemSamplingConstantProbIter(Map<Integer, Integer> norm, Map<Integer, Set<Integer>> dataset_, Set<Integer> items, double prob, double initValue, int[] users/*, Map<Integer,Integer> mapID*/) {
		this.norm = new HashMap<Integer, Integer>();
//		this.ratio = ratio;
		this.dataset_ = new HashMap<Integer, Set<Integer>>();
		this.initValue = initValue;
		this.items = items;
		this.prob = prob;
		for(int user: users) {
			this.norm.put(user, norm.get(user));
			this.dataset_.put(user, dataset_.get(user));
		}
	}
	public Dataset subDataset(int[] users) {
		return (new DatasetItemSamplingConstantProbIter(norm,dataset_,items,prob,initValue,users));
	}
	

	public DatasetItemSamplingConstantProbIter(String fileName, double prob, double initValue) throws IOException {
		dataset_ = new HashMap<Integer, Set<Integer>>();
		norm = new HashMap<Integer, Integer>();
//		ratio = new HashMap<Integer, Double>();
		this.items = new HashSet<Integer>();
		this.prob = prob;
		
		this.initValue = initValue;

		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io1 = new BufferedReader(ipsr);
		String ligne;
		String[] rating_results;
		int user;
		int item;
		double rating;
		Random random = new Random();
		while ((ligne=io1.readLine())!=null){
			rating_results = ligne.split(util.Names.datasetSplit);
			rating = (Double.parseDouble(rating_results[2])) + this.initValue;
			if (rating > initValue + 3.0) {
				item = Integer.parseInt(rating_results[1]);

				if(random.nextDouble()<prob) {
					items.add(item);
				}
				
			}
			
		}
		io1.close();
		
		
		ips = new FileInputStream(fileName);
		ipsr = new InputStreamReader(ips);
		io1 = new BufferedReader(ipsr);
		while ((ligne=io1.readLine())!=null){
			rating_results = ligne.split(util.Names.datasetSplit);
			rating = (Double.parseDouble(rating_results[2])) + this.initValue;
			if (rating > initValue + 3.0) {
				user = Integer.parseInt(rating_results[0]);
				item = Integer.parseInt(rating_results[1]);

//				
//				if(!norm.containsKey(user)) {
//					norm.put(user, 0);
//				}
//				norm.put(user, norm.get(user)+1);
				
				if(items.contains(item)) {
					if (dataset_.get(user) == null) {
						dataset_.put(user, new HashSet<Integer>());
						norm.put(user, 0);
					}
					dataset_.get(user).add(item);
					norm.put(user, norm.get(user)+1);
				}
			}
			
		}
		io1.close();
		
		
//		for(int userID: dataset_.keySet()) {
//			ratio.put(userID, ((double) dataset_.get(userID).size())/((double) norm.get(userID)));
//			
//		}

	}

	public DatasetItemSamplingConstantProbIter(String fileName) throws IOException { //Add double initValue = 0?
		this(fileName,20,0.0);
	}


	@Override
	public double getRating(int user, int item) {
		if(dataset_.get(user).contains(item)) {
			return 5;
		}
		else {
			return initValue;
		}
	}

	@Override
	public boolean hasRated(int user, int item) {
		return (dataset_.get(user) != null && dataset_.get(user).contains(item));
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	JACCARD: scaled because of the sampling
	
	@Override
	public double sim(int user1, int user2) {
		double sim = 0;
		Set<Integer> set1 = dataset_.get(user1);
		Set<Integer> set2 = dataset_.get(user2);
		for(int item:set1) {
			if (set2.contains(item)) {
				sim++;
			}
		}	

//		sim = sim * (1/(ratio.get(user1) * ratio.get(user2)));
		double dist = norm.get(user1) + norm.get(user2) - sim;
		if (dist != 0) {
			sim = sim / dist;
		}
		return sim;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public Set<Integer> getUsers() {
		return dataset_.keySet();
	}

	@Override
	public Set<Integer> getRatedItems(int user) {
		if(dataset_.get(user) == null) {
			return (new HashSet<Integer>());
		}
		else {
			return dataset_.get(user);
		}
	}

	
	@Override
	public double getInitValue() {
		return initValue;
	}
	

	
	@Override
	public String ParametersToString() {
		String s = "{";
		Map<String,String> map = ParametersToMap();
		boolean b = true;
		for(String name: map.keySet()) {
			if (b) {
				s = s + "\"" + name + "\": \"" + map.get(name) + "\"";
				b = false;
			}
			else {
				s = s + ",\n\"" + name + "\": \"" + map.get(name) + "\"";
			}
		}
		s = s + "}";
		return s;
	}
	
	@Override
	public Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetStructure, util.Names.ISCPI);
		dic.put(util.Names.initValue, Double.toString(initValue));
		dic.put(util.Names.prob, Double.toString(prob));
		return dic;
	}


}
