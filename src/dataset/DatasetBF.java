package dataset;

import java.io.*;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import orestes.bloomfilter.BloomFilter;
import orestes.bloomfilter.FilterBuilder;

public final class DatasetBF implements Dataset {

	private final Map<Integer,BloomFilter<Integer>> dataset;
	
	private final int average_number_ratings;
	private final double percent_false_positives;
	private final int nb_bits;
	private final int nb_hash;
	private final double initValue;

	public String structure() {
		return util.Names.BloomFilter;
	}
	
	public DatasetBF(Map<Integer,BloomFilter<Integer>> dataset, int avR, double PFP, int nb_bits, int nb_hash, double initValue, int[] users) {
		this.average_number_ratings = avR;
		this.percent_false_positives = PFP;
		this.nb_bits = nb_bits;
		this.nb_hash = nb_hash;
		this.initValue = initValue;
		this.dataset = new HashMap<Integer,BloomFilter<Integer>>();
		for (int user: users) {
			this.dataset.put(user, dataset.get(user));
		}
	}
	public Dataset subDataset(int[] users) {
		return (new DatasetBF(dataset,average_number_ratings,percent_false_positives,nb_bits,nb_hash,initValue,users));
	}
	
	
	
	public DatasetBF(String fileName, int average_number_ratings_, double percent_false_positives_) throws IOException {
		this(fileName, average_number_ratings_, percent_false_positives_, 0.0);
	}
	
	public DatasetBF(String fileName, int nb_bits, int nb_hash) throws IOException {
		this(fileName, nb_bits, nb_hash, 0.0);
	}
	
	
	public DatasetBF(String fileName, int nb_bits, int nb_hash, double initValue) throws IOException {
		this.initValue = initValue;
		dataset = new HashMap<Integer, BloomFilter<Integer>>();
		this.nb_bits = nb_bits;
		this.nb_hash = nb_hash;
		this.average_number_ratings = (new FilterBuilder(nb_bits, nb_hash).buildBloomFilter()).getExpectedElements();
		this.percent_false_positives = (new FilterBuilder(nb_bits, nb_hash).buildBloomFilter()).getFalsePositiveProbability();
//		this.average_number_ratings = average_number_ratings_;
//		this.percent_false_positives = percent_false_positives_;
		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		String[] rating_results;
		int user;
		int item;
		double rating;
		BloomFilter<Integer> bf;
		while ((ligne=io.readLine())!=null){
			rating_results = ligne.split(util.Names.datasetSplit);
			user = Integer.parseInt(rating_results[0]);
			item = Integer.parseInt(rating_results[1]);
			rating = Double.parseDouble(rating_results[2]);
			if (dataset.get(user) == null && rating > initValue + 3) {
//				BloomFilter<Integer> bf = new FilterBuilder(average_number_ratings, percent_false_positives).buildBloomFilter();				
				bf = new FilterBuilder(nb_bits, nb_hash).buildBloomFilter();
				dataset.put(user, bf);
			}
			if(rating > initValue + 3) {
				dataset.get(user).add(item);
			}
		}
		io.close();
	}

	
	
	public DatasetBF(String fileName, int average_number_ratings_, double percent_false_positives_, double initValue) throws IOException {
		this.initValue = initValue;
		dataset = new HashMap<Integer, BloomFilter<Integer>>();
		this.average_number_ratings = average_number_ratings_;
		this.percent_false_positives = percent_false_positives_;
		this.nb_bits = (new FilterBuilder(average_number_ratings, percent_false_positives).buildBloomFilter()).getSize();
		this.nb_hash = (new FilterBuilder(average_number_ratings, percent_false_positives).buildBloomFilter()).getHashes();
		InputStream ips = new FileInputStream(fileName);
		InputStreamReader ipsr = new InputStreamReader(ips);
		BufferedReader io = new BufferedReader(ipsr);
		String ligne;
		String[] rating_results;
		int user;
		int item;
		double rating;
		BloomFilter<Integer> bf;
		while ((ligne=io.readLine())!=null){
			rating_results = ligne.split(util.Names.datasetSplit);
			user = Integer.parseInt(rating_results[0]);
			item = Integer.parseInt(rating_results[1]);
			rating = Double.parseDouble(rating_results[2]);
			if (dataset.get(user) == null) {
				bf = new FilterBuilder(average_number_ratings, percent_false_positives).buildBloomFilter();
				dataset.put(user, bf);
			}
			if(rating > initValue + 3) {
				dataset.get(user).add(item);
			}
		}
		io.close();
	}

	@Override
	public double getRating(int user, int item) {
		if(dataset.get(user) != null && dataset.get(user).contains(item)) {
			return 1;
		}
		return 0;
	}
	
	@Override
	public boolean hasRated(int user, int item) {
		return dataset.get(user) != null && dataset.get(user).contains(item);
	}

	@Override
	public double sim(int user1, int user2) {
//		BloomFilter<Integer> p1 = dataset.get(user1);
//		BloomFilter<Integer> p2 = dataset.get(user2);
		double sim = 0;
//		BitSet bs1 = p1.getBitSet();
//		BitSet bs2 = p2.getBitSet();
		BitSet bs1 = dataset.get(user1).getBitSet();
		BitSet bs2 = dataset.get(user2).getBitSet();
		double dist1 = bs1.cardinality();
		double dist2 = bs2.cardinality();
		if (dist1 != 0 && dist2 != 0) {
			BitSet bs_inter = (BitSet) bs1.clone();
			bs_inter.and(bs2);
			double inter = bs_inter.cardinality();
			sim = (inter)/(dist1 + dist2 - inter);
		}
		return sim;
	}

	@Override
	public Set<Integer> getUsers() {
		return dataset.keySet();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<Integer> getRatedItems(int user) {
		return (Set<Integer>) dataset.get(user).getBitSet();
	}
	
	@Override
	public double getInitValue() {
		return this.initValue;
	}
	
	@Override
	public String ParametersToString() {
		String s = "{";
		Map<String,String> map = ParametersToMap();
		boolean b = true;
		for(String name: map.keySet()) {
			if (b) {
				s = s + "\"" + name + "\": \"" + map.get(name) + "\"";
				b = false;
			}
			else {
				s = s + ",\n\"" + name + "\": \"" + map.get(name) + "\"";
			}
		}
		s = s + "}";
		return s;
	}
	
	@Override
	public Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetStructure, util.Names.datasetBF);
		dic.put(util.Names.AVRatings, Double.toString(average_number_ratings));
		dic.put(util.Names.pfp, Double.toString(percent_false_positives));
		dic.put(util.Names.pfp, Double.toString(percent_false_positives));
		dic.put(util.Names.initValue, Double.toString(initValue));
//		dic.put(util.Names.nb_users, Integer.toString(dataset.size()));
		dic.put(util.Names.nb_bits, Integer.toString(nb_bits));
		dic.put(util.Names.nb_hash, Integer.toString(nb_hash));
		return dic;
	}
	

	
}
