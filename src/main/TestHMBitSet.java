package main;

import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class TestHMBitSet {

	public static void main(String[] args) {
		Map<Integer, Set<Integer>> dataset_full = new HashMap<Integer,Set<Integer>>();
		Map<Integer, BitSet> dataset_compacted = new HashMap<Integer,BitSet>();
		Map<Integer, boolean[]> dataset_raw_compacted = new HashMap<Integer,boolean[]>();
		int size_bitset=512;
		int max_id_item=1000;
		int nb_users = 5000;
		int nb_items = 20;
		int nb_exp = 10;
		
		Random random = new Random();
		for (int user = 0; user < nb_users; user++) {
			dataset_full.put(user, new HashSet<Integer>());
			dataset_compacted.put(user, new BitSet(size_bitset));
			boolean[] new_bitset = new boolean[size_bitset];
//			for(int i = 0; i < size_bitset; i++) {
//				new_bitset[i] = false;
//			}
			dataset_raw_compacted.put(user, new_bitset);
			
			for(int item = 0; item < nb_items; item++) {
				int item_id = random.nextInt(max_id_item);
				int hash = util.HashFunctions.get_hash(item_id, 1, size_bitset);
				dataset_full.get(user).add(item);
				dataset_compacted.get(user).set(hash);
				dataset_raw_compacted.get(user)[hash] = true;
			}
		}
		
//		long startHM = System.currentTimeMillis();
//		for(int exp = 0; exp < nb_exp; exp++) {
//			for(int user1:dataset_full.keySet()) {
//				Set<Integer> p1 = dataset_full.get(user1);
//				for(int user2:dataset_full.keySet()) {
//					compHM(p1, dataset_full.get(user2));
//				}
//			}
//		}
//		long stopHM = System.currentTimeMillis();
//		System.out.println("HM " + (stopHM-startHM));
		
//		long startRaw = System.currentTimeMillis();
//		for(int exp = 0; exp < nb_exp; exp++) {
//			for(int user1:dataset_full.keySet()) {
//				boolean[] p1 = dataset_raw_compacted.get(user1);
//				for(int user2:dataset_full.keySet()) {
//					compRawBitSet(p1, dataset_raw_compacted.get(user2));
//				}
//			}
//		}
//		long stopRaw = System.currentTimeMillis();
//		System.out.println("Raw " + (stopRaw-startRaw));

		long startBS = System.currentTimeMillis();
		for(int exp = 0; exp < nb_exp; exp++) {
			for(int user1:dataset_compacted.keySet()) {
				BitSet p1 = dataset_compacted.get(user1);
				for(int user2:dataset_compacted.keySet()) {
					compBitSet(p1, dataset_compacted.get(user2));
				}
			}
		}
		long stopBS = System.currentTimeMillis();
		System.out.println("BS " + (stopBS-startBS));
		
	}
	
	public static double compHM(Set<Integer> p1, Set<Integer> p2) {
		double score = 0;
//		Set<Integer> union = new HashSet<Integer>();
		for(int item1: p1) {
//			union.add(item1);
			for(int item2: p2) {
				if(item1==item2) {
					score++;
				}
			}
		}
//		for(int item2: p2) {
//			union.add(item2);
//		}
		if (score != 0) {
//			score = score / ((double) (union.size()));
			score = score / ((double) (p1.size() + p2.size() - score));
		}
		return score;
	}
	
	public static double compBitSet(BitSet p1, BitSet p2) {
		double score = 0;
		BitSet inter = (BitSet) p1.clone();
		inter.and(p2);
		int intercardinality = inter.cardinality();
		score = ((double) intercardinality) / ((double) (p1.cardinality() + p2.cardinality() - intercardinality));
		return score;
	}
	
	public static double compRawBitSet(boolean[] p1, boolean[] p2) {
		int size = p1.length;
		double score = 0;
		int s1 = 0;
		int s2 = 0;
//		for(int i = 0; i < size; i++) {
//			if(p1[i]){
//				s1++;
//				if(p2[i]){
//					s2++;
//					score++;
//				}
//			}
//			else{
//				if(p2[i]){
//					s2++;
//				}
//			}
//		}
		for(int i = 0; i < size; i++) {
			if(p1[i]) s1++;
		}
		for(int i = 0; i < size; i++) {
			if(p2[i]) s2++;
		}
		for(int i = 0; i < size; i++) {
			if(p1[i]&&p2[i]) score++;
		}
		score = score / ((double) (s1 + s2 - score));
		return score;
	}
	
	

}
