package main;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import dataset.Dataset;
import dataset.DatasetBitArray;
//import dataset.DatasetHM;
import dataset.DatasetBitArrayNativeSize;

public class TestBitArrayNative {

	public static void main(String[] args) throws IOException {
		
		String datasetName = "movielens1M";
//		String datasetName = "testset";
		
		int nb_bits = 4096;
//		Dataset dataset0 = new DatasetHM("./Files/Datasets/" + datasetName + "/TestSet0.data",0.0);
		Dataset dataset1 = new DatasetBitArray("./Files/Datasets/movielens1M/TestSet0.data",nb_bits,0.0);
		Dataset dataset3 = new DatasetBitArrayNativeSize("./Files/Datasets/" + datasetName + "/TestSet0.data",nb_bits,0.0);
		
//		System.out.println(dataset0.sim(1, 0));
//		System.out.println(dataset3.sim(1, 0));
//		System.out.println(dataset0.sim(1, 5343));
//		System.out.println(dataset3.sim(1, 5343));
		
		
		int i = 0;
		
//		System.out.println(dataset3.getUsers().equals(dataset1.getUsers()));
		
		for(int user1: dataset3.getUsers()) {
//			if(dataset3.getRatedItems(user1).equals(dataset1.getRatedItems(user1))) {
//				System.out.println("problem 3");
//			}
			for(int user2: dataset3.getUsers()) {
				if (user1 < user2) {
					i++;
					double sim1 = dataset1.sim(user1, user2);
					double sim2 = dataset3.sim(user1, user2);
					double sim3 = dataset3.sim(user2, user1);
//					if(user1 == 1 && user2 == 456) {
//						System.out.println(user1);
//						System.out.println(user2);
//						System.out.println(sim1);
//						System.out.println(sim2);
//						System.out.println(sim3);
//						System.out.println(dataset0.sim(user1, user2));
//					}
					if (sim1 != sim2) {
						System.out.println("problem 1");
						System.out.println(user1);
						System.out.println(user2);
						System.out.println(sim1);
						System.out.println(sim2);
						System.out.println(sim3);
					}
					if (sim3 != sim2) {
						System.out.println("problem 2");
						System.out.println(user1);
						System.out.println(user2);
						System.out.println(sim1);
						System.out.println(sim2);
						System.out.println(sim3);
					}
				}
			}
		}
		System.out.println(i + " done");
		
	}
	
	
	
	
	

	public static Set<Integer> getRatedItems(long[] bitset) {
		Set<Integer> set = new HashSet<Integer>();
		for(int indexbitset = 0; indexbitset < bitset.length; indexbitset++) {
			for(int indexlong = 0; indexlong < 64; indexlong++) {
				if((bitset[indexbitset] & (((long) 1)<<indexlong)) != 0) {
//					System.out.println(indexbitset);
//					System.out.println(indexlong);
//					System.out.println((indexbitset * 64) + indexlong);
					set.add((indexbitset * 64) + indexlong);
				}
			}	
		}
		return set;
	}
	
}
