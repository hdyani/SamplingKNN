package main;

import java.io.IOException;

import dataset.DatasetHM;
import dataset.DatasetBBMH;
//import parameters.ParametersHyrec;
import parameters.ParametersLSH;
//import util.IntDoublePair;
//import util.KNN;
//import dataset.Dataset;
//import algo.Hyrec.Hyrec;
import algo.LSH.LSH;
import parameters.ParametersStats;
import io.IO;
import util.KNNGraph;
public final class TestDatasetBBMHprintload {

	public static void main(String[] args) throws IOException {

		String input = "./Files/Datasets/AmazonMovies20min/TestSet0.data";
		
		
//		DatasetBBMH dataset = new DatasetBBMH(input,0,4,256);
//		dataset.toFile("essai256");
		
		DatasetHM datasetHM = new DatasetHM(input,0);
		DatasetBBMH dataset_ = new DatasetBBMH(0,4,256, "essai256");
		

//		ParametersHyrec params = new ParametersHyrec(30, "AmazonMovies20min",dataset_,5);
		ParametersLSH params = new ParametersLSH(30, "AmazonMovies20min",datasetHM,5);
		params.set_nb_hash(10);
		params.set_datasetHash(dataset_);
		
		ParametersStats paramstats = new ParametersStats(true,true);
//		Hyrec hyrec = new Hyrec(params, paramstats);
//		KNNGraph knng = hyrec.doKNN();
		LSH lsh = new LSH(params, paramstats);
		KNNGraph knng = lsh.doKNN();
		try {
			IO.KNNGraphtoJSON(knng, "./Files/results/", "knng_essai_aux_HM.data");
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(paramstats.get_scanrate());
		System.out.println(paramstats.get_time());
//		4852501
//		108538
//		4852501
//		135843

//		HM
//		filling bucket in:
//			68559
//			doing knn in:
//			47564
//			4852501
//			116788
		
//		BBMH:
//		filling bucket in:
//			67302
//			doing knn in:
//			36231
//			4852501
//			104085
		
//		scanrate LSH:           4852501    
//		scanrate bruteforce: 1644425226
//		scanrate Hyrec:       178600393
		
//		int[] random_neighbors = {45952, 47168, 69826, 6915, 36035, 274883, 35015, 75085, 77516, 30735, 37522, 209296, 76500, 70168, 285662, 132121, 271706, 83489, 25890, 75111, 133669, 23724, 121263, 132013, 97463, 77238, 13751, 243316, 215679, 48895};
//		KNN neighbors = new KNN(params.k(), params.dataset().getInitValue()-1);
//		for(int random_user_id: random_neighbors) {
//			
//			neighbors.add(new IntDoublePair(random_user_id, params.dataset().sim(0, random_user_id)));
////			if(ParametersStats.measure_scanrate) {
////				paramStats.inc();
////			}
//		}
//		System.out.println(neighbors.toString());
		
		
	}

	
}
