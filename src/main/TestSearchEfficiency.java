package main;

import java.util.HashSet;
import java.util.Random;

public final class TestSearchEfficiency {

	public static void main(String[] args) {
//		String dataset_add = "./Files/Datasets/movielens1M/TestSet0.data";
		
		int[] k_values = {1,2,5,10,30,50,100,200};
		int nb_search = 100000000;

		Random rand = new Random();
		
		int nb_max = 10000;
		
		
		for (int k : k_values) {
			
			HashSet<Integer> hs = new HashSet<Integer>();
			int[] arr = new int[k];
			for (int index = 0; index < k; index++) {
				int elem = rand.nextInt(nb_max);
				hs.add(elem);
				arr[index] = elem;
			}
			
			int[] searchs = new int[nb_search];
			for (int elem_to_search=0; elem_to_search < nb_search; elem_to_search++) {
				searchs[elem_to_search] = rand.nextInt(nb_max);
			}
			
			long start = System.currentTimeMillis();
			for (int elem:searchs) {
				if(hs.contains(elem)) {
					hs.remove(elem);
					hs.add(elem+40);
				}
			}
			long end = System.currentTimeMillis();
			System.out.println("Time to search "+ nb_search + " in a HashSet of size " + k + ": " + (end-start) + "ms");
			
			start = System.currentTimeMillis();
			for (int elem:searchs) {
				for(int index = 0; index<arr.length; index++) {
					if(arr[index] == elem) {
						break;
					}
				}
			}
			end = System.currentTimeMillis();
			System.out.println("Time to search "+ nb_search + " in an array of size " + k + ": " + (end-start) + "ms");
			
			System.out.println("k=" + k + " done");
			
		}
		
		

	}
}
