package main;

import java.util.HashSet;

import util.Permutations;

public class TestPermutations {

	public static void main(String[] args) {
		Permutations[] permutations = new Permutations[10];
		
		HashSet<Integer> set = new HashSet<Integer>();
		set.add(1);
		set.add(2);
		set.add(3);
		set.add(4);
		set.add(5);
		set.add(6);
		set.add(7);
		set.add(8);
		set.add(9);
		set.add(10);
		
		for(int index=0; index<permutations.length; index++) {
			permutations[index] = new Permutations(set);
		}

		HashSet<Integer> set_ = new HashSet<Integer>();
		set_.add(1);
		set_.add(5);
		set_.add(7);
		
		for(int index=0; index<permutations.length; index++) {
			
			System.out.println(permutations[index].getHash(set_));
			System.out.println(permutations[index].toString());
		}
		

	}

}
