package main;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import dataset.Dataset;
import dataset.DatasetBF;
import dataset.DatasetBitArray;
import dataset.DatasetBitArrayNativeSize;
import dataset.DatasetBitSet;
import dataset.DatasetHM;
import dataset.DatasetHMCosine;
import dataset.DatasetHMFull;
import dataset.DatasetMinMax;
import dataset.DatasetSketch;
import dataset.extend.DatasetItemSamplingConstantProb;
import dataset.extend.DatasetItemSamplingConstantProbCorrected;
import dataset.extend.DatasetItemSamplingConstantProbIter;
import dataset.extend.DatasetItemSamplingConstantProbIterCorrected;
import dataset.extend.DatasetMostLeastPop;
import dataset.extend.DatasetMostLeastPopCorrected;
import dataset.extend.DatasetLeastPop;
import dataset.extend.DatasetMostPop;
import dataset.extend.DatasetSamplingConstantProb;
import dataset.extend.DatasetSamplingConstantProbCorrected;
import dataset.extend.DatasetSamplingConstantSize;
import dataset.extend.DatasetSamplingConstantSizeCorrected;
import dataset.DatasetBBMH;
import parameters.ParametersBruteForce;
import parameters.ParametersHybrid;
import parameters.ParametersHybridOptiMem;
import parameters.ParametersHybrid_MH;
import parameters.ParametersHybrid_MH_rec;
import parameters.ParametersHyrec;
import parameters.ParametersLSH;
import parameters.ParametersLSH_fast_hash;
import parameters.ParametersNNDescent;
import parameters.ParametersStats;
import util.KNNGraph;

public final class KNNmain {

	public static void main(String[] args) {
		parse(args);
	}

	private static void parse(String[] args){
		String outputAdd = "default";
		
		String algo = util.Names.bruteForce;

		String datasetName = "movielens1M";
		int datasetPartition = 0;

		int nb_proc = Runtime.getRuntime().availableProcessors()+1;

		int k = 30;
		int iterations = 30;
		int r = 0;
		double delta = 0.001;
		double rho = 1.0;
		String datastructureSim = util.Names.HashMap;
		int nb_hash = 2;
		int nb_bits = 1024;

		int B = 90;
		int iterations_preprocessing = 30;
		int r_preprocessing = 0;
		double delta_preprocessing = 0.001;
		double rho_preprocessing = 1.0;
		String datastructureSim_preprocessing = util.Names.HashMap;
		int nb_hash_preprocessing = 2;
		int nb_bits_preprocessing = 1024;

//		int nb_bitset = 10;
//		int min_bitset_size = 12;
//		int add_size = 3;
		
		int BBMHb = 2;
		int BBMHhash = 32;
		int BBMHb_preprocessing = 2;
		int BBMHhash_preprocessing = 32;
		
		double prob = 0.5;
		int size = 20;
		
		double initValue = 0;

		int nb_reco = 30;

		boolean time = true;
		boolean scanrate = false;
		boolean reco = true;
		
//		boolean estimB = false;
//		boolean estimNbBitset = false;
		
		String datastructureLSHhash = util.Names.HashMap;
		int LSHhash = 10;
		int LSHhashtry = 10;
		int LSHbuckets = 1024;
		int LSH_ds_BBMHb = 2;
		int LSH_ds_BBMHhash = 32;
		int LSH_ds_nb_bits = 1024;
		double LSH_ds_prob = 0.5;
		int LSH_ds_size = 20;

		Options options = new Options();


		options.addOption("h", "help", false, "Show help.");
		options.addOption(util.Names.optionAlgoShort, util.Names.optionAlgoLong, true, "KNNg Algorithm. Default: " + algo);
		options.addOption(util.Names.optionDatasetShort, util.Names.optionDatasetLong, true, "Dataset to use. Default: " + datasetName);
		options.addOption(util.Names.optionPartitionShort, util.Names.optionPartitionLong, true, "Partition of the dataset to use. Default: " + Integer.toString(datasetPartition));
		options.addOption(util.Names.optionInitValueShort, util.Names.optionInitValueLong, true, "initValue of the dataset to use. Default: " + Double.toString(initValue));
		options.addOption(util.Names.optionOutputAddShort, util.Names.optionOutputAddLong, true, "Filename of the output. Default: " + outputAdd);

		options.addOption(util.Names.optionNbProcShort, util.Names.optionNbProcLong, true, "Number of threads to use. Default: number of processors available + 1");

		options.addOption(util.Names.optionkShort, util.Names.optionkLong, true, "Size of the KNN. Default: " + Integer.toString(k));
		options.addOption(util.Names.optionIterShort, util.Names.optionIterLong, true, "Number of iterations to use, if relevant. Default: " + Integer.toString(iterations));
		options.addOption(util.Names.optionrShort, util.Names.optionrLong, true, "Value of r, if relevant. Default: " + Integer.toString(r));
		options.addOption(util.Names.optionDeltaShort, util.Names.optionDeltaLong, true, "Value of delta, if relevant. Default: " + Double.toString(delta));
		options.addOption(util.Names.optionRhoShort, util.Names.optionRhoLong, true, "Value of rho, if relevant. Default: " + Double.toString(rho));
		options.addOption(util.Names.optionDatastructureShort, util.Names.optionDatastructureLong, true, "Datastructure to use. Default: " + datastructureSim);
		options.addOption(util.Names.optionNbHashShort, util.Names.optionNbHashLong, true, "Number of hash functions to use for the datastructure, if relevant. Default: " + Integer.toString(nb_hash));
		options.addOption(util.Names.optionNbBitsShort, util.Names.optionNbBitsLong, true, "Number of bits to use for the datastructure, if relevant. Default: " + Integer.toString(nb_bits));

		options.addOption(util.Names.optionBShort, util.Names.optionBLong, true, "Size of the KNN for the first algo of Cork. Default: " + Integer.toString(B));
		options.addOption(util.Names.optionIter_Short, util.Names.optionIter_Long, true, "Number of iterations to use for the first algo of Cork, if relevant. Default: " + Integer.toString(iterations_preprocessing));
		options.addOption(util.Names.optionr_Short, util.Names.optionr_Long, true, "Value of r for the first algo of Cork, if relevant. Default: " + Integer.toString(r_preprocessing));
		options.addOption(util.Names.optionDelta_Short, util.Names.optionDelta_Long, true, "Value of delta for the first algo of Cork, if relevant. Default: " + Double.toString(delta_preprocessing));
		options.addOption(util.Names.optionRho_Short, util.Names.optionRho_Long, true, "Value of rho for the first algo of Cork, if relevant. Default: " + Double.toString(rho_preprocessing));
		options.addOption(util.Names.optionDatastructure_Short, util.Names.optionDatastructure_Long, true, "Datastructure to use for the first algo of Cork, if relevant. Default: " + datastructureSim_preprocessing);
		options.addOption(util.Names.optionNbHash_Short, util.Names.optionNbHash_Long, true, "Number of hash functions to use for the datastructure for the first algo of Cork, if relevant. Default: " + Integer.toString(nb_hash_preprocessing));
		options.addOption(util.Names.optionNbBits_Short, util.Names.optionNbBits_Long, true, "Number of bits to use for the datastructure for the first algo of Cork, if relevant. Default: " + Integer.toString(nb_bits_preprocessing));
	

		options.addOption(util.Names.optionTimeShort, util.Names.optionTimeLong, false, "Desactivate the measure of the execution time.");
		options.addOption(util.Names.optionScanrateShort, util.Names.optionScanrateLong, false, "Activate the measure of the scanrate");
		options.addOption(util.Names.optionRecoShort, util.Names.optionRecoLong, false, "Desactivate the recommendations");

		options.addOption(util.Names.optionLSHDatastructureShort, util.Names.optionLSHDatastructureLong, true, "Datastructure to use for comparison in LSH. Default: " + datastructureLSHhash);
		options.addOption(util.Names.optionLSHNbHashShort, util.Names.optionLSHNbHashLong, true, "Number of hash functions to use for LSH, if relevant. Default: " + Integer.toString(LSHhash));
		options.addOption(util.Names.optionLSHNbBucketsShort, util.Names.optionLSHNbBucketsLong, true, "Number of buckets to use for LSH, if relevant. Default: " + Integer.toString(LSHbuckets));
		options.addOption(util.Names.optionLSHNbHashTryShort, util.Names.optionLSHNbHashTryLong, true, "Number of hash functions to use to find the best hash functions for LSH, if relevant. Default: " + Integer.toString(LSHhashtry));
		
		options.addOption(util.Names.optionLSH_ds_NbBitsShort, util.Names.optionLSH_ds_NbBitsLong, true, "Nb_bits for datastructure (e.g. goldfinger) used to fill buckets in LSH. Default: " + LSH_ds_nb_bits);
		options.addOption(util.Names.optionLSH_ds_BBMHbShort, util.Names.optionLSH_ds_BBMHbLong, true, "b value for BBMH datastructure used to fill buckets in LSH. Default: " + LSH_ds_BBMHb);
		options.addOption(util.Names.optionLSH_ds_BBMHNbHashShort, util.Names.optionLSH_ds_BBMHNbHashLong, true, "nb hash for BBMH datastructure used to fill buckets in LSH. Default: " + LSH_ds_BBMHhash);
		options.addOption(util.Names.optionLSH_ds_ProbShort, util.Names.optionLSH_ds_ProbLong, true, "prob value for datastructure (e.g. ItemSampling) used to fill buckets in LSH. Default: " + LSH_ds_prob);
		options.addOption(util.Names.optionLSH_ds_SizeShort, util.Names.optionLSH_ds_SizeLong, true, "size value for datastructure (e.g. LeastPop) used to fill buckets in LSH. Default: " + LSH_ds_size);
		
		
		options.addOption(util.Names.optionBBMHNbHashShort, util.Names.optionBBMHNbHashLong, true, "Number of hash functions to use for BBMH, if relevant. Default: " + Integer.toString(BBMHhash));
		options.addOption(util.Names.optionBBMHbShort, util.Names.optionBBMHbLong, true, "Number of bits to use for each hash for BBMH, if relevant. Default: " + Integer.toString(BBMHb));
		options.addOption(util.Names.optionBBMHNbHash_Short, util.Names.optionBBMHNbHash_Long, true, "Number of hash functions to use for BBMH, if relevant. Default: " + Integer.toString(BBMHhash_preprocessing));
		options.addOption(util.Names.optionBBMHb_Short, util.Names.optionBBMHb_Long, true, "Number of bits to use for each hash for BBMH, if relevant. Default: " + Integer.toString(BBMHb_preprocessing));
		
		options.addOption(util.Names.optionProbShort, util.Names.optionProbLong, true, "Probability used in data sampling. Default: " + prob);
		options.addOption(util.Names.optionSizeShort, util.Names.optionSizeLong, true, "Size used when shortening profiles. Default: " + Integer.toString(size));

		CommandLineParser parser = new BasicParser();

		CommandLine cmd = null;
		try {
			cmd = parser.parse(options, args);

			if (cmd.hasOption("h"))
				help(options);

			if (cmd.hasOption(util.Names.optionAlgoShort))
				algo = cmd.getOptionValue(util.Names.optionAlgoShort);

			if (cmd.hasOption(util.Names.optionDatasetShort))
				datasetName = cmd.getOptionValue(util.Names.optionDatasetShort);

			if (cmd.hasOption(util.Names.optionPartitionShort))
				datasetPartition = Integer.parseInt(cmd.getOptionValue(util.Names.optionPartitionShort));

			if (cmd.hasOption(util.Names.optionNbProcShort))
				nb_proc = Integer.parseInt(cmd.getOptionValue(util.Names.optionNbProcShort));

			if (cmd.hasOption(util.Names.optionOutputAddShort))
				outputAdd = cmd.getOptionValue(util.Names.optionOutputAddShort);



			if (cmd.hasOption(util.Names.optionkShort))
				k = Integer.parseInt(cmd.getOptionValue(util.Names.optionkShort));

			if (cmd.hasOption(util.Names.optionIterShort))
				iterations = Integer.parseInt(cmd.getOptionValue(util.Names.optionIterShort));

			if (cmd.hasOption(util.Names.optionrShort))
				r = Integer.parseInt(cmd.getOptionValue(util.Names.optionrShort));

			if (cmd.hasOption(util.Names.optionDeltaShort))
				delta = Double.parseDouble(cmd.getOptionValue(util.Names.optionDeltaShort));

			if (cmd.hasOption(util.Names.optionRhoShort))
				rho = Double.parseDouble(cmd.getOptionValue(util.Names.optionRhoShort));

			if (cmd.hasOption(util.Names.optionDatastructureShort))
				datastructureSim = cmd.getOptionValue(util.Names.optionDatastructureShort);

			if (cmd.hasOption(util.Names.optionNbHashShort))
				nb_hash = Integer.parseInt(cmd.getOptionValue(util.Names.optionNbHashShort));

			if (cmd.hasOption(util.Names.optionNbBitsShort))
				nb_bits = Integer.parseInt(cmd.getOptionValue(util.Names.optionNbBitsShort));



			if (cmd.hasOption(util.Names.optionBShort))
				B = Integer.parseInt(cmd.getOptionValue(util.Names.optionBShort));

			if (cmd.hasOption(util.Names.optionIter_Short))
				iterations_preprocessing = Integer.parseInt(cmd.getOptionValue(util.Names.optionIter_Short));

			if (cmd.hasOption(util.Names.optionr_Short))
				r_preprocessing = Integer.parseInt(cmd.getOptionValue(util.Names.optionr_Short));

			if (cmd.hasOption(util.Names.optionDelta_Short))
				delta_preprocessing = Double.parseDouble(cmd.getOptionValue(util.Names.optionDelta_Short));

			if (cmd.hasOption(util.Names.optionRho_Short))
				rho_preprocessing = Double.parseDouble(cmd.getOptionValue(util.Names.optionRho_Short));

			if (cmd.hasOption(util.Names.optionDatastructure_Short))
				datastructureSim_preprocessing = cmd.getOptionValue(util.Names.optionDatastructure_Short);

			if (cmd.hasOption(util.Names.optionNbHash_Short))
				nb_hash_preprocessing = Integer.parseInt(cmd.getOptionValue(util.Names.optionNbHash_Short));

			if (cmd.hasOption(util.Names.optionNbBits_Short))
				nb_bits_preprocessing = Integer.parseInt(cmd.getOptionValue(util.Names.optionNbBits_Short));





			if (cmd.hasOption(util.Names.optionTimeShort))
				time = false;

			if (cmd.hasOption(util.Names.optionScanrateShort))
				scanrate = true;
			
			if (cmd.hasOption(util.Names.optionRecoShort))
				reco = false;
			
			
			if(cmd.hasOption(util.Names.optionLSHDatastructureShort))
				datastructureLSHhash = cmd.getOptionValue(util.Names.optionLSHDatastructureShort);
			if(cmd.hasOption(util.Names.optionLSHNbBucketsShort))
				LSHbuckets = Integer.parseInt(cmd.getOptionValue(util.Names.optionLSHNbBucketsShort));
			if(cmd.hasOption(util.Names.optionLSHNbHashShort))
				LSHhash = Integer.parseInt(cmd.getOptionValue(util.Names.optionLSHNbHashShort));
			if(cmd.hasOption(util.Names.optionLSHNbHashTryShort))
				LSHhashtry = Integer.parseInt(cmd.getOptionValue(util.Names.optionLSHNbHashTryShort));
			
			if(cmd.hasOption(util.Names.optionLSH_ds_NbBitsShort))
				LSH_ds_nb_bits = Integer.parseInt(cmd.getOptionValue(util.Names.optionLSH_ds_NbBitsShort));
			if(cmd.hasOption(util.Names.optionLSH_ds_BBMHbShort))
				LSH_ds_BBMHb = Integer.parseInt(cmd.getOptionValue(util.Names.optionLSH_ds_BBMHbShort));
			if(cmd.hasOption(util.Names.optionLSH_ds_BBMHNbHashShort))
				LSH_ds_BBMHhash = Integer.parseInt(cmd.getOptionValue(util.Names.optionLSH_ds_BBMHNbHashShort));
			if(cmd.hasOption(util.Names.optionLSH_ds_ProbShort))
				LSH_ds_prob = Integer.parseInt(cmd.getOptionValue(util.Names.optionLSH_ds_ProbShort));
			if(cmd.hasOption(util.Names.optionLSH_ds_SizeShort))
				LSH_ds_size = Integer.parseInt(cmd.getOptionValue(util.Names.optionLSH_ds_SizeShort));
			
			
			
			
			
			if(cmd.hasOption(util.Names.optionBBMHNbHashShort))
				BBMHhash = Integer.parseInt(cmd.getOptionValue(util.Names.optionBBMHNbHashShort));
			if(cmd.hasOption(util.Names.optionBBMHbShort))
				BBMHb = Integer.parseInt(cmd.getOptionValue(util.Names.optionBBMHbShort));
			
			if(cmd.hasOption(util.Names.optionBBMHNbHash_Short))
				BBMHhash_preprocessing = Integer.parseInt(cmd.getOptionValue(util.Names.optionBBMHNbHash_Short));
			if(cmd.hasOption(util.Names.optionBBMHb_Short))
				BBMHb_preprocessing = Integer.parseInt(cmd.getOptionValue(util.Names.optionBBMHb_Short));
			
			
			if(cmd.hasOption(util.Names.optionProbShort))
				prob = Double.parseDouble(cmd.getOptionValue(util.Names.optionProbShort));
			if(cmd.hasOption(util.Names.optionSizeShort))
				size = Integer.parseInt(cmd.getOptionValue(util.Names.optionSizeShort));

			final Dataset datasetSim = initDataset(datastructureSim, datasetName, datasetPartition, nb_bits, nb_hash, BBMHb, BBMHhash, prob, size, initValue);
			final ParametersStats paramStat = new ParametersStats(time,scanrate);





			String Add = "./Files/results/" + outputAdd + "/" + datasetName + "/" + algo + "/k" + Integer.toString(k) + "/" + getTime() + "/";
			String dataName = datasetName + Integer.toString(datasetPartition);
			
			KNNGraph knng;
			
			
			final Dataset datasetSim_preprocessing;
			final ParametersStats paramStat_preprocessing;
			if (algo.startsWith(util.Names.cork) || algo.equals(util.Names.hyrec+util.Names.cork)) {
				datasetSim_preprocessing = initDataset(datastructureSim_preprocessing, datasetName, datasetPartition, nb_bits_preprocessing, nb_hash_preprocessing, BBMHb_preprocessing, BBMHhash_preprocessing, prob, size, initValue);
				paramStat_preprocessing = new ParametersStats(time,scanrate);
			}
			else {
				datasetSim_preprocessing = null;
				paramStat_preprocessing = null;
			}
			
			switch (algo) {
			case (util.Names.bruteForce): {
				ParametersBruteForce params = new ParametersBruteForce(k, dataName, datasetSim, nb_proc);
				knng = launch.LaunchBruteForce.launch(params, paramStat, Add);
			}
			break;
			case (util.Names.random): {
				ParametersBruteForce params = new ParametersBruteForce(k, dataName, datasetSim, nb_proc);
				knng = launch.LaunchRandom.launch(params, paramStat, Add);
			}
			break;
			case (util.Names.hyrec): {
				ParametersHyrec params = new ParametersHyrec(k, dataName, datasetSim, nb_proc);
				params.set_r(r);
				params.set_iterations(iterations);
				params.set_delta(delta);
				knng = launch.LaunchHyrec.launch(params, paramStat, Add);
			}
			break;
			case (util.Names.nndescent): {
				ParametersNNDescent params = new ParametersNNDescent(k, dataName, datasetSim, nb_proc);
				params.set_rho(rho);
				params.set_iterations(iterations);
				params.set_delta(delta);
				knng = launch.LaunchNNDescent.launch(params, paramStat, Add);
			}
			break;
				
			case (util.Names.LSH): {
				ParametersLSH params = new ParametersLSH(k, dataName, datasetSim, nb_proc);
				params.set_nb_hash(LSHhash);
				if((datastructureSim != datastructureLSHhash) /*&& (datastructure_ != datastructureLSH) */) {
					params.set_datasetHash(initDataset(datastructureLSHhash, datasetName, datasetPartition, LSH_ds_nb_bits, nb_hash_preprocessing, LSH_ds_BBMHb, LSH_ds_BBMHhash, LSH_ds_prob, LSH_ds_size, initValue));
				}
//				if((datastructure != datastructureLSH) && (datastructure_ == datastructureLSH) && cmd.hasOption(util.Names.optionDatastructure_Short)) {
//					params.set_dataset2(dataset_);
//				}
				knng = launch.LaunchLSH.launch(params, paramStat, Add);
				}
			break;
				
			case (util.Names.cork+util.Names.LSH+util.Names.bruteForce): {
				ParametersBruteForce params = new ParametersBruteForce(B, dataName, datasetSim, nb_proc);
				ParametersLSH params_preprocessing = new ParametersLSH(k, dataName, datasetSim_preprocessing, nb_proc);
				params_preprocessing.set_nb_hash(LSHhash);
				if((datastructureSim_preprocessing != datastructureLSHhash)) {
					if((datastructureSim == datastructureLSHhash)) {
						params_preprocessing.set_datasetHash(datasetSim);
					}
					else {
						params_preprocessing.set_datasetHash(initDataset(datastructureLSHhash, datasetName, datasetPartition, LSH_ds_nb_bits, 0, LSH_ds_BBMHb, LSH_ds_BBMHhash, LSH_ds_prob, LSH_ds_size, initValue));
					}
//					params_preprocessing.set_dataset2(initDataset(datastructureLSHhash, datasetName, datasetPartition, nb_bits_preprocessing, nb_hash_preprocessing, BBMHb_preprocessing, BBMHhash_preprocessing, prob, size, initValue));
				}
				knng = launch.LaunchCork.launch(params_preprocessing, paramStat_preprocessing, params, paramStat, Add);
				}
			break;
				
			case (util.Names.cork+util.Names.LSH_fast_hash+util.Names.bruteForce): {
				ParametersBruteForce params = new ParametersBruteForce(B, dataName, datasetSim, nb_proc);
				ParametersLSH_fast_hash params_preprocessing = new ParametersLSH_fast_hash(k, dataName, datasetSim_preprocessing, nb_proc);
				params_preprocessing.set_nb_hash(LSHhash);
				params_preprocessing.set_nb_buckets(LSHbuckets);
				if((datastructureSim_preprocessing != datastructureLSHhash)) {
					if((datastructureSim == datastructureLSHhash)) {
						params_preprocessing.set_datasetHash(datasetSim);
					}
					else {
						params_preprocessing.set_datasetHash(initDataset(datastructureLSHhash, datasetName, datasetPartition, LSH_ds_nb_bits, 0, LSH_ds_BBMHb, LSH_ds_BBMHhash, LSH_ds_prob, LSH_ds_size, initValue));
					}
//					params_preprocessing.set_dataset2(initDataset(datastructureLSHhash, datasetName, datasetPartition, nb_bits_preprocessing, nb_hash_preprocessing, BBMHb_preprocessing, BBMHhash_preprocessing, prob, size, initValue));
				}
				knng = launch.LaunchCork.launch(params_preprocessing, paramStat_preprocessing, params, paramStat, Add);
				}
			break;
				
			case (util.Names.cork+util.Names.Hybrid_first_version+util.Names.bruteForce): {
				ParametersBruteForce params = new ParametersBruteForce(B, dataName, datasetSim, nb_proc);
				ParametersHybrid params_preprocessing = new ParametersHybrid(k, dataName, datasetSim_preprocessing, nb_proc);
				params_preprocessing.set_nb_hash(LSHhash);
				params_preprocessing.set_nb_bits(LSHbuckets);
				if((datastructureSim_preprocessing != datastructureLSHhash)) {
					if((datastructureSim == datastructureLSHhash)) {
						params_preprocessing.set_datasetHash(datasetSim);
					}
					else {
						params_preprocessing.set_datasetHash(initDataset(datastructureLSHhash, datasetName, datasetPartition, LSH_ds_nb_bits, 0, LSH_ds_BBMHb, LSH_ds_BBMHhash, LSH_ds_prob, LSH_ds_size, initValue));
					}
				}
				knng = launch.LaunchCork.launch(params_preprocessing, paramStat_preprocessing, params, paramStat, Add);
			}
			break;

			case (util.Names.cork+util.Names.Hybrid_MH+util.Names.bruteForce): {
				ParametersBruteForce params = new ParametersBruteForce(B, dataName, datasetSim, nb_proc);
				ParametersHybrid_MH params_preprocessing = new ParametersHybrid_MH(k, dataName, datasetSim_preprocessing, nb_proc);
				params_preprocessing.set_nb_hash(LSHhash);
				params_preprocessing.set_nb_hash_try(LSHhashtry);
				params_preprocessing.set_nb_bits(LSHbuckets);
				if((datastructureSim_preprocessing != datastructureLSHhash)) {
					if((datastructureSim == datastructureLSHhash)) {
						params_preprocessing.set_datasetHash(datasetSim);
					}
					else {
//					params_.set_dataset2(initDataset(datastructureLSH, datasetName, datasetPartition, nb_bits_, nb_hash_, nb_bitset, min_bitset_size, add_size, BBMHb_, BBMHhash_, prob, size, initValue));
						params_preprocessing.set_datasetHash(initDataset(datastructureLSHhash, datasetName, datasetPartition, LSH_ds_nb_bits, 0, LSH_ds_BBMHb, LSH_ds_BBMHhash, LSH_ds_prob, LSH_ds_size, initValue));
					}
				}
				knng = launch.LaunchCork.launch(params_preprocessing, paramStat_preprocessing, params, paramStat, Add);
			}
			break;

			case (util.Names.cork+util.Names.Hybrid_MH_TP+util.Names.bruteForce): {
				ParametersBruteForce params = new ParametersBruteForce(B, dataName, datasetSim, nb_proc);
				ParametersHybrid_MH params_preprocessing = new ParametersHybrid_MH(k, dataName, datasetSim_preprocessing, nb_proc);
				params_preprocessing.set_nb_hash(LSHhash);
				params_preprocessing.set_nb_hash_try(LSHhashtry);
				params_preprocessing.set_nb_bits(LSHbuckets);
				if((datastructureSim_preprocessing != datastructureLSHhash)) {
					if((datastructureSim == datastructureLSHhash)) {
						params_preprocessing.set_datasetHash(datasetSim);
					}
					else {
//					params_.set_dataset2(initDataset(datastructureLSH, datasetName, datasetPartition, nb_bits_, nb_hash_, nb_bitset, min_bitset_size, add_size, BBMHb_, BBMHhash_, prob, size, initValue));
						params_preprocessing.set_datasetHash(initDataset(datastructureLSHhash, datasetName, datasetPartition, LSH_ds_nb_bits, 0, LSH_ds_BBMHb, LSH_ds_BBMHhash, LSH_ds_prob, LSH_ds_size, initValue));
					}
				}
				knng = launch.LaunchCork.launch_Hybrid_TP(params_preprocessing, paramStat_preprocessing, params, paramStat, Add);
			}
			break;

			case (util.Names.cork+util.Names.ClusterAndConquer+util.Names.bruteForce): {
				ParametersBruteForce params = new ParametersBruteForce(B, dataName, datasetSim, nb_proc);
				ParametersHybrid_MH_rec params_preprocessing = new ParametersHybrid_MH_rec(k, dataName, datasetSim_preprocessing, nb_proc);
				params_preprocessing.set_nb_hash(LSHhash);
				params_preprocessing.set_nb_hash_try(LSHhashtry);
				params_preprocessing.set_nb_bits(LSHbuckets);
				if((datastructureSim_preprocessing != datastructureLSHhash)) {
					if((datastructureSim == datastructureLSHhash)) {
						params_preprocessing.set_datasetHash(datasetSim);
					}
					else {
//					params_.set_dataset2(initDataset(datastructureLSH, datasetName, datasetPartition, nb_bits_, nb_hash_, nb_bitset, min_bitset_size, add_size, BBMHb_, BBMHhash_, prob, size, initValue));
						params_preprocessing.set_datasetHash(initDataset(datastructureLSHhash, datasetName, datasetPartition, LSH_ds_nb_bits, 0, LSH_ds_BBMHb, LSH_ds_BBMHhash, LSH_ds_prob, LSH_ds_size, initValue));
					}
				}
				knng = launch.LaunchCork.launch(params_preprocessing, paramStat_preprocessing, params, paramStat, Add);
			}
			break;
			case (util.Names.cork+util.Names.ClusterAndConquer_rec+util.Names.bruteForce): {
				ParametersBruteForce params = new ParametersBruteForce(B, dataName, datasetSim, nb_proc);
				ParametersHybrid_MH_rec params_preprocessing = new ParametersHybrid_MH_rec(k, dataName, datasetSim_preprocessing, nb_proc);
				params_preprocessing.set_nb_hash(LSHhash);
				params_preprocessing.set_nb_hash_try(LSHhashtry);
				params_preprocessing.set_nb_bits(LSHbuckets);
				if((datastructureSim_preprocessing != datastructureLSHhash)) {
					if((datastructureSim == datastructureLSHhash)) {
						params_preprocessing.set_datasetHash(datasetSim);
					}
					else {
//					params_.set_dataset2(initDataset(datastructureLSH, datasetName, datasetPartition, nb_bits_, nb_hash_, nb_bitset, min_bitset_size, add_size, BBMHb_, BBMHhash_, prob, size, initValue));
						params_preprocessing.set_datasetHash(initDataset(datastructureLSHhash, datasetName, datasetPartition, LSH_ds_nb_bits, 0, LSH_ds_BBMHb, LSH_ds_BBMHhash, LSH_ds_prob, LSH_ds_size, initValue));
					}
				}
				knng = launch.LaunchCork.launchCC_rec(params_preprocessing, paramStat_preprocessing, params, paramStat, Add);
			}
			break;

			case (util.Names.cork+util.Names.HybridOptiMem+util.Names.bruteForce): {
				ParametersBruteForce params = new ParametersBruteForce(B, dataName, datasetSim, nb_proc);
				ParametersHybridOptiMem params_preprocessing = new ParametersHybridOptiMem(k, dataName, (DatasetBitArrayNativeSize) datasetSim_preprocessing, nb_proc);
				params_preprocessing.set_nb_hash(LSHhash);
				params_preprocessing.set_nb_hash_try(LSHhashtry);
				params_preprocessing.set_nb_bits(LSHbuckets);
				if((datastructureSim_preprocessing != datastructureLSHhash)) {
					if((datastructureSim == datastructureLSHhash)) {
						params_preprocessing.set_datasetHash(datasetSim);
					}
					else {
//					params_.set_dataset2(initDataset(datastructureLSH, datasetName, datasetPartition, nb_bits_, nb_hash_, nb_bitset, min_bitset_size, add_size, BBMHb_, BBMHhash_, prob, size, initValue));
						params_preprocessing.set_datasetHash(initDataset(datastructureLSHhash, datasetName, datasetPartition, LSH_ds_nb_bits, 0, LSH_ds_BBMHb, LSH_ds_BBMHhash, LSH_ds_prob, LSH_ds_size, initValue));
					}
				}
				knng = launch.LaunchCork.launch(params_preprocessing, paramStat_preprocessing, params, paramStat, Add);
			}
			break;

			case (util.Names.cork+util.Names.Hybrid_MH_LSHhash+util.Names.bruteForce): {
				ParametersBruteForce params = new ParametersBruteForce(B, dataName, datasetSim, nb_proc);
				ParametersHybrid_MH params_preprocessing = new ParametersHybrid_MH(k, dataName, datasetSim_preprocessing, nb_proc);
				params_preprocessing.set_nb_hash(LSHhash);
				params_preprocessing.set_nb_hash_try(LSHhashtry);
				params_preprocessing.set_nb_bits(LSHbuckets);
				if((datastructureSim_preprocessing != datastructureLSHhash)) {
					if((datastructureSim == datastructureLSHhash)) {
						params_preprocessing.set_datasetHash(datasetSim);
					}
					else {
//					params_.set_dataset2(initDataset(datastructureLSH, datasetName, datasetPartition, nb_bits_, nb_hash_, nb_bitset, min_bitset_size, add_size, BBMHb_, BBMHhash_, prob, size, initValue));
						params_preprocessing.set_datasetHash(initDataset(datastructureLSHhash, datasetName, datasetPartition, LSH_ds_nb_bits, 0, LSH_ds_BBMHb, LSH_ds_BBMHhash, LSH_ds_prob, LSH_ds_size, initValue));
					}
				}
				knng = launch.LaunchCork.launch_Hybrid_MH_LSHhash(params_preprocessing, paramStat_preprocessing, params, paramStat, Add);
			}

			break;
			case (util.Names.cork+util.Names.Hybrid_MH_ind_buckets+util.Names.bruteForce): {
				ParametersBruteForce params = new ParametersBruteForce(B, dataName, datasetSim, nb_proc);
				ParametersHybrid_MH params_preprocessing = new ParametersHybrid_MH(k, dataName, datasetSim_preprocessing, nb_proc);
				params_preprocessing.set_nb_hash(LSHhash);
				params_preprocessing.set_nb_hash_try(LSHhashtry);
				params_preprocessing.set_nb_bits(LSHbuckets);
				if((datastructureSim_preprocessing != datastructureLSHhash)) {
					if((datastructureSim == datastructureLSHhash)) {
						params_preprocessing.set_datasetHash(datasetSim);
					}
					else {
//					params_.set_dataset2(initDataset(datastructureLSH, datasetName, datasetPartition, nb_bits_, nb_hash_, nb_bitset, min_bitset_size, add_size, BBMHb_, BBMHhash_, prob, size, initValue));
						params_preprocessing.set_datasetHash(initDataset(datastructureLSHhash, datasetName, datasetPartition, LSH_ds_nb_bits, 0, LSH_ds_BBMHb, LSH_ds_BBMHhash, LSH_ds_prob, LSH_ds_size, initValue));
					}
				}
				knng = launch.LaunchCork.launch_Hybrid_MH_ind_buckets(params_preprocessing, paramStat_preprocessing, params, paramStat, Add);
			}
			break;


			case (util.Names.cork+util.Names.ClusterAndConquer_LSHhash+util.Names.bruteForce): {
				ParametersBruteForce params = new ParametersBruteForce(B, dataName, datasetSim, nb_proc);
				ParametersHybrid_MH_rec params_preprocessing = new ParametersHybrid_MH_rec(k, dataName, datasetSim_preprocessing, nb_proc);
				params_preprocessing.set_nb_hash(LSHhash);
				params_preprocessing.set_nb_hash_try(LSHhashtry);
				params_preprocessing.set_nb_bits(LSHbuckets);
				if((datastructureSim_preprocessing != datastructureLSHhash)) {
					if((datastructureSim == datastructureLSHhash)) {
						params_preprocessing.set_datasetHash(datasetSim);
					}
					else {
//					params_.set_dataset2(initDataset(datastructureLSH, datasetName, datasetPartition, nb_bits_, nb_hash_, nb_bitset, min_bitset_size, add_size, BBMHb_, BBMHhash_, prob, size, initValue));
						params_preprocessing.set_datasetHash(initDataset(datastructureLSHhash, datasetName, datasetPartition, LSH_ds_nb_bits, 0, LSH_ds_BBMHb, LSH_ds_BBMHhash, LSH_ds_prob, LSH_ds_size, initValue));
					}
				}
				knng = launch.LaunchCork.launch_ClusterAndConquer_LSHhash(params_preprocessing, paramStat_preprocessing, params, paramStat, Add);
			}

			break;
			case (util.Names.cork+util.Names.ClusterAndConquer_ind_buckets+util.Names.bruteForce): {
				ParametersBruteForce params = new ParametersBruteForce(B, dataName, datasetSim, nb_proc);
				ParametersHybrid_MH params_preprocessing = new ParametersHybrid_MH(k, dataName, datasetSim_preprocessing, nb_proc);
				params_preprocessing.set_nb_hash(LSHhash);
				params_preprocessing.set_nb_hash_try(LSHhashtry);
				params_preprocessing.set_nb_bits(LSHbuckets);
				if((datastructureSim_preprocessing != datastructureLSHhash)) {
					if((datastructureSim == datastructureLSHhash)) {
						params_preprocessing.set_datasetHash(datasetSim);
					}
					else {
//					params_.set_dataset2(initDataset(datastructureLSH, datasetName, datasetPartition, nb_bits_, nb_hash_, nb_bitset, min_bitset_size, add_size, BBMHb_, BBMHhash_, prob, size, initValue));
						params_preprocessing.set_datasetHash(initDataset(datastructureLSHhash, datasetName, datasetPartition, LSH_ds_nb_bits, 0, LSH_ds_BBMHb, LSH_ds_BBMHhash, LSH_ds_prob, LSH_ds_size, initValue));
					}
				}
				knng = launch.LaunchCork.launch_ClusterAndConquer_ind_buckets(params_preprocessing, paramStat_preprocessing, params, paramStat, Add);
			}
			break;
			
			
			case (util.Names.cork+util.Names.bruteForce+util.Names.bruteForce): {
				ParametersBruteForce params = new ParametersBruteForce(k, dataName, datasetSim, nb_proc);
				ParametersBruteForce params_preprocessing = new ParametersBruteForce(B, dataName, datasetSim_preprocessing, nb_proc);
				knng = launch.LaunchCork.launch(params_preprocessing, paramStat_preprocessing, params, paramStat, Add);
			}
			break;
			case (util.Names.cork+util.Names.hyrec+util.Names.bruteForce): {
				ParametersBruteForce params = new ParametersBruteForce(k, dataName, datasetSim, nb_proc);
				ParametersHyrec params_preprocessing = new ParametersHyrec(B, dataName, datasetSim_preprocessing, nb_proc);
				params_preprocessing.set_r(r_preprocessing);
				params_preprocessing.set_iterations(iterations_preprocessing);
				params_preprocessing.set_delta(delta_preprocessing);
				knng = launch.LaunchCork.launch(params_preprocessing, paramStat_preprocessing, params, paramStat, Add);
			}
			break;
			case (util.Names.cork+util.Names.nndescent+util.Names.bruteForce): {
				ParametersBruteForce params = new ParametersBruteForce(k, dataName, datasetSim, nb_proc);
				ParametersNNDescent params_preprocessing = new ParametersNNDescent(B, dataName, datasetSim_preprocessing, nb_proc);
				params_preprocessing.set_rho(rho_preprocessing);
				params_preprocessing.set_iterations(iterations_preprocessing);
				params_preprocessing.set_delta(delta_preprocessing);
				knng = launch.LaunchCork.launch(params_preprocessing, paramStat_preprocessing, params, paramStat, Add);
			}
			break;
			case (util.Names.cork+util.Names.random+util.Names.bruteForce): {
				ParametersBruteForce params = new ParametersBruteForce(k, dataName, datasetSim, nb_proc);
				ParametersBruteForce params_preprocessing = new ParametersBruteForce(B, dataName, datasetSim_preprocessing, nb_proc);
				knng = launch.LaunchCork.launchRandom(params_preprocessing, paramStat_preprocessing, params, paramStat, Add);
			}
			break;
				
				default: knng = new KNNGraph(k,datasetSim.getUsers().size(),initValue);
			}
			
			if (reco) {
				Dataset ds_reco = initDataset(util.Names.HashMapFull, datasetName, datasetPartition, 0, 0, 0, 0, 0, 0, initValue);
				recommendations.RecoWeightedAverage.launch(ds_reco, knng, nb_reco, nb_proc, Add);
			}



		} catch (ParseException | IOException e) {
			e.printStackTrace();
			help(options);
		}
	}




	private static void help(Options options) {
		// This prints out some help
		HelpFormatter formater = new HelpFormatter();

		formater.printHelp("Main", options);
		System.exit(0);
	}

	private static Dataset initDataset(String datastructure, String datasetName, int datasetPartition, int nb_bits, int nb_hash, int BBMHb, int BBMHhash, double prob, int size, double initValue) throws IOException {
//	private static Dataset initDataset(String datastructure, String datasetName, int datasetPartition, int nb_bits, int nb_hash, int nb_bitset, int min_bitset_size, int add_size, int BBMHb, int BBMHhash, double prob, int size, double initValue) throws IOException {
		Dataset dataset;
		switch(datastructure) {
		case util.Names.HashMap: dataset = new DatasetHM("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", initValue);
		break;
		case util.Names.HashMapFull: dataset = new DatasetHMFull("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", initValue);
		break;
		case util.Names.HashMapCosine: dataset = new DatasetHMCosine("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", initValue);
		break;
		case util.Names.Sketch: dataset = new DatasetSketch("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", nb_bits, nb_hash, initValue);
		break;
		case util.Names.BitArray: dataset = new DatasetBitArray("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", nb_bits, initValue);
		break;
		case util.Names.BitArrayNative: dataset = new DatasetBitArrayNativeSize("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", nb_bits, initValue);
		break;
		case util.Names.BloomFilter: dataset = new DatasetBF("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", nb_bits, nb_hash, initValue);
		break;
		case util.Names.MinMax: dataset = new DatasetMinMax("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", initValue);
		break;
		case util.Names.BBMH: dataset = new DatasetBBMH("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", initValue, BBMHb, BBMHhash);
		break;
		case util.Names.BitSet: dataset = new DatasetBitSet("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", nb_bits, initValue);
		break;
		

		case util.Names.ISCP: dataset = new DatasetItemSamplingConstantProb("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", prob, initValue);
		break;
		case util.Names.ISCPC: dataset = new DatasetItemSamplingConstantProbCorrected("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", prob, initValue);
		break;
		case util.Names.ISCPI: dataset = new DatasetItemSamplingConstantProbIter("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", prob, initValue);
		break;
		case util.Names.ISCPIC: dataset = new DatasetItemSamplingConstantProbIterCorrected("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", prob, initValue);
		break;
		case util.Names.MLP: dataset = new DatasetMostLeastPop("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", size, initValue);
		break;
		case util.Names.MLPC: dataset = new DatasetMostLeastPopCorrected("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", size, initValue);
		break;
		case util.Names.SCP: dataset = new DatasetSamplingConstantProb("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", prob, initValue);
		break;
		case util.Names.SCPC: dataset = new DatasetSamplingConstantProbCorrected("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", prob, initValue);
		break;
		case util.Names.SCS: dataset = new DatasetSamplingConstantSize("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", size, initValue);
		break;
		case util.Names.SCSC: dataset = new DatasetSamplingConstantSizeCorrected("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", size, initValue);
		break;
		case util.Names.MP: dataset = new DatasetMostPop("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", size, initValue);
		break;
		case util.Names.LP: dataset = new DatasetLeastPop("./Files/Datasets/"+datasetName+"/TestSet" + datasetPartition + ".data", size, initValue);
		break;
		
		
		
		
		
		default: dataset = null; throw (new IOException("Incorrect Datastructure"));
		}
		return dataset;
	}
	

	
	public static String getTime() {
		SimpleDateFormat formater = null;
		Date aujourdhui = new Date();
		formater = new SimpleDateFormat("yyyyMMddHHmmss");
		return (formater.format(aujourdhui));
	}
}
