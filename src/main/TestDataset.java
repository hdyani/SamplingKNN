package main;

import java.io.IOException;

import dataset.Dataset;
import dataset.DatasetHM;
//import dataset.extend.DatasetItemSamplingConstantProb;
//import dataset.extend.DatasetItemSamplingConstantProbCorrected;
//import dataset.extend.DatasetItemSamplingConstantProbIter;
//import dataset.extend.DatasetMostLeastPop;
import dataset.extend.DatasetLeastPop;
//import dataset.extend.DatasetSamplingConstantSize;
//import dataset.extend.DatasetSamplingConstantSizeCorrected;
//import dataset.extend.DatasetSamplingConstantProb;
//import dataset.extend.DatasetSamplingConstantProbCorrected;
//import dataset.extend.DatasetSamplingConstantSize;
public final class TestDataset {

	public static void main(String[] args) throws IOException {
//		essai();
//		String input = "./Files/Datasets/LiveJournal/raw/soc-LiveJournal1.txt";
//		String output = "./Files/Datasets/LiveJournal/u.data";
//		String input = "./Files/Datasets/Friendster/raw/com-friendster.ungraph.txt";
//		String output = "./Files/Datasets/Friendster/u.data";
//		String input = "./Files/Datasets/Twitter/raw/twitter_rv.net";
//		String output = "./Files/Datasets/Twitter/u.data";
//		String output = "./Files/Datasets/Twitter/TestSet0.data";
//		String output = "./Files/Datasets/movielens1M/TestSet0.data";
//		String output = "./Files/Datasets/AmazonBooks/u.data";
//		String output = "./Files/Datasets/AmazonMovies/u.data";
//		String output = "./Files/Datasets/DBLP_full/u.data";
		String output = "./Files/Datasets/movielens10M/TestSet0.data";
//		String output = "./Files/Datasets/movielens10M/u.data";
//		String output = "./Files/Datasets/movielens20M/TestSet0.data";
//		String output = "./Files/Datasets/AmazonMovies20min/TestSet0.data";
		
//		dataset.gen.CrossValidationTestSetGen.testset8020gen(output, "./Files/Datasets/Twitter/TestSet");
		
//		int B=10;
//		double prob = 1.0;
		int maxsize = 25;
		
//		long start = System.currentTimeMillis();
		Dataset dataset = new DatasetHM(output,0);
//		Dataset dataset_ = new DatasetSamplingConstantSize(output,maxsize,0);
//		Dataset dataset_ = new DatasetLeastPop(output,maxsize,0);
//		Dataset dataset__ = new DatasetSamplingConstantSizeCorrected(output,maxsize,0);
//		System.out.println("HM: " + (System.currentTimeMillis()-start) + "ms");
//		start = System.currentTimeMillis();
//		Dataset dataset_ = new DatasetMostLeastPop(output,maxsize,0);
////		Dataset dataset_ = new DatasetSamplingConstantProb(output,prob,0);
//		Dataset dataset_ = new DatasetSamplingConstantSize(output,maxsize,0);
//		Dataset dataset_ = new DatasetItemSamplingConstantProb(output,prob,0);
//		Dataset dataset__ = new DatasetItemSamplingConstantProbCorrected(output,prob,0);
//		Dataset dataset_ = new DatasetSamplingConstantProb(output,prob,0);
//		Dataset dataset__ = new DatasetSamplingConstantProbCorrected(output,prob,0);
//		Dataset dataset_ = new DatasetItemSamplingConstantProbIter(output,prob,0);
//		System.out.println("MostLeastPop: " + (System.currentTimeMillis()-start) + "ms");

//		System.out.println(dataset.getRatedItems(1));
//		System.out.println(dataset_.getRatedItems(1));
//
//		System.out.println(dataset.getRatedItems(2));
//		System.out.println(dataset_.getRatedItems(2));
//		System.out.println(dataset.sim(1, 2));
//		System.out.println(dataset_.sim(1, 2));
//		System.out.println(dataset__.sim(1, 2));

//		System.out.println(dataset.getRatedItems(4));
//		System.out.println(dataset_.getRatedItems(4));
//		System.out.println(dataset.sim(1, 4));
//		System.out.println(dataset_.sim(1, 4));
//		System.out.println(dataset__.sim(1, 4));

//		System.out.println(dataset.getRatedItems(5343));
//		System.out.println(dataset_.getRatedItems(5343));
//		System.out.println(dataset.sim(1, 5343));
//		System.out.println(dataset_.sim(1, 5343));
//		System.out.println(dataset__.sim(1, 5343));
		String[] parts = {"0","1","2","3","4"};

		int nb_ratings = 0;
		for (int user: dataset.getUsers()) {
			nb_ratings = nb_ratings + dataset.getRatedItems(user).size();
		}
		
		int nb_ratings_sum=0;
		for(String part: parts) {
			output = "./Files/Datasets/movielens10M/TestSet"+part+".data";
			Dataset dataset_ = new DatasetLeastPop(output,maxsize,0);
			int nb_ratings_ = 0;
			for (int user: dataset.getUsers()) {
				nb_ratings_ = nb_ratings_ + dataset_.getRatedItems(user).size();
			}
			nb_ratings_sum = nb_ratings_sum + nb_ratings_;
			System.out.println(nb_ratings - nb_ratings_);
		}
		nb_ratings_sum = nb_ratings_sum / parts.length;
		System.out.println("av: " + (nb_ratings - nb_ratings_sum));
		
//		start = System.currentTimeMillis();
//		int size = 0;
//		double diff = 0;
//		double diff_ = 0;
//		double sim;
//		double sim_ ;
//		double sim__ ;
//		for(int user: dataset.getUsers()) {
////			size = size + dataset.getRatedItems(user).size();
//			for(int user_: dataset.getUsers()) {
//				if (user < user_) {
//					sim = dataset.sim(user, user_);
//					sim_ = dataset_.sim(user, user_);
//					sim__ = dataset__.sim(user, user_);
//					diff = diff + Math.abs(sim-sim_);
//					diff_ = diff_ + Math.abs(sim-sim__);
//				}
//			}
//		}
//		System.out.println(diff);
//		System.out.println(diff_);
//		System.out.println("HM average size:" + size/dataset.getUsers().size());
//		size = 0;
//		for(int user: dataset.getUsers()) {
//			size = size + dataset_.getRatedItems(user).size();
//		}
//		System.out.println("HM average size:" + size/dataset_.getUsers().size());
		
	}

	
}
