package recommendations;

import dataset.Dataset;
import util.KNNGraph;

public abstract interface Recommandation {
	public KNNGraph doRecommandation(Dataset dataset, KNNGraph knng, int nb_reco);
}
