package recommendations;

import java.util.HashSet;

import dataset.Dataset;
import util.IntDoublePair;
import util.KNNGraph;

public final class RecoWeightedAverageRunnable implements Runnable{
	
	private final Dataset dataset;
	private final KNNGraph knng;
	private final KNNGraph reco;
	private final int nb_reco;
	private final int loop;
	private final int[] users;
	private final int nb_proc;
	
	
	public RecoWeightedAverageRunnable(Dataset dataset, KNNGraph knng, KNNGraph reco, int nb_reco, int nb_proc, int[] users, int loop) {
		this.dataset = dataset;
		this.knng = knng;
		this.reco = reco;
		this.nb_reco = nb_reco;
		this.users = users;
		this.loop = loop;
		this.nb_proc = nb_proc;
	}
	
	public void run() {
		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, nb_proc, loop);
		for(int user:user_ids) {
			reco.init(user, nb_reco, 0);
			HashSet<Integer> candidates_items = new HashSet<Integer>();
			for(int neighbor: knng.get_neighbors_ids(user)) {
				candidates_items.addAll(dataset.getRatedItems(neighbor));
			}
			for(int item: candidates_items) {
				double score = 0;
				double sum_sim = 0;
				for(IntDoublePair idp: knng.get_neighbors(user)) {
					score = score + (idp.score * dataset.getRating(idp.integer, item));
					sum_sim = sum_sim + idp.score;
				}
				if (sum_sim != 0) {
					score = score / sum_sim;
				}
				reco.put(user, item, score);
			}
		}
	}
	
}
