package launch;

import java.io.IOException;
import java.util.Map;

import algo.HyrecFlagsCork.HyrecFlagsCork;
import io.IO;
import parameters.Parameters;
import parameters.ParametersHyrecCork;
import parameters.ParametersStats;
import util.KNNGraph;

public final class LaunchHyrecCork {

	public final static KNNGraph launch(ParametersHyrecCork params, ParametersStats paramStats, String Add) throws IOException {
		Map<String,String> map = params.DatasetToMap();
		map.putAll(params.ParametersToMap());
		IO.toFile(Parameters.ParametersToString(map), Add, util.Names.paramFile);
//		IO.toFile(params.DatasetToString(), Add, util.Names.paramDataFile);
//		IO.toFile(params.ParametersToString(), Add, util.Names.paramFile);
		HyrecFlagsCork algo = new HyrecFlagsCork(params,paramStats);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
//		IO.KNNGraphtoJSON(knng, params.dataset_exact(), Add, util.Names.knngFile);
		if(ParametersStats.measure_time) {
			IO.toFile(paramStats.TimeToString(), Add, util.Names.timeFile);
		}
		if(ParametersStats.measure_scanrate) {
			IO.toFile(paramStats.ScanRateToString(), Add, util.Names.scanFile);
			IO.toFile(paramStats.CompByIterToString(), Add, util.Names.compIterFile);
			IO.toFile(paramStats.ChangesByIterToString(), Add, util.Names.changeIterFile);
		}
		return knng;
	}
	
}
