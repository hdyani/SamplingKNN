package launch;

import java.io.IOException;
import java.util.Map;

import algo.LSH.LSH;
import io.IO;
import parameters.Parameters;
import parameters.ParametersLSH;
import parameters.ParametersStats;
import util.KNNGraph;

public final class LaunchLSH {

	public final static KNNGraph launch(ParametersLSH params, ParametersStats paramStats, String Add) throws IOException {
		Map<String,String> map = params.DatasetToMap();
		map.putAll(params.ParametersToMap());
		IO.toFile(Parameters.ParametersToString(map), Add, util.Names.paramFile);
		LSH algo = new LSH(params,paramStats);
		KNNGraph knng = algo.doKNN();
		IO.KNNGraphtoJSON(knng, Add, util.Names.knngFile);
		if(ParametersStats.measure_time) {
			IO.toFile(paramStats.TimeToString(), Add, util.Names.timeFile);
		}
		if(ParametersStats.measure_scanrate) {
			IO.toFile(paramStats.ScanRateToString(), Add, util.Names.scanFile);
		}
		return knng;
	}

	public final static KNNGraph launch(ParametersLSH params, ParametersStats paramStats, String Add, int[] users) throws IOException {
		LSH algo = new LSH(params,paramStats);
		KNNGraph knng = algo.doKNN(users);
		return knng;
	}
	

}
