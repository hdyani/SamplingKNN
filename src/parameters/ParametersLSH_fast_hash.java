package parameters;

import java.util.HashMap;
import java.util.Map;

import dataset.Dataset;

public final class ParametersLSH_fast_hash implements Parameters {

	private final int k;
	private final String dataName;
	private final Dataset datasetSim;
	private Dataset datasetHash;
	
	private int nb_buckets;
	private int nb_hash;
	
	private final int nb_proc;
	
	public ParametersLSH_fast_hash(int k, String dataName, Dataset dataset, int nb_proc) {
		this.k = k;
		this.dataName = dataName;
		this.datasetSim = dataset;
		this.nb_proc = nb_proc;
	}
	
	public int k() {
		return k;
	}
	
	public Dataset dataset() {
		return datasetSim;
	}
	
	public int nb_proc() {
		return nb_proc;
	}
	
	public void set_nb_buckets(int nb_buckets) {
		this.nb_buckets = nb_buckets;
	}
	
	public int nb_buckets() {
		return nb_buckets;
	}
	
	public void set_nb_hash(int nb_hash) {
		this.nb_hash = nb_hash;
	}
	
	public int nb_hash() {
		return nb_hash;
	}
	
	public void set_datasetHash(Dataset dataset) {
		this.datasetHash = dataset;
	}
	
	public Dataset datasetHash() {
		if (datasetHash != null) {
			return datasetHash;
		}
		else {
			return datasetSim;
		}
	}
	
//	public String ParametersToString() {
//		String s = "{";
//		Map<String,String> map = ParametersToMap();
//		boolean b = true;
//		for(String name: map.keySet()) {
//			if (b) {
//				s = s + name + ": " + map.get(name);
//				b = false;
//			}
//			else {
//				s = s + ",\n" + name + ": " + map.get(name);
//			}
//		}
//		s = s + "}";
//		return s;
//	}


	private final Map<String,String> alterDataset2MapParameters(Map<String,String> map) {
		Map<String,String> new_map = new HashMap<String,String>();
		for (String param: map.keySet()) {
			if(!param.equals(util.Names.datasetName))
				new_map.put(param+"LSH", map.get(param));
		}
		return new_map;
	}
	
	@Override
	public final Map<String,String> ParametersToMap() {
		Map<String,String> dic = new HashMap<String,String>();
		dic.put(util.Names.datasetName, dataName);
		dic.put(util.Names.k, Integer.toString(k));
		dic.put(util.Names.nb_proc, Integer.toString(nb_proc));
		dic.put(util.Names.nb_hash, Integer.toString(nb_hash));
//		dic.put(util.Names.nb_buckets, Integer.toString(nb_buckets));
		return dic;
	}
	
	@Override
	public final Map<String,String> DatasetToMap() {
		Map<String, String> s = datasetSim.ParametersToMap();
		if(datasetHash != null) {
			s.putAll(alterDataset2MapParameters(datasetHash.ParametersToMap()));
		}
		return s;
	}
	@Override
	public final String DatasetToString() {
		return datasetSim.ParametersToString();
	}
}
