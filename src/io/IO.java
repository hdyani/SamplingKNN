package io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

//import dataset.Dataset;
import util.IntDoublePair;
import util.KNN;
import util.KNNGraph;

public final class IO {

	public static void toJSON(Map<String, String> map, String Add, String fileName) throws IOException {
		File file = new File(Add);
		file.mkdirs();
		BufferedWriter mbrWriterOutput = new BufferedWriter(new FileWriter(Add+fileName));
		
		mbrWriterOutput.write("{");
		boolean b = true;
		for(String item: map.keySet()) {
			if (b) {
				mbrWriterOutput.newLine();
				mbrWriterOutput.write("\"" + item + "\": \"" + map.get(item)+"\"");
				b = false;
			}
			else {
				mbrWriterOutput.write(",");
				mbrWriterOutput.newLine();
				mbrWriterOutput.write("\"" + item + "\": \"" + map.get(item)+"\"");
			}
		}
		mbrWriterOutput.newLine();
		mbrWriterOutput.write("}");
		mbrWriterOutput.close();
	}
	
	public static void toFile(String s, String Add, String fileName) throws IOException {
		File file = new File(Add);
		file.mkdirs();
		BufferedWriter mbrWriterOutput = new BufferedWriter(new FileWriter(Add+fileName));
		mbrWriterOutput.write(s);
		mbrWriterOutput.close();
	}
	
	public static String KNNtoJSON(KNN knn) {
		String json = "[";

		boolean b = true;
		for(IntDoublePair idp: knn.toSortedArray()) {
			if (b) {
				json = json + "\n{ \"" + idp.integer + "\": " + idp.score + "}"; 
				b = false;
			}
			else {
				json = json + ",\n{ \"" + idp.integer + "\": " + idp.score + "}"; 
			}
		}
		json = json + "\n]";
		return json;
	}
	
	public static String IPDStoJSON(IntDoublePair[] idps) {
		String json = "[";

		boolean b = true;
		for(IntDoublePair idp: idps) {
			if (b) {
				json = json + "\n{ \"" + idp.integer + "\": " + idp.score + "}"; 
				b = false;
			}
			else {
				json = json + ",\n{ \"" + idp.integer + "\": " + idp.score + "}"; 
			}
		}
		json = json + "\n]";
		return json;
	}
	
//	public static String IPDStoJSON(IntDoublePair[] idps, Dataset dataset) {
//		String json = "[";
//
//		boolean b = true;
//		for(IntDoublePair idp: idps) {
//			if (b) {
//				json = json + "\n{ \"" + dataset.getID(idp.integer) + "\": " + idp.score + "}"; 
//				b = false;
//			}
//			else {
//				json = json + ",\n{ \"" + dataset.getID(idp.integer) + "\": " + idp.score + "}"; 
//			}
//		}
//		json = json + "\n]";
//		return json;
//	}
	
	public static void RectoJSON(KNNGraph knng, /*Dataset dataset, */String Add, String fileName) throws IOException {
		File file = new File(Add);
		file.mkdirs();
		BufferedWriter mbrWriterOutput = new BufferedWriter(new FileWriter(Add+fileName));
		mbrWriterOutput.write("{");

		boolean b = true;
		for(int user: knng.get_users()) {
			if (b) {
				mbrWriterOutput.newLine();
//				mbrWriterOutput.write("\"" + user + "\": " + KNNtoJSON(knng.get_KNN(user)));
//				mbrWriterOutput.write("\"" + dataset.getID(user) + "\": " + IPDStoJSON(knng.get_sorted_neighbors(user)));
				mbrWriterOutput.write("\"" + user + "\": " + IPDStoJSON(knng.get_sorted_neighbors(user)));
				b = false;
			}
			else {
				mbrWriterOutput.write(",");
				mbrWriterOutput.newLine();
//				mbrWriterOutput.write("\"" + user + "\": " +KNNtoJSON(knng.get_KNN(user)));
//				mbrWriterOutput.write("\"" + dataset.getID(user) + "\": " +IPDStoJSON(knng.get_sorted_neighbors(user)));
				mbrWriterOutput.write("\"" + user + "\": " +IPDStoJSON(knng.get_sorted_neighbors(user)));
			}
		}

		mbrWriterOutput.newLine();
		mbrWriterOutput.write("}");
		mbrWriterOutput.close();
	}
	
	public static void KNNGraphtoJSON(KNNGraph knng, /*Dataset dataset, */String Add, String fileName) throws IOException {
		File file = new File(Add);
		file.mkdirs();
		BufferedWriter mbrWriterOutput = new BufferedWriter(new FileWriter(Add+fileName));
		mbrWriterOutput.write("{");

		boolean b = true;
		for(int user: knng.get_users()) {
			if (b) {
				mbrWriterOutput.newLine();
//				mbrWriterOutput.write("\"" + user + "\": " + KNNtoJSON(knng.get_KNN(user)));
//				mbrWriterOutput.write("\"" + dataset.getID(user) + "\": " + IPDStoJSON(knng.get_sorted_neighbors(user),dataset));
				mbrWriterOutput.write("\"" + user + "\": " + IPDStoJSON(knng.get_sorted_neighbors(user)));
				b = false;
			}
			else {
				mbrWriterOutput.write(",");
				mbrWriterOutput.newLine();
//				mbrWriterOutput.write("\"" + user + "\": " +KNNtoJSON(knng.get_KNN(user)));
//				mbrWriterOutput.write("\"" + dataset.getID(user) + "\": " +IPDStoJSON(knng.get_sorted_neighbors(user),dataset));
				mbrWriterOutput.write("\"" + user + "\": " +IPDStoJSON(knng.get_sorted_neighbors(user)));
			}
		}

		mbrWriterOutput.newLine();
		mbrWriterOutput.write("}");
		mbrWriterOutput.close();
	}
	
	
	public static void HeatMaptoFile(int[][] heatmap, String Add, String fileName) throws IOException {
		File file = new File(Add);
		file.mkdirs();
		BufferedWriter mbrWriterOutput = new BufferedWriter(new FileWriter(Add+fileName));

		boolean b = true;
		for(int[] line: heatmap) {
			if (b) {
				b = false;
			}
			else {
				mbrWriterOutput.newLine();
			}
			boolean b2 = true;
			for(int i:line) {
				if(b2) b2 = false;
				else mbrWriterOutput.write(",");
				mbrWriterOutput.write(Integer.toString(i));
			}
		}

		mbrWriterOutput.close();
	}
	
	
	
}
