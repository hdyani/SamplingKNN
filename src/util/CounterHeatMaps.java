package util;

public class CounterHeatMaps {

//	long counterx01;
//	long countery01;
//	long counterxy01;
	
	long counterKNN0001;
	long counterKNN0002;
	long counterKNN0005;
	long counterKNN001;
	long counterKNN002;
	long counterKNN005;
	long counterKNN01;
	long counterKNN015;
	long counterKNN02;
	long counterKNN03;
	long counterKNN0;
	
	long nb_comp;
	
	public CounterHeatMaps(long nb_comp) {
		this.counterKNN0 = 0;
		this.counterKNN001 = 0;
		this.counterKNN002 = 0;
		this.counterKNN005 = 0;
		this.counterKNN0001 = 0;
		this.counterKNN0002 = 0;
		this.counterKNN0005 = 0;
		this.counterKNN01 = 0;
		this.counterKNN015 = 0;
		this.counterKNN02 = 0;
		this.counterKNN03 = 0;
//		this.counterx01 = 0;
//		this.countery01 = 0;
//		this.counterxy01 = 0;
		this.nb_comp = nb_comp;
	}
	
	
	
	public synchronized void add(double s1, double s2) {
//		if (s1 < 0.1) {
//			counterx01++;
//			if(s2<0.1) {
//				counterxy01++;
//			}
//		}
//		if(s2<0.1) {
//			countery01++;
//		}
		
		double dist = (Math.abs(s2-s1))/(Math.sqrt(2));
		
		if(dist == 0) {
			counterKNN0++;
		}
		if(dist <= 0.3) {
			counterKNN03++;
		}
		if(dist <= 0.2) {
			counterKNN02++;
		}
		if(dist <= 0.15) {
			counterKNN015++;
		}
		if(dist <= 0.1) {
			counterKNN01++;
		}
		if(dist <= 0.01) {
			counterKNN001++;
		}
		if(dist <= 0.02) {
			counterKNN002++;
		}
		if(dist <= 0.05) {
			counterKNN005++;
		}
		if(dist <= 0.001) {
			counterKNN0001++;
		}
		if(dist <= 0.002) {
			counterKNN0002++;
		}
		if(dist <= 0.005) {
			counterKNN0005++;
		}
		
	}
	
	public String toString() {
		String s = "";
		s = s + nb_comp + " comparisons\n";
//		s = s + " " + counterx01 + " pairs with x <= 0.1\n";
//		s = s + " " + (counterx01*100)/(nb_comp) + " percent with x <= 0.1\n";
//		s = s + " " + countery01 + " pairs with y <= 0.1\n";
//		s = s + " " + (countery01*100)/(nb_comp) + " percent with y<= 0.1\n";
//		s = s + " " + counterxy01 + " pairs with x,y <= 0.1\n";
//		s = s + " " + (counterxy01*100)/(nb_comp) + " percent with x,y <= 0.1\n";
		s = s + " " + counterKNN0 + " pairs with d(diag)=0\n";
		s = s + " " + (counterKNN0*100)/(nb_comp) + " percent with d(diag)=0\n";
		s = s + " " + counterKNN0001 + " pairs with d(diag)<=0.001\n";
		s = s + " " + (counterKNN0001*100)/(nb_comp) + " percent with d(diag)<=0.001\n";
		s = s + " " + counterKNN0002+ " pairs with d(diag)<=0.002\n";
		s = s + " " + (counterKNN0002*100)/(nb_comp) + " percent with d(diag)<=0.002\n";
		s = s + " " + counterKNN0005+ " pairs with d(diag)<=0.005\n";
		s = s + " " + (counterKNN0005*100)/(nb_comp) + " percent with d(diag)<=0.005\n";
		s = s + " " + counterKNN001 + " pairs with d(diag)<=0.01\n";
		s = s + " " + (counterKNN001*100)/(nb_comp) + " percent with d(diag)<=0.01\n";
		s = s + " " + counterKNN002+ " pairs with d(diag)<=0.02\n";
		s = s + " " + (counterKNN002*100)/(nb_comp) + " percent with d(diag)<=0.02\n";
		s = s + " " + counterKNN005+ " pairs with d(diag)<=0.05\n";
		s = s + " " + (counterKNN005*100)/(nb_comp) + " percent with d(diag)<=0.05\n";
		s = s + " " + counterKNN01+ " pairs with d(diag)<=0.1\n";
		s = s + " " + (counterKNN01*100)/(nb_comp) + " percent with d(diag)<=0.1\n";
		s = s + " " + counterKNN015+ " pairs with d(diag)<=0.15\n";
		s = s + " " + (counterKNN015*100)/(nb_comp) + " percent with d(diag)<=0.15\n";
		s = s + " " + counterKNN02+ " pairs with d(diag)<=0.2\n";
		s = s + " " + (counterKNN02*100)/(nb_comp) + " percent with d(diag)<=0.2\n";
		s = s + " " + counterKNN03+ " pairs with d(diag)<=0.3\n";
		s = s + " " + (counterKNN03*100)/(nb_comp) + " percent with d(diag)<=0.3\n";
		return s;
	}
	
}
