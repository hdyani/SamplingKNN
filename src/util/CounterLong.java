package util;

public final class CounterLong {
	private long counter=0;
	
	public CounterLong() {
		counter = 0;
	}
	
	public void inc() {
		counter++;
	}
	
	public void inc(long count) {
		counter = counter + count;
	}
	
	public long getValue() {
		return counter;
	}
	
	public void init() {
		counter = 0;
	}
}
