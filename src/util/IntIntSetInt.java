package util;

import java.util.HashSet;
import java.util.Set;

public final class IntIntSetInt {
	public int index_i;
	public int index_j;
	public Set<Integer> set;
	public int score;
	public IntIntSetInt(int index_i,int index_j, Set<Integer> set, int score) {
		this.index_i = index_i;
		this.index_j = index_j;
		this.set = set;
		this.score = score;
	}
	
	public IntIntSetInt(int index_i,int index_j, Set<Integer> set) {
		this.index_i = index_i;
		this.index_j = index_j;
		this.set = set;
		this.score = set.size();
	}

	@Override
	public String toString() {
		return ("("+index_i + ","+index_j+","+set.toString()+","+score+")");
	}
	
	@Override
	public IntIntSetInt clone() {
		HashSet<Integer> new_set = new HashSet<Integer>();
		new_set.addAll(set);
		return (new IntIntSetInt(index_i,index_j,new_set,score));
	}
}
