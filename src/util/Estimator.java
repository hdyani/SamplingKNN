package util;

import java.util.BitSet;

import dataset.Dataset;

public final class Estimator {

	public static final double estimMinMinMax(Dataset dataset, KNNGraph knng, int nb_users, double ratio) {
		int size_sampling = (int) (((double) nb_users) * ratio);
		return (estimMinMinMax(dataset,knng,size_sampling));
	}
	public static final double estimMinMinMax(Dataset dataset, KNNGraph knng, double ratio) {
		//we take the proportion "ratio" of the n(n-1)/2 pairs of users.
		int size_sampling = (int) (((double) (dataset.getUsers().size() * (dataset.getUsers().size()-1))/2) * ratio);
		return (estimMinMinMax(dataset,knng,size_sampling));
	}
	public static final double estimMinMinMax(Dataset dataset, KNNGraph knng, int size) {
		KNN knngMinMax = new KNN(size,0);
		int i=0;
		for(int user: knng.get_users()) {
			for(int neighbor:knng.get_neighbors_ids(user)){//.get_KNN(user).itemArray()) {
				//KNNG is store according an increasing order, here we want to sort according a decreasing order => t = 1-sim
				double minMax = 1-minMax(dataset.getRatedItems(user).size(), dataset.getRatedItems(neighbor).size());
//				if (minMax == 1) {
//
//					System.out.println(user + ":");
//					System.out.println(dataset.getRatedItems(user).size());
//					System.out.println(neighbor + ":");
//					System.out.println(dataset.getRatedItems(neighbor).size());
//				}
				knngMinMax.add(new IntDoublePair(i,minMax));
				i++;
			}
		}
		//knngMinMax.min_score() returns the minimum score of the top k 1-sim, i.e. the k best 1-sim, so sim is the kth worst.
		return (1-knngMinMax.min_score());
	}
	
	public static final double minMax(int a, int b) {
		if(a == 0 || b == 0) {
			return 0;
		}
		if (a<b) {
			return (((double) a)/ ((double) b));
		}
		else {
			return (((double) b)/ ((double) a));
		}
	}
	
	
	
	
	


	public static final int estimB(KNNGraph exactKNNG, KNNGraph approxKNNG) {
		return estimB(exactKNNG,approxKNNG,exactKNNG.get_users().size()/2);
	}
	public static final int estimB(KNNGraph exactKNNG, KNNGraph approxKNNG, int nb_users, double ratio) {
		int size_sampling = (int) (((double) nb_users) * ratio);
		return estimB(exactKNNG,approxKNNG,size_sampling);
	}
	public static final int estimB(KNNGraph exactKNNG, KNNGraph approxKNNG, double ratio) {
		int size_sampling = (int) (((double) exactKNNG.get_users().size()) * (1-ratio));
//		System.out.println("ratio estimB: " + (1-ratio));
//		System.out.println("size sampling: " + size_sampling);
		return estimB(exactKNNG,approxKNNG,size_sampling);
	}
	public static final int estimB(KNNGraph exactKNNG, KNNGraph approxKNNG, int size) {
		int maxValueB = 30;
		for(int user: approxKNNG.get_users()) {
//			maxValueB = approxKNNG.get_KNN(user).toArray().length;
			maxValueB = approxKNNG.get_neighbors(user).length;
			break;
		}
		KNN knngRank = new KNN(size,-1);
		for (int user: exactKNNG.get_users()) {
			int maxRank = -1;
			for(int neighbor: exactKNNG.get_neighbors_ids(user)) {
//				int rank = get_rank(neighbor, approxKNNG.get_KNN(user));
				int rank = get_rank(neighbor, approxKNNG.get_neighbors_ids(user));
				if (rank > maxRank) {
					maxRank = rank;
				}
			}
			knngRank.add(new IntDoublePair(user, maxValueB-maxRank));
		}
//		System.out.println(maxValueB-knngRank.min_score());
		return (int) (maxValueB-knngRank.min_score());
	}

	
	public static final int get_rank(int user, int[] knn) {
		int rank = 0;
		for(int neighbor: knn) {
			if (neighbor == user) {
				break;
			}
			rank++;
		}
		if (rank > knn.length) {
			rank = -1;
//			System.out.println("ERROR");
		}
		return rank;
	}
	
	public static final int get_rank(int user, KNN knn) {
		int rank = 0;
		for(int neighbor: knn.itemArray()) {
			if (neighbor == user) {
				break;
			}
			rank++;
		}
		if (rank > knn.itemArray().length) {
			rank = -1;
//			System.out.println("ERROR");
		}
		return rank;
	}
	

	public static final int estimBQuality(Dataset dataset, KNNGraph exactKNNG, KNNGraph approxKNNG) {
		return estimBQuality(dataset,exactKNNG,approxKNNG,0.1,1);//exactKNNG.get_users().size()/2);
	}
	public static final int estimBQuality(Dataset dataset, KNNGraph exactKNNG, KNNGraph approxKNNG, int nb_users, double ratio_sampling) {
		int size_sampling = (int) (((double) nb_users) * ratio_sampling);
		return estimBQuality(dataset,exactKNNG,approxKNNG,0.1,size_sampling);
	}
	public static final int estimBQuality(Dataset dataset, KNNGraph exactKNNG, KNNGraph approxKNNG, double ratio) {
		return estimBQuality(dataset,exactKNNG,approxKNNG,ratio,exactKNNG.get_users().size()/2);
	}
	public static final int estimBQuality(Dataset dataset, KNNGraph exactKNNG, KNNGraph approxKNNG, double ratio, int nb_users, double ratio_sampling) {
		int size_sampling = (int) (((double) nb_users) * ratio_sampling);
		return estimBQuality(dataset,exactKNNG,approxKNNG,ratio,size_sampling);
	}
	public static final int estimBQuality(Dataset dataset, KNNGraph exactKNNG, KNNGraph approxKNNG, double ratio, double ratio_sampling) {
		int size_sampling = (int) (((double) exactKNNG.get_users().size()) * ratio_sampling);
		return estimBQuality(dataset,exactKNNG,approxKNNG,ratio,size_sampling);
	}
	public static final int estimBQuality(Dataset dataset, KNNGraph exactKNNG, KNNGraph approxKNNG, double ratio, int size) {
//		int maxValueB = 30;
//		for(int user: approxKNNG.get_users()) {
//			maxValueB = approxKNNG.get_KNN(user).toArray().length;
//			break;
//		}
		int k = 30;
		for(int user: exactKNNG.get_users()) {
//			k = exactKNNG.get_KNN(user).toArray().length;
			k = exactKNNG.get_neighbors_ids(user).length;
			break;
		}
//		System.out.println("MaxValue: "+ maxValueB);
		KNN knngRank = new KNN(size,-1);
		for (int user: exactKNNG.get_users()) {
//			int maxRank = -1;
//			double minscore = exactKNNG.get_KNN(user).min_score();
			double minscore = exactKNNG.get_neighbors(user)[0].score;
//			for(int neighbor: exactKNNG.get_neighbors_ids(user)) {
//				int rank = get_rank(neighbor, approxKNNG.get_KNN(user));
//				if (rank > maxRank) {
//					maxRank = rank;
//				}
//			}
//			System.out.println("ratio: " + ratio);
			int maxRank = get_rankQuality(dataset,user,ratio*minscore, k, approxKNNG.get_neighbors(user));
//			System.out.println(maxValueB-maxRank);
//			knngRank.add(new IntDoublePair(user, maxValueB-maxRank));
			knngRank.add(new IntDoublePair(user, maxRank));
		}
//		System.out.println(maxValueB);
		System.out.println(knngRank.toString());
		System.out.println(knngRank.min_score());
//		System.out.println(maxValueB-knngRank.min_score());
//		return (int) (maxValueB-knngRank.min_score());
		return (int) (knngRank.min_score());
	}

//	public static final int get_rankQuality(Dataset dataset, int user, double quality, int nb_users, KNN knn) {
	public static final int get_rankQuality(Dataset dataset, int user, double quality, int nb_users, IntDoublePair[] knn) {
		int rank = 0;
		int aux=0;
		for(IntDoublePair idp: knn) {
//		for(IntDoublePair idp: knn.toSortedArray()) {
//			if(!dataset.getUsers().contains(idp.integer)) {
//				System.out.println(knn.toArray().length);
//				System.out.println(knn.toString());
//				System.out.println(idp.integer);
//				System.out.println(idp.score);
//			}
			if (dataset.sim(user, idp.integer) >= quality) {
//			if (idp.score >= quality) {
				aux++;
			}
			if(aux == nb_users) {
				break;
			}
			rank++;
		}
//		System.out.println(rank + " out of " + knn.itemArray().length);
//		if (rank >= knn.itemArray().length) {
		if (rank >= knn.length) {
			rank = -1;
		}
		return rank;
	}
	
	
	
	
	
	
	
	
	public static final int estimSizeBitArray(int nb_users, int k) {
//		KNNGraph knng = new KNNGraph();
		KNNGraph knng = new KNNGraph(k,nb_users,0);
		for(int user = 0; user < nb_users; user++) {
			knng.put(user, new KNN(k,0));
		}
		int nb_bits=32;
	
		while(true){
			int current_nb_bits = nb_bits * 2;
			long initMem = Runtime.getRuntime().freeMemory();
			@SuppressWarnings("unused")
			BitSet bs = new BitSet(current_nb_bits);
			long mem = Runtime.getRuntime().freeMemory();
			
			if(initMem < (mem * nb_users)) {
				break;
			}
			nb_bits = nb_bits * 2;
		}
		
		
		knng = null;
		return nb_bits;
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
