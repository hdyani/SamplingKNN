package util;

//import java.util.Map;
import java.util.Set;
//import java.util.concurrent.ConcurrentHashMap;

public final class KNNGraph{

//	private Map<Integer, KNN> knng;
	private KNNG knng;
	
	public KNNGraph(int k, int nb_users, double score_init) {
//		this.knng = new KNNGraphArray(k,nb_users,score_init);
		this.knng = new KNNGraphHashMapKNN(nb_users);
//		this.knng = new KNNGraphHashMapKNN();
	}
//	public KNNGraph(int k, int nb_users, double score_init) {
//		this.knng = new KNNGraphArray(k,nb_users,score_init);
//	}
	
	public KNNGraph(KNNG knng) {
		this.knng = knng;
	}
	
//	public void init(int user, int k, double score_init) {
//		this.knng.put(user, new KNN(k, score_init));
//	}
	public void init(int user, int k, double score_init) {
		this.knng.init(user, k, score_init);
	}
	
	public boolean put(int user, int neighbor, double score) {
//		return this.knng.get(user).add(new IntDoublePair(neighbor,score));
		return (this.knng.put(user, neighbor, score));
	}
	
//	public boolean put(int user, IntDoublePair idp) {
////		return this.knng.get(user).add(idp);
//		return (this.knng.put(user, idp));
//	}
	
	public void put(int user, KNN knn) {
		this.knng.put(user, knn);
	}
	
	public int[] get_neighbors_ids(int user) {
//		return this.knng.get(user).itemArray();
		return (this.knng.get_neighbors_ids(user));
	}
	
	
	public IntDoublePair[] get_neighbors(int user) {
//		return this.knng.get(user).toArray();
		return (this.knng.get_neighbors(user));
	}
	
	public IntDoublePair[] get_sorted_neighbors(int user) {
		return this.knng.get_sorted_neighbors(user);
	}
	
	public boolean has_neighbors(int user) {
//		return (this.knng.get(user) != null);
		return (this.knng.has_neighbors(user));
	}
	
	public Set<Integer> get_users() {
//		return knng.keySet();
		return (this.knng.get_users());
	}
	
//	public KNN get_KNN(int user) {
////		return knng.get(user);
//		return (this.knng.get_KNN(user));
//	}
	
	
	
	
	@Override
	public KNNGraph clone() {
//		KNNGraph clone = new KNNGraph();
//		for(int index: this.knng.keySet()) {
//			clone.put(index, this.knng.get(index).clone());
//		}
//		return clone;
		return (new KNNGraph(this.knng.clone()));
	}
}
