package util;

//import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.BitSet;
//import java.util.Random;

public class HashFunctions {

//    private static final int seed32 = 89478583;
//    private static final int m = 32;
	
    
    
    public static int[] hash(int v, int m, int k) {
//    	byte[] essai = BigInteger.valueOf(v).toByteArray(); //ALSO WORK
    	byte[] value = IntToByteArray(v);
    	int[] hash = hashCrypt(value,m,k,"SHA-512");
    	return hash;
    }
    
    public static int hash(int v, int m) {
//    	byte[] essai = BigInteger.valueOf(v).toByteArray(); //ALSO WORK
    	
    	//SHA-512
//    	byte[] value = IntToByteArray(v);
//    	int[] hash = hashCrypt(value,m,1,"SHA-512");
//    	return hash[0];
    	
    	
//    	return (v%m); //modulo

    	return get_hash(v,m); //Jenkins
    }
    

    /**
     * @param value the value to be hashed
     * @param m     integer output range [1,size]
     * @param k     number of hashes to be computed
     * @param method the hash method name used by {@link MessageDigest#getInstance(String)}
     * @return array with <i>hashes</i> integer hash positions in the range <i>[0,size)</i>
     */
    public static int[] hashCrypt(byte[] value, int m, int k, String method) {
        //MessageDigest is not thread-safe --> use new instance
        MessageDigest cryptHash = null;
        try {
            cryptHash = MessageDigest.getInstance(method);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

        int[] positions = new int[k];

        int computedHashes = 0;
        // Add salt to the hash deterministically in order to generate different
        // hashes for each round
        // Alternative: use pseudorandom sequence
//        Random r = new Random(seed32);
        byte[] digest = new byte[0];
        while (computedHashes < k) {
            // byte[] saltBytes =
            // ByteBuffer.allocate(4).putInt(r.nextInt()).array();
            cryptHash.update(digest);
            digest = cryptHash.digest(value);
            BitSet hashed = BitSet.valueOf(digest);

            // Convert the hash to numbers in the range [0,size)
            // Size of the BloomFilter rounded to the next power of two
            int filterSize = 32 - Integer.numberOfLeadingZeros(m);
            // Computed hash bits
            int hashBits = digest.length * 8;
            // Split the hash value according to the size of the Bloomfilter --> higher performance than just doing modulo
            for (int split = 0; split < (hashBits / filterSize)
                    && computedHashes < k; split++) {
                int from = split * filterSize;
                int to = (split + 1) * filterSize;
                BitSet hashSlice = hashed.get(from, to);
                // Bitset to Int
                long[] longHash = hashSlice.toLongArray();
                int intHash = longHash.length > 0 ? (int) longHash[0] : 0;
                // Only use the position if it's in [0,size); Called rejection sampling
                if (intHash < m) {
                    positions[computedHashes] = intHash;
                    computedHashes++;
                }
            }
        }

        return positions;
    }

	public final static byte[] IntToByteArray(int v) {
		byte[] bytes = ByteBuffer.allocate(Integer.BYTES).putInt(v).array();
//
//		for (byte b : bytes) {
//		   System.out.format("0x%x ", b);
//		}
		return bytes;
	}
	

	public final static int get_hash_mod(int item, int max_value){
		return (item % max_value);
	}
	
	public final static int get_hash(int item, int max_value) {
		return get_hash(item,5,max_value);
//		return get_hash_mod(item,max_value);
//		return item;
	}
	//Robert Jenkins' 96 bit Mix Function
	public final static int get_hash(int item, int hash, int max_value) {
		int a = hash * 0xcc9e2d51;
		int b = hash * (0x1b873593)^(hash-1);
		int c = item;
		a=a-b;  a=a-c;  a=a^(c >>> 13);
		b=b-c;  b=b-a;  b=b^(a << 8);
		c=c-a;  c=c-b;  c=c^(b >>> 13);
		a=a-b;  a=a-c;  a=a^(c >>> 12);
		b=b-c;  b=b-a;  b=b^(a << 16);
		c=c-a;  c=c-b;  c=c^(b >>> 5);
		a=a-b;  a=a-c;  a=a^(c >>> 3);
		b=b-c;  b=b-a;  b=b^(a << 10);
		c=c-a;  c=c-b;  c=c^(b >>> 15);
		c = c % max_value;
		c = c + max_value;
		c = c % max_value;
		return c;
	}
	
}
