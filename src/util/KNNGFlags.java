package util;

import java.util.Set;

public interface KNNGFlags {
	
	public void init(int user, int k, double score_init);
	
	public boolean put(int user, int neighbor, double score, boolean bool);
	
	public boolean put_AS(int user, int neighbor, double score, boolean bool);
	
//	public boolean put(int user, IntDoubleBoolTriplet idp);
	
//	public void put(int user, KNNFlags knn);
	
	public int[] get_neighbors_ids(int user);
	
	
	public IntDoubleBoolTriplet[] get_neighbors(int user);
	
	public boolean has_neighbors(int user);
	
	public Set<Integer> get_users();
	
//	public KNNFlags get_KNN(int user);
	
	
	public KNNGraph toKNNGraph();
	
	public KNNGFlags clone();
	
	public void changeBoolToFalse(int user, IntDoubleBoolTriplet idbt);
}
