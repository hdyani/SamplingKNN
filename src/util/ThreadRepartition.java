package util;

public final class ThreadRepartition {

	public static int[] repartition(int[] users, int nb_thread, int loop) {
		int nb_partition=0;
		int normal_size;
		int n = users.length;
		normal_size = n / nb_thread;
		if(n % nb_thread == 0) {
			nb_partition = n / nb_thread;
		}
		else {
			nb_partition = n / nb_thread;
			if((n % nb_thread) > (nb_thread  - (loop+1))) {
				//			if((n % nb_partition) > (nb_thread  - (loop+1))) {
				nb_partition++;
			}
		}
		int[] partition = new int[nb_partition];
		int index_users;
		for(int index_partition = 0; index_partition < nb_partition; index_partition++) {
			index_users = loop * normal_size + index_partition;
			if((n % nb_thread) > (nb_thread  - (loop+1))) {
				index_users = index_users + (n % nb_thread) - (nb_thread  - (loop));
			}
			partition[index_partition] = users[index_users];
		}
		return partition;
	}


	
	public static int[] pivot_repartition(int[] users, int nb_proc, int loop) {
		int[] user_ids;
		int n = users.length;
		int size_usual_partition = n / nb_proc;
		int size_partition = n / nb_proc;
		if((n % nb_proc) > (nb_proc  - (loop+1))) {
			size_partition++;
		}
		user_ids = new int[size_partition];

		int index_users_ids;
		if(size_usual_partition % 2 == 1) {
			for(int j = 0; j < size_partition/2; j++) {
				index_users_ids = loop * (size_usual_partition/2) + j;
				if((n % nb_proc) > (nb_proc  - (loop+1))) {
					index_users_ids = index_users_ids + ((n % nb_proc) - (nb_proc  - (loop)));
				}
				user_ids[j] = users[index_users_ids];
			}
			for(int j = size_partition/2; j < size_partition; j++) {
				index_users_ids = n + (j-(size_partition/2)) - (loop+1) * (size_usual_partition/2 + 1);
				user_ids[j] = users[index_users_ids];
			}
		}
		
		if(size_usual_partition % 2 == 0) {
			for(int j = 0; j < size_usual_partition/2; j++) {
				index_users_ids = loop * (size_usual_partition/2) + j;
				user_ids[j] = users[index_users_ids];
			}
			for(int j = size_partition/2; j < size_partition; j++) {
				index_users_ids = n + (j-(size_partition/2)) - (loop+1) * (size_usual_partition/2);
				if((n % nb_proc) > (nb_proc  - (loop+1))) {
					index_users_ids = index_users_ids - ((n % nb_proc) - (nb_proc  - (loop+1)));
				}
				user_ids[j] = users[index_users_ids];
			}
		}
		return user_ids;
	}



//	public static int[] pivot_repartition(int[] users, int nb_proc, int loop) {
//		int[] user_ids;
//		int n = users.length;
//		int size_usual_partition = n / nb_proc;
//		int size_partition = n / nb_proc;
//		if((n % nb_proc) > (nb_proc  - (loop+1))) {
//			//			if((n % nb_partition) > (nb_thread  - (loop+1))) {
//			size_partition++;
//		}
////		System.out.println(size_usual_partition);
////		System.out.println(size_partition);
//
//		if(loop == nb_proc - 1) {
//			//			int size_usual_partition = (users.length+1)/nb_proc;
//			//			int size_partition = users.length - loop * size_usual_partition;
//			user_ids = new int[size_partition];
//			for(int j = 0; j < size_partition; j++) {
//				int index_users_ids = loop * (size_usual_partition/2) + j;
////				if((n % nb_proc) > (nb_proc  - (loop+1))) {
////					index_users_ids = index_users_ids + (n % nb_proc) - (nb_proc  - (loop));
////				}
//				user_ids[j] = (int) users[index_users_ids];
//			}
//		}
//		else {
//			//			int size_partition = (users.length+1)/nb_proc;
//			user_ids = new int[size_partition];
//
//			//first part
//			for(int j = 0; j < size_partition/2; j++) {
//				int index_users_ids = loop * (size_usual_partition/2) + j;
////				System.out.println(index_users_ids);
////				System.out.println(index_users_ids);
//				user_ids[j] = (int) users[index_users_ids];
//			}
//
//			//second part
//			for(int j = size_partition/2; j < size_partition; j++) {
//				//				System.out.println(users.length + j - (((i+1) * (size_partition - (size_partition/2)))+(size_partition/2)));
////				int index_users_ids = n + j - (((loop+1) * (size_partition - (size_partition/2)))+(size_partition/2));
//
//				//				int index_users_ids = (nb_proc - 1) * (size_usual_partition/2) + size_partition + ((nb_proc - loop) * (size_partition/2)) + j;
//				int index_users_ids = (n-1) - (loop+1) * (size_usual_partition/2) + j - 1;
//				if((n % nb_proc) > (nb_proc  - (loop+1))) {
//					index_users_ids = index_users_ids - ((n % nb_proc) - (nb_proc  - (loop+1)));
////					index_users_ids = n - loop * size_partition;
//				}
////				System.out.println(j);
////				System.out.println(index_users_ids);
//				user_ids[j] = (int) users[index_users_ids];
//			}
//		}
//		return user_ids;
//	}

}
