package util;

import java.util.Set;

import algo.BruteForce.BruteForceRunnable_generic_KNNG;
import dataset.Dataset;
import parameters.ParametersBruteForce;
import parameters.ParametersStats;

public class DoBruteForceKNN {
	public final Dataset dataset;
	public final int nb_proc;
	public final int nb_xp;
	public final KNNG knng;
	public final ParametersBruteForce params;
	public final ParametersStats paramStats;
	
	
	public DoBruteForceKNN(Dataset dataset, int k, int nb_proc, int nb_xp, KNNG knng) {
		this.dataset = dataset;
		this.nb_proc = nb_proc;
		this.nb_xp = nb_xp;
		this.knng = knng;
		this.params = new ParametersBruteForce(k, "", dataset, nb_proc);
		this.paramStats = new ParametersStats(false,false);
	}
	
	public long doKNN() {

		
		long start = System.currentTimeMillis();
		for (int i_xp=0; i_xp<nb_xp; i_xp++) {

			Set<Integer> user_aux = params.dataset().getUsers();
			Integer[] users_ = new Integer[user_aux.size()];
			int[] users = new int[user_aux.size()];
			user_aux.toArray(users_);
			Thread[] threads = new Thread[params.nb_proc()];



			int index = 0;
			for(int user: users_) {
				knng.put(user, new KNN(params.k(), params.dataset().getInitValue()-1));
//				knng.put(user, new KNN_para(params.k(), params.dataset().getInitValue()-1));
				users[index] = user;
				index++;
			}


			for(int i = 0; i < params.nb_proc(); i++) {
				threads[i] = new Thread(new BruteForceRunnable_generic_KNNG(params, knng, users, i));
				threads[i].start();
			}

			for(int i = 0; i < params.nb_proc(); i++) {
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		return (System.currentTimeMillis() - start);
	}
}
