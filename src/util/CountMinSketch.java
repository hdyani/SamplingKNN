package util;

public class CountMinSketch {

	private final long[][] sketch;
	private final int number_rows;
	private final int number_columns;
	
	public CountMinSketch(int nb_rows, int nb_columns) {
		this.number_rows = nb_rows;
		this.number_columns = nb_columns;
		this.sketch = new long[number_rows][number_columns];
		for(int i=0; i < number_rows; i++) {
			for(int j=0; j<number_columns; j++) {
				sketch[i][j]=0;
			}
		}
	}
	
	
//	public void set(int n, double v) {
//		
//	}
	
	public void incr(int n, int m) {
		int hash;
		for(int i=0; i < number_rows; i++) {
			hash = HashFunctions.get_hash(n, i, number_columns);
			sketch[i][hash] = sketch[i][hash]+m;
		}
	}
	
	public void incr(int n) {
		int hash;
		for(int i=0; i < number_rows; i++) {
			hash = HashFunctions.get_hash(n, i, number_columns);
			sketch[i][hash] = sketch[i][hash]+1;
		}
	}
	
	public long get(int n) {
		long score = Long.MAX_VALUE;
		int hash;
		for(int i=0; i < number_rows; i++) {
			hash = HashFunctions.get_hash(n, i, number_columns);
			score = Math.min(score,sketch[i][hash]);
		}
		return 0;
	}
	
	
}
