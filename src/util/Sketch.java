package util;

import java.util.BitSet;

public final class Sketch {

	private final int nb_bitset;
	private final int size_bitset;
	private final BitSet[] sketch;
	
	public Sketch(int nb_bitset, int size_bitset) {
		this.nb_bitset = nb_bitset;
		this.size_bitset = size_bitset;
		this.sketch = new BitSet[nb_bitset];
		for(int i = 0; i < nb_bitset; i++) {
			sketch[i] = new BitSet(size_bitset);
		}
	}
	
	public void put(int item) {
		for(int i = 0; i < nb_bitset; i++) {
//			int hash = util.HashFunctions.hash(item, size_bitset);
			int hash = util.HashFunctions.get_hash(item, i, size_bitset);
			sketch[i].set(hash);
		}
	}
	
	public int get_size() {
		return size_bitset;
	}
	public int get_nb() {
		return nb_bitset;
	}
	
	public BitSet get_bitset(int i) {
		return (BitSet) (sketch[i].clone());
	}
	
	public int hamming(Sketch sketch_) {
		int hamming = 0;
		for(int i = 0; i < nb_bitset; i++) {
			BitSet bs = (sketch_.get_bitset(i));
			bs.and(sketch[i]);
			hamming = Math.min(hamming, bs.cardinality());
		}
		return hamming;
	}
	
	public int cardinality() {
		int cardinality = 0;
		for(int i = 0; i < nb_bitset; i++) {
			cardinality = Math.min(cardinality, sketch[i].cardinality());
		}
		return cardinality;
	}

}
