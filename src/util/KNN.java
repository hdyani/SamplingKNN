package util;

//import java.util.TreeSet;
import java.util.HashSet;

/**
 * Heap in which the IntDoublePair are stored in an increasing order.
 * Int is actually the neighbor or the item, and the score is the similarity or the rating.
 * The order is the lexicographic order.
 * The size is fixed and there is no suppression.
 */
public final class KNN implements KNN_i{

	private final int size;
	private final IntDoublePair[] heap;
//	private final TreeSet<Integer> tree;
	private final HashSet<Integer> hashset;

	public KNN(int _size) {
		size = _size;
		heap = new IntDoublePair[size];
//		tree = new TreeSet<Integer>();
//		tree = new HashSet<Integer>((int) (((double) size)/0.75));
		hashset = new HashSet<Integer>((int) Math.ceil(((double) size)/0.75));
		for(int index = 0; index < size; index++) {
			heap[index] = new IntDoublePair(-1,0);
		}
	}

	public KNN(int _size, double initValue) {
		size = _size;
		heap = new IntDoublePair[size];
//		tree = new TreeSet<Integer>();
		hashset = new HashSet<Integer>((int) Math.ceil(((double) size)/0.75));
		for(int index = 0; index < size; index++) {
			heap[index] = new IntDoublePair(-1,initValue);
		}
	}

	public KNN(IntDoublePair[] elements) {
		size = elements.length;
		heap = elements;
//		tree = new TreeSet<Integer>();
		hashset = new HashSet<Integer>((int) Math.ceil(((double) size)/0.75));
		for(int index = 0; index < size; index++) {
			hashset.add(heap[0].integer);
		}
	}


	public IntDoublePair[] toArray() {
		return heap;
	}

	/**
	 * Return the integers of the heap in an array, sorted by ratings, from the highest to the lowest.
	 * @return
	 */
	public int[] itemArray() {
		IntDoublePair[] aux = toSortedArray();
		int[] array = new int[size];
		for(int index = 0; index < size; index++) {
			//			array[size - index - 1] = heap[index].integer;
			array[index] = aux[index].integer;
		}
		return array;
	}


	/**
	 * Adding an int is the same as suppressing the lowest one (which is the root) and replace it by the new one.
	 * @param irp
	 */
	public /*synchronized*/ boolean add(IntDoublePair irp) {
		if(can_be_added(irp)){// && !contains(irp)) {
//		if(is_lower(heap[0], irp) && !contains(irp)) {
			
//			if(heap[0].integer != -1) {
//				tree.remove(heap[0].integer);
//			}
			double min_score = heap[0].score;
			hashset.remove(heap[0].integer);
			hashset.add(irp.integer);
//			if(hashset.size() > heap.length) {
//				System.out.println("error: " + hashset.size());
//			}
			
			int index = 0;
			while(true) {
				int child = index * 2 + 1;
				if (child > size - 1) {
					break;
				}
				if (child + 1 <= size - 1) {
					if (!is_lower(heap[child], heap[child+1])) {
						child = child + 1;
					}
//					if (is_lower(heap[child+1], heap[child])) {
//						child = child + 1;
//					}
				}
				if(is_lower(heap[child],irp)) {
					heap[index] = heap[child];
					index = child;
				}
				else {
					break;
				}
//				if(is_lower(irp,heap[child])) {
//					break;
//				}
//				else {
//					heap[index] = heap[child];
//					index = child;
//				}
			}
			heap[index] = irp;
			return (irp.score != min_score);
//			return true;
		}
		else return false;
	}


	public boolean add_KIFF(IntDoublePair irp) {
		boolean b = false;
		int place = 0;
		for (int index = 0; index < size; index++) {
			b = b || (heap[index].integer == irp.integer);
			if(b) {
				place = index;
				break;
			}
		}
		if (b) {
			IntDoublePair old_irp = heap[place];
			heap[place] = new IntDoublePair(old_irp.integer, old_irp.score + irp.score);
			int index = place;
			while(true) {
				int child = index * 2 + 1;
				if (child > size - 1) {
					break;
				}
				if (child + 1 <= size - 1) {
					if (is_lower(heap[child+1], heap[child])) {
						child = child + 1;
					}
				}
				if(is_lower(irp,heap[child])) {
					break;
				}
				else {
					heap[index] = heap[child];
					index = child;
				}
			}
			heap[index] = irp;
			return true;
		}
		else {
			if(is_lower(heap[0], irp)) {
				int index = 0;
				while(true) {
					int child = index * 2 + 1;
					if (child > size - 1) {
						break;
					}
					if (child + 1 <= size - 1) {
						if (is_lower(heap[child+1], heap[child])) {
							child = child + 1;
						}
					}
					if(is_lower(irp,heap[child])) {
						break;
					}
					else {
						heap[index] = heap[child];
						index = child;
					}
				}
				heap[index] = irp;
				return true;
			}
			else return false;
		}
	}





	public IntDoublePair[] toSortedArray() {
		IntDoublePair[] sortedArray = new IntDoublePair[size];
		IntDoublePair[] heap_aux = clone().toArray();

		for(int item = 0; item < size; item++) {
			sortedArray[size-1-item] = heap_aux[0];
			heap_aux[0] = heap_aux[size-1 - item];
			IntDoublePair irp = heap_aux[0];
			int index = 0;
			while(true) {
				int child = index * 2 + 1;
				if (child > size-1 - item) {
					break;
				}
				if (child + 1 <= size-1 - item) {
					if (is_lower(heap_aux[child+1], heap_aux[child])) {
						child = child + 1;
					}
				}
				if(is_lower(irp,heap_aux[child])) {
					break;
				}
				else {
					heap_aux[index] = heap_aux[child];
					index = child;
				}
			}
			heap_aux[index] = irp;
		}


		return sortedArray;
	}








//	public boolean contains(IntDoublePair irp) {
//		boolean b = false;
//		for (int index = 0; index < size; index++) {
//			b = b || (heap[index].integer == irp.integer);
//			if(b) {
//				break;
//			}
//		}
//		return b;
//	}

	public double min_score() {
		return heap[0].score;
	}

	public boolean is_bigger(IntDoublePair irp1, IntDoublePair irp2) {
		return (irp2.integer == -1 || irp1.score > irp2.score || (irp1.score == irp2.score && irp1.integer > irp2.integer));
	}

	public boolean can_be_added(IntDoublePair irp1) {
		IntDoublePair irp2 = heap[0];
//		return (irp2.integer == -1 || (irp2.score == 0 && irp1.integer != -1) || irp1.score > irp2.score);
		return (!hashset.contains(irp1.integer) && (irp2.integer == -1 || irp1.score > irp2.score));
	}

	public boolean is_lower(IntDoublePair irp1, IntDoublePair irp2) {
		return (irp1.integer == -1 || (irp1.score == 0 && irp2.integer != -1) || irp1.score < irp2.score);
//		return (irp1.integer == -1 || irp1.score < irp2.score || (irp1.score == irp2.score && irp1.integer < irp2.integer));
//		return (irp1.integer == -1 || irp1.score < irp2.score || (irp1.score == irp2.score && irp1.integer < irp2.score)); //FRec version
	}

	public boolean equals(KNN hp) {
		return this.itemArray().equals(hp.itemArray());
	}

	@Override
	public String toString() {
		String s = "[";
		boolean b = true;
		for(IntDoublePair idp: heap) {
			if (b) {
				b = false;
				s = s + "("+idp.integer+","+idp.score+")";
			}
			else {
				s = s + " ("+idp.integer+","+idp.score+")";
			}
			
		}
		s = s + "]";
		return s;
	}

	@Override
	public KNN clone() {
		if (heap != null) {
			IntDoublePair[] new_heap = new IntDoublePair[heap.length];
			for(int index = 0; index < heap.length; index++) {
				new_heap[index] = heap[index].clone();
			}
			return (new KNN(new_heap));
		}
		else {
			return null;
		}
	}

	@Override
	public boolean equals(KNN_i hp) {
		return equals((KNN) hp);
	}



}
