package util;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public final class KNNGraphHashMapKNN implements KNNG{

	private Map<Integer, KNN> knng;
	
	public KNNGraphHashMapKNN() {
		this.knng = new ConcurrentHashMap<Integer, KNN>();
	}
	public KNNGraphHashMapKNN(int nb_users) {
		this.knng = new ConcurrentHashMap<Integer, KNN>(nb_users);
	}
	
	public void init(int user, int k, double score_init) {
		this.knng.put(user, new KNN(k, score_init));
	}
	
	public boolean put(int user, int neighbor, double score) {
		return this.knng.get(user).add(new IntDoublePair(neighbor,score));
	}
	
	public boolean put(int user, IntDoublePair idp) {
		return this.knng.get(user).add(idp);
	}
	
	public void put(int user, KNN knn) {
		this.knng.put(user, knn);
	}
	
	public int[] get_neighbors_ids(int user) {
		return this.knng.get(user).itemArray();
	}
	
	public IntDoublePair[] get_neighbors(int user) {
		return this.knng.get(user).toArray();
	}
	
	public IntDoublePair[] get_sorted_neighbors(int user) {
		return this.knng.get(user).toSortedArray();
	}
	
	public boolean has_neighbors(int user) {
		return (this.knng.get(user) != null);
	}
	
	public Set<Integer> get_users() {
		return knng.keySet();
	}
	
	public KNN get_KNN(int user) {
		return knng.get(user);
	}
	
	
	
	
	
	
	@Override
	public KNNGraphHashMapKNN clone() {
		KNNGraphHashMapKNN clone = new KNNGraphHashMapKNN();
		for(int index: this.knng.keySet()) {
			clone.put(index, this.knng.get(index).clone());
		}
		return clone;
	}

	@Override
	public void put(int user, KNN_i knn) {
		this.knng.put(user, (KNN) knn);
	}
}
