package util;

import java.util.BitSet;

public final class BloomFilter {

	private final int nb_hash;
	private final int size_bitset;
	private final BitSet bloomfilter;
	
	public BloomFilter(int nb_hash, int size_bitset) {
		this.nb_hash = nb_hash;
		this.size_bitset = size_bitset;
		this.bloomfilter = new BitSet(size_bitset);
	}
	
	public void put(int item) {
		for(int i = 0; i < nb_hash; i++) {
//			int hash = util.HashFunctions.hash(item, size_bitset);
			int hash = util.HashFunctions.get_hash(item, i, size_bitset);
			bloomfilter.set(hash);
		}
	}
	
	public int get_size() {
		return size_bitset;
	}
	public int get_nb() {
		return nb_hash;
	}
	
	/** return a copy of the bitset **/
	public BitSet get_bitset() {
		return (BitSet) (bloomfilter.clone());
	}
	
	public int hamming(BloomFilter bf) {
		int hamming = 0;
		BitSet bs = (bf.get_bitset());
		bs.and(bloomfilter);
		hamming = bs.cardinality();
		return hamming;
	}
	
	public int cardinality() {
		int cardinality = bloomfilter.cardinality();
		return cardinality;
	}

}
