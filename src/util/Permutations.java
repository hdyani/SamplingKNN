package util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class Permutations {

	private final List<Integer> permutation;
	
//	public Permutations() {
//		permutation = null;
//	}
	
	public Permutations(Set<Integer> set) {
		permutation = new ArrayList<Integer>();
		for(int item:set) {
			permutation.add(item);
		}
		java.util.Collections.sort(permutation);
		java.util.Collections.shuffle(permutation);
	}
	
	public Permutations(Set<Integer> set, int seed) {
		permutation = new ArrayList<Integer>();
		for(int item:set) {
			permutation.add(item);
		}
		java.util.Collections.sort(permutation);
		Random rnd = new Random(seed);
		java.util.Collections.shuffle(permutation, rnd);
	}
	
	public int getHash(Set<Integer> set) {
		int index=0;
		
		for(int elem: permutation) {
			if (set.contains(elem)) {
				break;
			}
			index++;
		}
		
		return index;
	}
	
	public String toString() {
		return permutation.toString();
	}
	
	
}
