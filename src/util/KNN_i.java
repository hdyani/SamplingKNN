package util;

public interface KNN_i {


	public IntDoublePair[] toArray();

	/**
	 * Return the integers of the heap in an array, sorted by ratings, from the highest to the lowest.
	 * @return
	 */
	public int[] itemArray();


	/**
	 * Adding an int is the same as suppressing the lowest one (which is the root) and replace it by the new one.
	 * @param irp
	 */
	public boolean add(IntDoublePair irp);


	public IntDoublePair[] toSortedArray() ;


	public double min_score();


	public boolean equals(KNN_i hp) ;

	public String toString() ;

	public KNN_i clone();



}
