package util;

import java.util.Comparator;

public class Bucket_recComparator implements Comparator<Bucket_rec>{

	@Override
	public int compare(Bucket_rec b0, Bucket_rec b1) {
		return b0.compare(b1);
	}
//	@Override
	public boolean equals(Bucket_rec b0, Bucket_rec b1) {
		return b0.equals(b1);
	}

}
