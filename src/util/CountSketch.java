package util;

public class CountSketch {

	private final double[][] sketch;
	private final int number_rows;
	private final int number_columns;
	
	public CountSketch(int nb_rows, int nb_columns) {
		this.number_rows = nb_rows;
		this.number_columns = nb_columns;
		this.sketch = new double[number_rows][number_columns];
		for(int i=0; i < number_rows; i++) {
			for(int j=0; j<number_columns; j++) {
				sketch[i][j]=0;
			}
		}
	}
	
	public void incr(int n, double m) {
		int hash;
		int hash_sign;
		for(int i=0; i < number_rows; i++) {
			hash = HashFunctions.get_hash(n, i, number_columns);
			hash_sign = HashFunctions.get_hash(n, i, 2);
			if (hash_sign == 0) {
				hash_sign = -1;
			}
			sketch[i][hash] = sketch[i][hash]+hash_sign*m;
		}
	}
	
	public double get(int n) {
		double score = 0;
		int hash;
		int hash_sign;
		for(int i=0; i < number_rows; i++) {
			hash = HashFunctions.get_hash(n, i, number_columns);
			hash_sign = HashFunctions.get_hash(n, i, 2);
			if (hash_sign == 0) {
				hash_sign = -1;
			}
			score = score + sketch[i][hash]*hash_sign;
		}
		return (score/((double) number_rows));
	}
	
	
}
