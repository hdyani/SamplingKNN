package matrixFactorization;

public final class Ratings {
	
	private final int u;
	private final int i;
	private final double r;
	
	public Ratings(int u, int i, double r) {
		this.u = u;
		this.i = i;
		this.r = r;
	}

	public int getU() {
		return u;
	}
	
	public int getI() {
		return i;
	}
	
	public double getR() {
		return r;
	}
}
