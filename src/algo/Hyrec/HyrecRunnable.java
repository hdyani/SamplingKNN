package algo.Hyrec;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import parameters.ParametersHyrec;
import util.Counter;
import util.IntDoublePair;
import util.KNNGraph;

public final class HyrecRunnable implements Runnable{

	private final ParametersHyrec params;
	private final KNNGraph knng;
	private final KNNGraph old_knng;
	private final int loop;
	private final int[] users;
	private final Counter counter;

	

	public HyrecRunnable(ParametersHyrec params, int[] users, KNNGraph knng, KNNGraph old_knng, Counter counter, int loop) {
		this.users = users;
		this.knng = knng;
		this.old_knng = old_knng;
		this.loop = loop;
		this.counter = counter;
		this.params = params;
	}


	@Override
	public void run() {
		Random randomGenerator = new Random();
		int nb_user = users.length;

		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, params.nb_proc(), loop);
		
		
		Set<Integer> candidates;
		int random_user;
		int count = 0;
		double sim;
		boolean b;
		for(int user1 : user_ids) {
			candidates = new HashSet<Integer>();
			//Adding random neighbors.
			if(params.r()>0){
				for(int index = 0; index < params.r(); index++) {
					random_user = users[randomGenerator.nextInt(nb_user)];
					while(random_user == user1) { //really userful ?
						random_user = users[randomGenerator.nextInt(nb_user)];
					}
					candidates.add(random_user);
				}
			}

			for(IntDoublePair user2 : old_knng.get_neighbors(user1)) {
				if (user2.integer != user1) {
					if(old_knng.has_neighbors(user2.integer)) {
						for(IntDoublePair user3: old_knng.get_neighbors(user2.integer)) {
//			for(IntDoublePair user2 : old_knng.get(user1).toArray()) {
//				if (user2.integer != user1) {
//					if(old_knng.get(user2.integer) != null) {
//						for(IntDoublePair user3: old_knng.get(user2.integer).toArray()) {
							if(user3.integer != -1 && user3.integer != user1 && user3.integer > user1) {
								candidates.add(user3.integer);
							}
						}
					}
				}
			}
			
			for(int candidate: candidates) {
				sim = params.dataset().sim(user1, candidate);

				
//				if(ParametersStats.measure_scanrate) {
//					paramStats.inc();
//				}
				
//				IntDoublePair idp = new IntDoublePair(candidate,sim);
				b = knng.put(user1,candidate,sim);

				if(b) {
//					counter.inc();
					count++;
				}
//				idp = new IntDoublePair(user1,sim);
				if(knng.put(candidate,user1,sim)) {
//					counter.inc();
					count++;
				}
			}
		}
		counter.inc(count);
		
		
		
	}

}
