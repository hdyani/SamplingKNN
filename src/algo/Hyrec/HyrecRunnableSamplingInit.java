package algo.Hyrec;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import parameters.ParametersHyrec;
import util.IntDoublePair;
import util.KNN;
import util.KNNGraph;

public final class HyrecRunnableSamplingInit implements Runnable{

	private final int[] users;
	private final ParametersHyrec params;
	private final KNNGraph knng;
	private final KNNGraph old_knng;
	private final int loop;
	
	public HyrecRunnableSamplingInit(ParametersHyrec params, int[] users, KNNGraph knng, KNNGraph old_knng, int loop) {
		this.users = users;
		this.knng = knng;
		this.old_knng = old_knng;
		this.loop = loop;
		this.params = params;
	}
	
	
	@Override
	public void run() {

		Random randomGenerator = new Random();

		int nb_user = users.length;
		
		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, params.nb_proc(), loop);
		
		Set<Integer> random_neighbors;
		KNN neighbors;
		int random_user;
		for(int user: user_ids) {
			random_neighbors = new HashSet<Integer>();
			while (random_neighbors.size() != params.k()) {
				random_user = users[randomGenerator.nextInt(nb_user)];
				while(random_user == user) {
					random_user = users[randomGenerator.nextInt(nb_user)];
				}
				random_neighbors.add(random_user);
			}
			
//			neighbors = new KNN(params.k(), params.dataset().getInitValue());
			neighbors = new KNN(params.k(), params.dataset().getInitValue()-1);
			for(int random_user_id: random_neighbors) {
				
				neighbors.add(new IntDoublePair(random_user_id, params.dataset().sim(user, random_user_id)));
//				if(ParametersStats.measure_scanrate) {
//					paramStats.inc();
//				}
			}
			
			knng.put(user, neighbors);
			old_knng.put(user, neighbors.clone());
		}
		
	}

}
