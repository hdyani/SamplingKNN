package algo.ClusterAndConquer_rec;

import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;

import algo.Hyrec.HyrecRunnable;
import algo.Hyrec.HyrecRunnableSamplingInit;
import dataset.Dataset;
import parameters.ParametersHybrid_MH_rec;
import parameters.ParametersHyrec;
import util.Bucket_rec;
import util.Counter;
import util.KNN;
import util.KNNGraph;

public final class HybridRunnable implements Runnable{

	private final ParametersHybrid_MH_rec params;
	private final Dataset dataset;
	
	private final LinkedList<KNNGraph> knngs;
	private final PriorityBlockingQueue<Bucket_rec> nextCluster;
	
	

	public HybridRunnable(ParametersHybrid_MH_rec params, LinkedList<KNNGraph> knngs, PriorityBlockingQueue<Bucket_rec> nextCluster, Dataset dataset) {
		this.knngs = knngs;
		this.params = params;
		this.nextCluster=nextCluster;
		this.dataset = dataset;
	}


	@Override
	public void run() {
		
		Integer[] users_;
		int[] users;
		int index;
		
		Dataset local_dataset;
		KNNGraph knng;

		double sim;
		
		Set<Integer> user_aux;
		Bucket_rec bucket;

		while ((bucket=nextCluster.poll())!=null) {

			user_aux=bucket.get_users();
			

			
			
			//Getting the corresponding users
//			user_aux = buckets[index_i][index_j];
			users_ = new Integer[user_aux.size()];
			users = new int[user_aux.size()];
			user_aux.toArray(users_);
			index = 0;
			for(int user: users_) {
				users[index] = user;
				index++;
			}
			
			//Creating the corresponding KNNGraph
			knng = new KNNGraph(params.k(),users.length,params.dataset().getInitValue()-1);
			
			//Getting the corresponding dataset => needed?
			local_dataset = dataset.subDataset(users);
			
			//If 5 * k * k < n then perform Hyrec, else perform BruteForce

			if(5*params.k()*params.k()<users.length) {
				ParametersHyrec params_ = new ParametersHyrec(params.k(), params.ParametersToMap().get("Name"), local_dataset, 1);
				params_.set_r(0);
				params_.set_iterations(30);
				params_.set_delta(0.001);
				KNNGraph old_knng = new KNNGraph(params_.k(),users.length,local_dataset.getInitValue()-1);
				HyrecRunnableSamplingInit hyrecInit = new HyrecRunnableSamplingInit(params_, users, knng, old_knng, 0);
				hyrecInit.run();

				
				Counter counter = new Counter();
				for (int iter_hyrec = 0; iter_hyrec < params_.iterations(); iter_hyrec++) {
					counter.init();
					HyrecRunnable hyrecRun = new HyrecRunnable(params_, users, knng, old_knng, counter, 0);
					hyrecRun.run();
					if(counter.getValue() < params_.delta() * params_.k() * users.length) {
						break;
					}
					old_knng = knng.clone();
				}
			}
			else { //BRUTE FORCE
				for(int user_id: users) {
					knng.put(user_id, new KNN(params.k(), params.dataset().getInitValue()-1));
				}

				for(int user_id: users) {
					for(int user: users) {
						if(user < user_id) {
							sim = local_dataset.sim(user_id, user);
							knng.put(user_id,user,sim);
							knng.put(user, user_id,sim);
						}
					}
				}
			}
			knngs.add(knng);
		}
		
	}

}
