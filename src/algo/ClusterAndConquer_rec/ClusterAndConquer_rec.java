package algo.ClusterAndConquer_rec;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;

import algo.Algo;
import parameters.Parameters;
import parameters.ParametersHybrid_MH_rec;
import parameters.ParametersStats;
import util.Bucket_rec;
import util.Bucket_recComparator;
import util.Counter;
import util.CounterLong;
import util.KNNGraph;

public final class ClusterAndConquer_rec implements Algo {

	/*
	 * Cluster and Conquer algorithm with recursivity, using a thread pool
	 */

	private final ParametersHybrid_MH_rec params;
	private final ParametersStats paramStats;
	private final int nb_bits;

	public ClusterAndConquer_rec(ParametersHybrid_MH_rec params, ParametersStats paramStats) {
		this.params = params;
		this.paramStats = paramStats;
		this.nb_bits = params.nb_bits();
	}

	@Override
	public KNNGraph doKNN(){

		//		int nb_bits = 1024;

		long start = 0;
		long start_=0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		if(ParametersStats.measure_scanrate) {
			paramStats.init_time_proc(params.nb_proc());
			start_ = start;
		}

		/*
		 * CREATION OF THE BUCKETS (SEQ) AND THE PERMUTATIONS (DIST)
		 */

		@SuppressWarnings("unchecked")
		LinkedList<KNNGraph>[] knngs = (LinkedList<KNNGraph>[]) new LinkedList[params.nb_proc()];


		@SuppressWarnings("unchecked")
		HashSet<Integer>[][] buckets = (HashSet<Integer>[][]) new HashSet[params.nb_hash()][nb_bits];
		for(int i = 0; i < params.nb_hash(); i++) {
			for(int j=0; j < nb_bits; j++) {
				buckets[i][j] = new HashSet<Integer>();
			}
		}


		if(ParametersStats.measure_scanrate) {
			paramStats.set_time_creating_bucket(System.currentTimeMillis()-start_);
			start_ = System.currentTimeMillis();
		}


		/*
		 * FILLING THE BUCKETS (DIST)
		 * */

		Set<Integer> user_aux = params.dataset().getUsers();
		Integer[] users_ = new Integer[user_aux.size()];
		int[] users = new int[user_aux.size()];
		user_aux.toArray(users_);
		int index = 0;
		for(int user: users_) {
			users[index] = user;
			index++;
		}



		//SEQUENTIAL FILLING
		int bucket_id;
		int hash;
		for(int user: users) {
			for(int permutation = 0; permutation < params.nb_hash(); permutation++) {
				bucket_id = nb_bits;
				for(int item : params.datasetHash().getRatedItems(user)) {
					hash = util.HashFunctions.get_hash(item, permutation, nb_bits);
					if(hash<bucket_id) {
						bucket_id = hash;
					}
				}
				buckets[permutation][bucket_id].add(user);
			}
		}



		if(ParametersStats.measure_scanrate) {
			paramStats.set_time_filling_bucket(System.currentTimeMillis()-start_);
			start_ = System.currentTimeMillis();
		}


		/*
		 *CREATION OF THE THREAD POOL
		 */
		PriorityBlockingQueue<Bucket_rec> nextCluster = new PriorityBlockingQueue<Bucket_rec>(1000,new Bucket_recComparator());
		
		

		Set<Integer> set_aux;
		for(int i=0; i<params.nb_hash();i++) {
			for(int j=0; j<nb_bits; j++) {
				set_aux = buckets[i][j];
				if(set_aux != null && set_aux.size()>1) {
					nextCluster.add(new Bucket_rec(i,j,set_aux));
				}
			}
		}
		buckets=null;
		
		/*
		 *SPLITTING RECURSIVELY THE BUCKETS TOO LARGE 
		 */
		Set<Bucket_rec> outcasts_clusters = new HashSet<Bucket_rec>();
		Bucket_rec bucket;
		int seed;
		int new_bucket_id;
		@SuppressWarnings("unchecked")
		HashSet<Integer>[] local_buckets = (HashSet<Integer>[]) new HashSet[nb_bits];
		HashSet<Integer> outcasts_set;
		while(nextCluster.peek().get_users().size() > params.nb_hash_try()) {
			bucket = nextCluster.poll();
			seed = bucket.seed();
			bucket_id = bucket.bucket_id();
			outcasts_set = new HashSet<Integer>();
			for(int i=0; i<nb_bits; i++) {
				local_buckets[i] = new HashSet<Integer>();
			}
			for(int user: bucket.get_users()) {
				new_bucket_id = nb_bits;
				for(int item : params.datasetHash().getRatedItems(user)) {
					hash = util.HashFunctions.get_hash(item, seed, nb_bits);
					if(hash > bucket_id && hash<nb_bits) {
						new_bucket_id = hash;
					}
				}
				if(new_bucket_id > bucket_id && new_bucket_id != nb_bits) {
					local_buckets[new_bucket_id].add(user);
				}
				else {
					outcasts_set.add(user);
				}
			}
			for(int j=0; j<nb_bits; j++) {
				set_aux = local_buckets[j];
				if(set_aux != null && set_aux.size()>1) {
					nextCluster.add(new Bucket_rec(seed,j,set_aux));
				}
				if (set_aux.size()==1) {
					outcasts_set.addAll(set_aux);
				}
			}
//			if(outcasts_set.size()>params.nb_hash_try()) {
//			}
//			nextCluster.add(new Bucket_rec(seed,bucket_id,outcasts_set));
			outcasts_clusters.add(new Bucket_rec(seed,bucket_id,outcasts_set));
		}
		bucket=null;
		local_buckets=null;
		for (Bucket_rec bucket_: outcasts_clusters) {
			nextCluster.add(bucket_);
		}

		if(ParametersStats.measure_scanrate) {
			paramStats.set_time_distribution_bucket(System.currentTimeMillis()-start_);
			//			start_ = System.currentTimeMillis();
		}

		/*
		 * CREATION OF THE KNN
		 */


		//		bucket_start = System.currentTimeMillis();

		Counter[] counters = new Counter[params.nb_proc()];

		CounterLong[] counters_time = new CounterLong[params.nb_proc()];




		Thread[] threads = new Thread[params.nb_proc()];
		for(int i = 0; i < params.nb_proc(); i++) {
			knngs[i] = new LinkedList<KNNGraph>();
			if(ParametersStats.measure_scanrate) {
				counters[i] = new Counter();
				counters_time[i] = new CounterLong();
				threads[i] = new Thread(new HybridRunnableScan(params, knngs[i], counters[i], counters_time[i], nextCluster, params.dataset()));
			}
			else {
				threads[i] = new Thread(new HybridRunnable(params, knngs[i], nextCluster, params.dataset()));
			}
			threads[i].start();
		}

		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		if(ParametersStats.measure_scanrate) {
			start_ = System.currentTimeMillis();
		}
		HybridMeltKNNG[] melt = new HybridMeltKNNG[params.nb_proc()];
		for(int i = 0; i < params.nb_proc(); i++) {
			melt[i] = new HybridMeltKNNG(params, knngs[i]);
			threads[i] = new Thread(melt[i]);
			threads[i].start();
		}

		LinkedList<KNNGraph> knngs_ = new LinkedList<KNNGraph>();
		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
				knngs_.add(melt[i].getKNNG());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		melt[0] = new HybridMeltKNNG(params, knngs_);
		melt[0].run();



		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}

		if(ParametersStats.measure_scanrate) {
			paramStats.set_time_merging_bucket(System.currentTimeMillis()-start_);
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters[i].getValue();
				paramStats.set_time_proc(i, counters_time[i].getValue());
			}
			paramStats.increase_scanrate(scanrate);
		}


		return melt[0].getKNNG();
	}












	/*
	 * doKNN(int[] users) will do the KNN of each user in users, using the users of ALL the dataset
	 * */

	@Override
	public KNNGraph doKNN(int[] users){

		//		int nb_bits = 1024;

		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}

		/*
		 * CREATION OF THE BUCKETS (SEQ) AND THE PERMUTATIONS (DIST)
		 */


		@SuppressWarnings("unchecked")
		LinkedList<KNNGraph>[] knngs = (LinkedList<KNNGraph>[]) new Object[params.nb_proc()];





		@SuppressWarnings("unchecked")
		HashSet<Integer>[][] buckets = (HashSet<Integer>[][]) new HashSet[params.nb_hash()][nb_bits];
		for(int i = 0; i < params.nb_hash(); i++) {
			for(int j=0; j < nb_bits; j++) {
				buckets[i][j] = new HashSet<Integer>();
			}
		}





		/*
		 * FILLING THE BUCKETS (DIST)
		 * */

		//SEQUENTIAL FILLING
		int bucket_id;
		int hash;
		for(int user: users) {
			for(int permutation = 0; permutation < params.nb_hash(); permutation++) {
				bucket_id = nb_bits;
				for(int item : params.datasetHash().getRatedItems(user)) {
					hash = util.HashFunctions.get_hash(item, permutation, nb_bits);
					if(hash<bucket_id) {
						bucket_id = hash;
					}
				}
				buckets[permutation][bucket_id].add(user);
			}
		}



		if(ParametersStats.measure_scanrate) {
			paramStats.set_time_filling_bucket(System.currentTimeMillis()-start);
			start = System.currentTimeMillis();
		}


		/*
		 *CREATION OF THE THREAD POOL
		 */
		PriorityBlockingQueue<Bucket_rec> nextCluster = new PriorityBlockingQueue<Bucket_rec>(1000,new Bucket_recComparator());
		
		

		Set<Integer> set_aux;
		for(int i=0; i<params.nb_hash();i++) {
			for(int j=0; j<nb_bits; j++) {
				set_aux = buckets[i][j];
				if(set_aux != null && set_aux.size()>1) {
					nextCluster.add(new Bucket_rec(i,j,set_aux));
				}
			}
		}
		buckets=null;
		
		/*
		 *SPLITTING RECURSIVELY THE BUCKETS TOO LARGE 
		 */
		Bucket_rec bucket;
		int seed;
		int new_bucket_id;
		@SuppressWarnings("unchecked")
		HashSet<Integer>[] local_buckets = (HashSet<Integer>[]) new HashSet[nb_bits];
		HashSet<Integer> outcasts_set;
		while(nextCluster.element().get_users().size() > params.nb_hash_try()) {
			bucket = nextCluster.remove();
			seed = bucket.seed();
			bucket_id = bucket.bucket_id();
			outcasts_set = new HashSet<Integer>();
			for(int i=0; i<nb_bits; i++) {
				local_buckets[i] = new HashSet<Integer>();
			}
			for(int user: bucket.get_users()) {
				new_bucket_id = nb_bits;
				for(int item : params.datasetHash().getRatedItems(user)) {
					hash = util.HashFunctions.get_hash(item, seed, nb_bits);
					if(hash > bucket_id && hash<bucket_id) {
						new_bucket_id = hash;
					}
				}
				if(new_bucket_id > bucket_id && new_bucket_id != nb_bits) {
					local_buckets[new_bucket_id].add(user);
				}
				else {
					outcasts_set.add(user);
				}
			}
			for(int j=0; j<nb_bits; j++) {
				set_aux = local_buckets[j];
				if(set_aux != null && set_aux.size()>1) {
					nextCluster.add(new Bucket_rec(seed,j,set_aux));
				}
				if (set_aux.size()==1) {
					outcasts_set.addAll(set_aux);
				}
			}
			nextCluster.add(new Bucket_rec(seed,bucket_id,outcasts_set));
		}
		bucket=null;
		local_buckets=null;


		if(ParametersStats.measure_scanrate) {
			paramStats.set_time_distribution_bucket(System.currentTimeMillis()-start);
			//			start_ = System.currentTimeMillis();
		}


		/*
		 * CREATION OF THE KNN
		 */


		Counter[] counters = new Counter[params.nb_proc()];
		CounterLong[] counters_time = new CounterLong[params.nb_proc()];




		Thread[] threads = new Thread[params.nb_proc()];
		for(int i = 0; i < params.nb_proc(); i++) {
			knngs[i] = new LinkedList<KNNGraph>();
			if(ParametersStats.measure_scanrate) {
				counters[i] = new Counter();
				counters_time[i] = new CounterLong();
				threads[i] = new Thread(new HybridRunnableScan(params, knngs[i], counters[i], counters_time[i], nextCluster, params.dataset()));
			}
			else {
				threads[i] = new Thread(new HybridRunnable(params, knngs[i], nextCluster, params.dataset()));
			}
			threads[i].start();
		}

		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		HybridMeltKNNG[] melt = new HybridMeltKNNG[params.nb_proc()];
		for(int i = 0; i < params.nb_proc(); i++) {
			melt[i] = new HybridMeltKNNG(params, knngs[i]);
			threads[i] = new Thread(melt[i]);
			threads[i].start();
		}

		LinkedList<KNNGraph> knngs_ = new LinkedList<KNNGraph>();
		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
				knngs_.add(melt[i].getKNNG());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		melt[0] = new HybridMeltKNNG(params, knngs_);
		melt[0].run();


		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}

		if(ParametersStats.measure_scanrate) {
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters[i].getValue();
			}
			paramStats.increase_scanrate(scanrate);
		}

		return melt[0].getKNNG();
	}








	@Override
	public KNNGraph doKNN(KNNGraph anng) {

		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}

		/*
		 * CREATION OF THE BUCKETS (SEQ) AND THE PERMUTATIONS (DIST)
		 */


		@SuppressWarnings("unchecked")
		LinkedList<KNNGraph>[] knngs = (LinkedList<KNNGraph>[]) new Object[params.nb_proc()];




		@SuppressWarnings("unchecked")
		HashSet<Integer>[][] buckets = (HashSet<Integer>[][]) new HashSet[params.nb_hash()][nb_bits];
		for(int i = 0; i < params.nb_hash(); i++) {
			for(int j=0; j < nb_bits; j++) {
				buckets[i][j] = new HashSet<Integer>();
			}
		}






		/*
		 * FILLING THE BUCKETS (DIST)
		 * */
		Set<Integer> user_aux = anng.get_users();
		Integer[] users_ = new Integer[user_aux.size()];
		int[] users = new int[user_aux.size()];
		user_aux.toArray(users_);
		int index = 0;
		for(int user: users_) {
			users[index] = user;
			index++;
		}

		//SEQUENTIAL FILLING
		int bucket_id;
		int hash;
		for(int user: users) {
			for(int permutation = 0; permutation < params.nb_hash(); permutation++) {
				bucket_id = nb_bits;
				for(int item : params.datasetHash().getRatedItems(user)) {
					hash = util.HashFunctions.get_hash(item, permutation, nb_bits);
					if(hash<bucket_id) {
						bucket_id = hash;
					}
				}
				buckets[permutation][bucket_id].add(user);
			}
		}



		if(ParametersStats.measure_scanrate) {
			paramStats.set_time_filling_bucket(System.currentTimeMillis()-start);
			start = System.currentTimeMillis();
		}


		/*
		 *CREATION OF THE THREAD POOL
		 */
		PriorityBlockingQueue<Bucket_rec> nextCluster = new PriorityBlockingQueue<Bucket_rec>(1000,new Bucket_recComparator());
		
		

		Set<Integer> set_aux;
		for(int i=0; i<params.nb_hash();i++) {
			for(int j=0; j<nb_bits; j++) {
				set_aux = buckets[i][j];
				if(set_aux != null && set_aux.size()>1) {
					nextCluster.add(new Bucket_rec(i,j,set_aux));
				}
			}
		}
		buckets=null;
		
		/*
		 *SPLITTING RECURSIVELY THE BUCKETS TOO LARGE 
		 */
		Bucket_rec bucket;
		int seed;
		int new_bucket_id;
		@SuppressWarnings("unchecked")
		HashSet<Integer>[] local_buckets = (HashSet<Integer>[]) new HashSet[nb_bits];
		HashSet<Integer> outcasts_set;
		while(nextCluster.element().get_users().size() > params.nb_hash_try()) {
			bucket = nextCluster.remove();
			seed = bucket.seed();
			bucket_id = bucket.bucket_id();
			outcasts_set = new HashSet<Integer>();
			for(int i=0; i<nb_bits; i++) {
				local_buckets[i] = new HashSet<Integer>();
			}
			for(int user: bucket.get_users()) {
				new_bucket_id = nb_bits;
				for(int item : params.datasetHash().getRatedItems(user)) {
					hash = util.HashFunctions.get_hash(item, seed, nb_bits);
					if(hash > bucket_id && hash<bucket_id) {
						new_bucket_id = hash;
					}
				}
				if(new_bucket_id > bucket_id && new_bucket_id != nb_bits) {
					local_buckets[new_bucket_id].add(user);
				}
				else {
					outcasts_set.add(user);
				}
			}
			for(int j=0; j<nb_bits; j++) {
				set_aux = local_buckets[j];
				if(set_aux != null && set_aux.size()>1) {
					nextCluster.add(new Bucket_rec(seed,j,set_aux));
				}
				if (set_aux.size()==1) {
					outcasts_set.addAll(set_aux);
				}
			}
			nextCluster.add(new Bucket_rec(seed,bucket_id,outcasts_set));
		}
		bucket=null;
		local_buckets=null;


		if(ParametersStats.measure_scanrate) {
			paramStats.set_time_distribution_bucket(System.currentTimeMillis()-start);
			//			start_ = System.currentTimeMillis();
		}

		/*
		 * CREATION OF THE KNN
		 */


		//		bucket_start = System.currentTimeMillis();

		Counter[] counters = new Counter[params.nb_proc()];
		CounterLong[] counters_time = new CounterLong[params.nb_proc()];




		Thread[] threads = new Thread[params.nb_proc()];
		for(int i = 0; i < params.nb_proc(); i++) {
			knngs[i] = new LinkedList<KNNGraph>();
			if(ParametersStats.measure_scanrate) {
				counters[i] = new Counter();
				counters_time[i] = new CounterLong();
				threads[i] = new Thread(new HybridRunnableScan(params, knngs[i], counters[i], counters_time[i], nextCluster, params.dataset()));
			}
			else {
				threads[i] = new Thread(new HybridRunnable(params, knngs[i], nextCluster, params.dataset()));
			}
			threads[i].start();
		}

		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}


		HybridMeltKNNG[] melt = new HybridMeltKNNG[params.nb_proc()];
		for(int i = 0; i < params.nb_proc(); i++) {
			melt[i] = new HybridMeltKNNG(params, knngs[i]);
			threads[i] = new Thread(melt[i]);
			threads[i].start();
		}

		LinkedList<KNNGraph> knngs_ = new LinkedList<KNNGraph>();
		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
				knngs_.add(melt[i].getKNNG());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		melt[0] = new HybridMeltKNNG(params, knngs_);
		melt[0].run();


		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}

		if(ParametersStats.measure_scanrate) {
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters[i].getValue();
			}
			paramStats.increase_scanrate(scanrate);
		}

		return melt[0].getKNNG();
	}


	//	public Set<IntIntPair> newHashSet() {
	//		return ConcurrentHashMap.newKeySet();
	//	}


	//	public void SetParameters(Parameters params) {
	//		this.params = (ParametersBruteForce) params;
	//	}

	@Override
	public Parameters GetParameters() {
		return params;
	}

	@Override
	public ParametersStats GetStatsParameters() {
		return paramStats;
	}
}
