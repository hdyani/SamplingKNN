package algo.LSH_fast_hash;

import util.Permutations1024;

public final class LSHCreationPermutationsRunnable implements Runnable{
//	private final HashSet<Integer> items;
	private final int nb_buckets;
	private Permutations1024 permutation;
	private final int loop;
	

//	public LSHCreationPermutationsRunnable(HashSet<Integer> items, int loop) {
	public LSHCreationPermutationsRunnable(int loop, int nb_buckets) {
//		this.items = items;
		this.nb_buckets = nb_buckets;
		this.loop = loop;
	}


	@Override
	public void run() {
		permutation = new Permutations1024(loop,nb_buckets);
	}
	
	public Permutations1024 get_permutation() {
		return this.permutation;
	}

}
