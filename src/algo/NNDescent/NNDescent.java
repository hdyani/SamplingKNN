package algo.NNDescent;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import algo.Algo;
import parameters.Parameters;
import parameters.ParametersNNDescent;
import parameters.ParametersStats;
import util.Counter;
import util.KNNGraph;
import util.KNNGraphFlags;

public final class NNDescent implements Algo {

	private final ParametersNNDescent params;
	private final ParametersStats paramStats;
	
	public NNDescent(ParametersNNDescent params, ParametersStats paramStats) {
		this.params = params;
		this.paramStats = paramStats;
	}

	@Override
	public KNNGraph doKNN() {
		
//		AtomicLong scanrateCounter = new AtomicLong();
		
		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		
		
//		Random randomGenerator = new Random();
//		Map<Integer,KNNFlags> B = new ConcurrentHashMap<Integer,KNNFlags>();
		KNNGraphFlags B = new KNNGraphFlags();


		Thread[] threads = new Thread[params.nb_proc()];
		Counter[] counters = new Counter[params.nb_proc()];
		Counter[] counters_scanrate = new Counter[params.nb_proc()];

		int nb_user = params.dataset().getUsers().size();
		Object[] users_aux = params.dataset().getUsers().toArray();

		//DEF OF USERS, used to do random sampling.
		int[] users = new int[nb_user];
		for(int index = 0; index < nb_user; index++) {
			users[index] = (int) users_aux[index];
		}


		//INIT OF B
		
		for(int i = 0; i < params.nb_proc(); i++) {
//			Thread t = new Thread(new NNDescentRunnableSamplingInit(params, paramStats, users, B, i));
//			t.start();
//			threads[i] = t;
			counters[i] = new Counter();
			if(ParametersStats.measure_scanrate) {
				counters_scanrate[i] = new Counter();
				threads[i] = new Thread(new NNDescentRunnableSamplingInitScan(params, users, B, counters_scanrate[i], i));
			}
			else {
				threads[i] = new Thread(new NNDescentRunnableSamplingInit(params, users, B, i));
			}
			threads[i].start();
		}
		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		

		//MAIN LOOP
//		System.out.println(scanrateCounter.get() + " comparisons after initialization");
		if(ParametersStats.measure_scanrate) {
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters_scanrate[i].getValue();
			}
			paramStats.increase_scanrate(scanrate);
			paramStats.add_iter();
		}
		Map<Integer, Set<Integer>> oldneighbors;
		Map<Integer, Set<Integer>> newneighbors;
		Map<Integer, Set<Integer>> oldneighbors_;
		Map<Integer, Set<Integer>> newneighbors_;
		int counter;
		for (int iter = 0; iter < params.iterations(); iter++) {
//			Map<Integer, Set<IntDoubleBoolTriplet>> oldneighbors = new ConcurrentHashMap<Integer, Set<IntDoubleBoolTriplet>>();
//			Map<Integer, Set<IntDoubleBoolTriplet>> newneighbors = new ConcurrentHashMap<Integer, Set<IntDoubleBoolTriplet>>();
			oldneighbors = new ConcurrentHashMap<Integer, Set<Integer>>();
			newneighbors = new ConcurrentHashMap<Integer, Set<Integer>>();


			for(int i = 0; i < params.nb_proc(); i++) {
//				Thread t = new Thread(new NNDescentRunnableOldNew(params, users, B, newneighbors, oldneighbors, i));
//				t.start();
//				threads[i] = t;
				threads[i] = new Thread(new NNDescentRunnableOldNew(params, users, B, newneighbors, oldneighbors, i));
				threads[i].start();
			}
			for(int i = 0; i < params.nb_proc(); i++) {
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}


			oldneighbors_ = reverse(oldneighbors);
			newneighbors_ = reverse(newneighbors);

			//			int nb_changes = 0;
//			AtomicInteger counter = new AtomicInteger();

			for(int i = 0; i < params.nb_proc(); i++) {
//				Thread t = new Thread(new NNDescentPCHMRunnable(dataset, rho, k, users, B, newneighbors, oldneighbors, newneighbors_, oldneighbors_, counter, nb_proc, i));

//				Counter counter = new Counter();
//				Thread t = new Thread(new NNDescentRunnable(params, paramStats, users, B, newneighbors, oldneighbors, newneighbors_, oldneighbors_, counter, i));
//				counters[i] = counter;
//				t.start();
//				threads[i] = t;
				counters[i].init();
				if(ParametersStats.measure_scanrate) {
					counters_scanrate[i].init();
					threads[i] = new Thread(new NNDescentRunnableScan(params, users, B, newneighbors, oldneighbors, newneighbors_, oldneighbors_, counters[i], counters_scanrate[i], i));
				}
				else {
					threads[i] = new Thread(new NNDescentRunnable(params, users, B, newneighbors, oldneighbors, newneighbors_, oldneighbors_, counters[i], i));
				}
				threads[i].start();
			}
			for(int i = 0; i < params.nb_proc(); i++) {
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			counter = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				counter = counter + counters[i].getValue();
			}
			
//			System.out.println(counter + " changes for round " + iter);
//			System.out.println(scanrateCounter.get() + " comparisons in total at round " + iter);
			if(ParametersStats.measure_scanrate) {
				long scanrate = 0;
				for(int i = 0; i < params.nb_proc(); i++) {
					scanrate = scanrate + counters_scanrate[i].getValue();
				}
				paramStats.increase_scanrate(scanrate);
				paramStats.add_iter();
				paramStats.add_iter_changes(counter);
			}

//			System.out.println(counter + " out of " + params.delta() * params.k() * nb_user);
//			if (counter.intValue() < delta * nb_user * k) {
			if (counter < params.delta() * nb_user * params.k()) {
				break;
			}
		}

//		System.out.println(scanrateCounter.get() + " comparisons in total at the end.");
//		System.out.println((dataset.getUsers().size() * (dataset.getUsers().size() - 1))/2 + " comparisons at max:");
//		System.out.println("scanrate: " + (scanrateCounter.get()/((long) (dataset.getUsers().size() * (dataset.getUsers().size() - 1))/2) ) );

		//Converting B of type Hashtable<Integer, HeapItemRatingPair> to Hashtable<Integer, int[]>
//		KNNGraph knng = new KNNGraph(params.k(),params.dataset().getUsers().size(),params.dataset().getInitValue());
////		KNNGraph knng = new KNNGraph();
//		for (int user: B.get_users()) {
//			knng.put(user, B.get_KNN(user).toKNN());
//		}
		KNNGraph knng = B.toKNNGraph();
		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}
		
		return knng;
	}

	

	/*
	 * doKNN(int[] users) will do the KNN of the user in users, using ONLY the users of in users
	 * */
	@Override
	public KNNGraph doKNN(int[] users) {
		
//		AtomicLong scanrateCounter = new AtomicLong();
		
		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		
		
//		Random randomGenerator = new Random();
		KNNGraphFlags B = new KNNGraphFlags();


		Thread[] threads = new Thread[params.nb_proc()];
		Counter[] counters = new Counter[params.nb_proc()];
		Counter[] counters_scanrate = new Counter[params.nb_proc()];

		int nb_user = params.dataset().getUsers().size();
//		Object[] users_aux = params.dataset().getUsers().toArray();

		//DEF OF USERS, used to do random sampling.
//		int[] users = new int[nb_user];
//		for(int index = 0; index < nb_user; index++) {
//			users[index] = (int) users_aux[index];
//		}


		//INIT OF B
		
		for(int i = 0; i < params.nb_proc(); i++) {
//			Thread t = new Thread(new NNDescentRunnableSamplingInit(params, paramStats, users, B, i));
//			t.start();
//			threads[i] = t;
			counters[i] = new Counter();
			if(ParametersStats.measure_scanrate) {
				counters_scanrate[i] = new Counter();
				threads[i] = new Thread(new NNDescentRunnableSamplingInitScan(params, users, B, counters_scanrate[i], i));
			}
			else {
				threads[i] = new Thread(new NNDescentRunnableSamplingInit(params, users, B, i));
			}
			threads[i].start();
		}
		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		

		//MAIN LOOP
//		System.out.println(scanrateCounter.get() + " comparisons after initialization");
		if(ParametersStats.measure_scanrate) {
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters_scanrate[i].getValue();
			}
			paramStats.increase_scanrate(scanrate);
			paramStats.add_iter();
		}
		Map<Integer, Set<Integer>> oldneighbors;
		Map<Integer, Set<Integer>> newneighbors;
		Map<Integer, Set<Integer>> oldneighbors_;
		Map<Integer, Set<Integer>> newneighbors_;
		int counter;
		for (int iter = 0; iter < params.iterations(); iter++) {
			oldneighbors = new ConcurrentHashMap<Integer, Set<Integer>>();
			newneighbors = new ConcurrentHashMap<Integer, Set<Integer>>();


			for(int i = 0; i < params.nb_proc(); i++) {
//				Thread t = new Thread(new NNDescentRunnableOldNew(params, users, B, newneighbors, oldneighbors, i));
//				t.start();
//				threads[i] = t;
				counters_scanrate[i].init();
				threads[i] = new Thread(new NNDescentRunnableOldNew(params, users, B, newneighbors, oldneighbors, i));
				threads[i].start();
			}
			for(int i = 0; i < params.nb_proc(); i++) {
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}


			oldneighbors_ = reverse(oldneighbors);
			newneighbors_ = reverse(newneighbors);

			//			int nb_changes = 0;
//			AtomicInteger counter = new AtomicInteger();

			for(int i = 0; i < params.nb_proc(); i++) {
//				Thread t = new Thread(new NNDescentPCHMRunnable(dataset, rho, k, users, B, newneighbors, oldneighbors, newneighbors_, oldneighbors_, counter, nb_proc, i));
				
//				Counter counter = new Counter();
//				Thread t = new Thread(new NNDescentRunnable(params, paramStats, users, B, newneighbors, oldneighbors, newneighbors_, oldneighbors_, counter, i));
//				counters[i] = counter;
//				t.start();
//				threads[i] = t;
				counters[i].init();
				if(ParametersStats.measure_scanrate) {
					counters_scanrate[i].init();
					threads[i] = new Thread(new NNDescentRunnableScan(params, users, B, newneighbors, oldneighbors, newneighbors_, oldneighbors_, counters[i], counters_scanrate[i], i));
				}
				else {
					threads[i] = new Thread(new NNDescentRunnable(params, users, B, newneighbors, oldneighbors, newneighbors_, oldneighbors_, counters[i], i));
				}
				threads[i].start();
			}
			for(int i = 0; i < params.nb_proc(); i++) {
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			counter = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				counter = counter + counters[i].getValue();
			}
			
//			System.out.println(counter + " changes for round " + iter);
//			System.out.println(scanrateCounter.get() + " comparisons in total at round " + iter);
			if(ParametersStats.measure_scanrate) {
				long scanrate = 0;
				for(int i = 0; i < params.nb_proc(); i++) {
					scanrate = scanrate + counters_scanrate[i].getValue();
				}
				paramStats.increase_scanrate(scanrate);
				paramStats.add_iter();
				paramStats.add_iter_changes(counter);
			}

			System.out.println(counter + " out of " + params.delta() * params.k() * nb_user);
//			if (counter.intValue() < delta * nb_user * k) {
			if (counter < params.delta() * nb_user * params.k()) {
				break;
			}
		}

//		System.out.println(scanrateCounter.get() + " comparisons in total at the end.");
//		System.out.println((dataset.getUsers().size() * (dataset.getUsers().size() - 1))/2 + " comparisons at max:");
//		System.out.println("scanrate: " + (scanrateCounter.get()/((long) (dataset.getUsers().size() * (dataset.getUsers().size() - 1))/2) ) );

		//Converting B of type Hashtable<Integer, HeapItemRatingPair> to Hashtable<Integer, int[]>
//		KNNGraph knng = new KNNGraph();
//		KNNGraph knng = new KNNGraph(params.k(),params.dataset().getUsers().size(),params.dataset().getInitValue());
//		for (int user: B.get_users()) {
//			knng.put(user, B.get_KNN(user).toKNN());
//		}
		KNNGraph knng = B.toKNNGraph();
		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}
		
		return knng;
	}

	
	
	
	
	
	
	
	
	@Override
	public KNNGraph doKNN(KNNGraph anng) {
		
		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
//		AtomicLong scanrateCounter = new AtomicLong();
		
		KNNGraphFlags B = new KNNGraphFlags();


		Thread[] threads = new Thread[params.nb_proc()];
		Counter[] counters = new Counter[params.nb_proc()];
		Counter[] counters_scanrate = new Counter[params.nb_proc()];

		int nb_user = params.dataset().getUsers().size();
//		Object[] users_aux = params.dataset().getUsers().toArray();
		Object[] users_aux = anng.get_users().toArray();

		//DEF OF USERS, used to do random sampling.
		int[] users = new int[nb_user];
		for(int index = 0; index < nb_user; index++) {
			users[index] = (int) users_aux[index];
		}

		//INIT OF B
		
		for(int i = 0; i < params.nb_proc(); i++) {
//			Thread t = new Thread(new NNDescentRunnableSamplingInitANN(params, paramStats, users, anng, B, i));
//			t.start();
//			threads[i] = t;
			counters[i] = new Counter();
			if(ParametersStats.measure_scanrate) {
				counters_scanrate[i] = new Counter();
				threads[i] = new Thread(new NNDescentRunnableSamplingInitANNScan(params, users, anng, B, counters_scanrate[i], i));
			}
			else {
				threads[i] = new Thread(new NNDescentRunnableSamplingInitANN(params, users, anng, B, i));
			}
			threads[i].start();
		}
		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		//MAIN LOOP
		if(ParametersStats.measure_scanrate) {
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters_scanrate[i].getValue();
			}
			paramStats.increase_scanrate(scanrate);
			paramStats.add_iter();
		}
		Map<Integer, Set<Integer>> oldneighbors;
		Map<Integer, Set<Integer>> newneighbors;
		Map<Integer, Set<Integer>> oldneighbors_;
		Map<Integer, Set<Integer>> newneighbors_;
		int counter;
		for (int iter = 0; iter < params.iterations(); iter++) {
			oldneighbors = new ConcurrentHashMap<Integer, Set<Integer>>();
			newneighbors = new ConcurrentHashMap<Integer, Set<Integer>>();

			for(int i = 0; i < params.nb_proc(); i++) {
//				Thread t = new Thread(new NNDescentRunnableOldNew(params, users, B, newneighbors, oldneighbors, i));
//				t.start();
//				threads[i] = t;
				counters_scanrate[i].init();
				threads[i] = new Thread(new NNDescentRunnableOldNew(params, users, B, newneighbors, oldneighbors, i));
				threads[i].start();
			}
			for(int i = 0; i < params.nb_proc(); i++) {
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}


			oldneighbors_ = reverse(oldneighbors);
			newneighbors_ = reverse(newneighbors);

			for(int i = 0; i < params.nb_proc(); i++) {
//				Counter counter = new Counter();
//				Thread t = new Thread(new NNDescentRunnable(params, paramStats, users, B, newneighbors, oldneighbors, newneighbors_, oldneighbors_, counter, i));
//				counters[i] = counter;
//				t.start();
//				threads[i] = t;
				counters[i].init();
				if(ParametersStats.measure_scanrate) {
					counters_scanrate[i].init();
					threads[i] = new Thread(new NNDescentRunnableScan(params, users, B, newneighbors, oldneighbors, newneighbors_, oldneighbors_, counters[i], counters_scanrate[i], i));
				}
				else {
					threads[i] = new Thread(new NNDescentRunnable(params, users, B, newneighbors, oldneighbors, newneighbors_, oldneighbors_, counters[i], i));
				}
				threads[i].start();
			}
			for(int i = 0; i < params.nb_proc(); i++) {
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			counter = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				counter = counter + counters[i].getValue();
			}
			if(ParametersStats.measure_scanrate) {
				long scanrate = 0;
				for(int i = 0; i < params.nb_proc(); i++) {
					scanrate = scanrate + counters_scanrate[i].getValue();
				}
				paramStats.increase_scanrate(scanrate);
				paramStats.add_iter();
				paramStats.add_iter_changes(counter);
			}
			if (counter < params.delta() * nb_user * params.k()) {
				break;
			}
			
		}

		//Converting B of type Hashtable<Integer, HeapItemRatingPair> to Hashtable<Integer, int[]>
//		KNNGraph knng = new KNNGraph();
//		KNNGraph knng = new KNNGraph(params.k(),params.dataset().getUsers().size(),params.dataset().getInitValue());
//		for (int user: B.get_users()) {
//			knng.put(user, B.get_KNN(user).toKNN());
//		}
		KNNGraph knng = B.toKNNGraph();
		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}
		return knng;
	}







	private static Map<Integer, Set<Integer>> reverse (Map<Integer, Set<Integer>> knng) {
		Map<Integer, Set<Integer>> reverseknn = new ConcurrentHashMap<Integer, Set<Integer>>();
		for(int user: knng.keySet()) {
			//			IntDoubleBoolTriplet[] neighbors = knn.get(key).toArray();
			for(int neighbor: knng.get(user)) {
				if (reverseknn.get(neighbor) == null) {
					reverseknn.put(neighbor, new HashSet<Integer>());
				}
//				reverseknn.get(neighbor).add(new IntDoubleBoolTriplet(key,neighbor.score,neighbor.bool));
				reverseknn.get(neighbor).add(user);
			}
		}
		return reverseknn;
	}






//	public void SetParameters(Parameters params) {
//		this.params = (ParametersNNDescent) params;
//	}

	@Override
	public Parameters GetParameters() {
		return params;
	}

	@Override
	public ParametersStats GetStatsParameters() {
		return paramStats;
	}

}
