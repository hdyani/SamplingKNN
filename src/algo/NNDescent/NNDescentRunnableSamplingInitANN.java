package algo.NNDescent;

import parameters.ParametersNNDescent;
//import util.IntDoubleBoolTriplet;
//import util.KNNFlags;
import util.KNNGraph;
import util.KNNGraphFlags;

public final class NNDescentRunnableSamplingInitANN implements Runnable{

	private final ParametersNNDescent params;
	private final int[] users;
	private final KNNGraph anng;
	private final KNNGraphFlags B;
	private final int loop;
	
//	AtomicLong scanrateCounter;
	
	public NNDescentRunnableSamplingInitANN(ParametersNNDescent params, int[] users, KNNGraph anng, KNNGraphFlags B, int loop) {
		this.params = params;
		this.users = users;
		this.anng = anng;
		this.B = B;
		this.loop = loop;
	}
	
	
	@Override
	public void run() {
		

		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, params.nb_proc(), loop);


//		KNNFlags neighbors;
		for(int user: user_ids) {
			B.init(user, params.k(), params.dataset().getInitValue()-1);
//			neighbors = new KNNFlags(params.k(), params.dataset().getInitValue());
			for(int neighbor: anng.get_neighbors_ids(user)) {
//				neighbors.add(new IntDoubleBoolTriplet(neighbor, params.dataset().sim(user, neighbor),true));
				B.put_AS(user,neighbor, params.dataset().sim(user, neighbor),true);
			}
//			B.put(user, neighbors);
		}
	}

}
