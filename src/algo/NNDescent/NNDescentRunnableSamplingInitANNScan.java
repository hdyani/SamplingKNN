package algo.NNDescent;

import parameters.ParametersNNDescent;
import util.Counter;
//import util.IntDoubleBoolTriplet;
//import util.KNNFlags;
import util.KNNGraph;
import util.KNNGraphFlags;

public final class NNDescentRunnableSamplingInitANNScan implements Runnable{

	private final ParametersNNDescent params;
	private final int[] users;
	private final KNNGraph anng;
	private final KNNGraphFlags B;
	private final int loop;
	private final Counter counter_scanrate;
	
//	AtomicLong scanrateCounter;
	
	public NNDescentRunnableSamplingInitANNScan(ParametersNNDescent params, int[] users, KNNGraph anng, KNNGraphFlags B, Counter counter_scanrate, int loop) {
		this.params = params;
		this.users = users;
		this.anng = anng;
		this.B = B;
		this.loop = loop;
		this.counter_scanrate = counter_scanrate;
	}
	
	
	@Override
	public void run() {
		

		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, params.nb_proc(), loop);


//		KNNFlags neighbors;
		int count_scanrate = 0;
		for(int user: user_ids) {
			B.init(user, params.k(), params.dataset().getInitValue()-1);
//			neighbors = new KNNFlags(params.k(), params.dataset().getInitValue());
			for(int neighbor: anng.get_neighbors_ids(user)) {
//				neighbors.add(new IntDoubleBoolTriplet(neighbor, params.dataset().sim(user, neighbor),true));

				B.put_AS(user,neighbor, params.dataset().sim(user, neighbor),true);
//				if(ParametersStats.measure_scanrate) {
//					paramStats.inc();
//				}
				count_scanrate++;
			}
//			B.put(user, neighbors);
		}
//		if(ParametersStats.measure_scanrate) {
		counter_scanrate.inc(count_scanrate);
//		}
		
	}

}
