package algo.NNDescent;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import parameters.ParametersNNDescent;
import util.Counter;
//import util.IntDoubleBoolTriplet;
//import util.KNNFlags;
import util.KNNGraphFlags;

public final class NNDescentRunnableSamplingInitScan implements Runnable{

	private final ParametersNNDescent params;
	
	private final int[] users;
	private final KNNGraphFlags B;
	private final int loop;
	private final Counter counter_scanrate;
	
//	AtomicLong scanrateCounter;
	
	public NNDescentRunnableSamplingInitScan(ParametersNNDescent params, int[] users, KNNGraphFlags B, Counter counter_scanrate, int loop) {
		this.params = params;
		this.users = users;
		this.B = B;
		this.loop = loop;
		this.counter_scanrate = counter_scanrate;
//		this.scanrateCounter = atomcounter;
	}
	
	
	@Override
	public void run() {

		Random randomGenerator = new Random();

		int nb_user = users.length;
		

		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, params.nb_proc(), loop);


		Set<Integer> random_neighbors;
		int random_user;
//		KNNFlags neighbors;
		int count_scanrate = 0;
		for(int user: user_ids) {
			random_neighbors = new HashSet<Integer>();
			while (random_neighbors.size() != params.k()) {
				random_user = users[randomGenerator.nextInt(nb_user)];
				while(random_user == user) {
					random_user = users[randomGenerator.nextInt(nb_user)];
				}
				random_neighbors.add(random_user);
			}
			B.init(user, params.k(), params.dataset().getInitValue()-1);
//			neighbors = new KNNFlags(params.k(), params.dataset().getInitValue());
			for(int random_user_id: random_neighbors) {
				B.put_AS(user,random_user_id, params.dataset().sim(user, random_user_id),true);
//				neighbors.add(new IntDoubleBoolTriplet(random_user_id, params.dataset().sim(user, random_user_id),true));

//				if(ParametersStats.measure_scanrate) {
//					paramStats.inc();
//				}
				count_scanrate++;
				
//				scanrateCounter.incrementAndGet();
			}
//			B.put(user, neighbors);
		}
//		if(ParametersStats.measure_scanrate) {
		counter_scanrate.inc(count_scanrate);
//		}
		
	}

}
