package algo.Hybrid_MH_TP;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.PriorityBlockingQueue;

import algo.Hyrec.HyrecRunnable;
import algo.Hyrec.HyrecRunnableSamplingInit;
//import algo.Hyrec.HyrecRunnableScan;
import dataset.Dataset;
import parameters.ParametersHybrid_MH;
import parameters.ParametersHyrec;
import util.Counter;
import util.CounterLong;
import util.IntIntPair;
import util.KNN;
import util.KNNGraph;

public final class HybridRunnableScan implements Runnable{


	private final ParametersHybrid_MH params;
//	private final int loop;
	private final Counter counter_scanrate;
	private final CounterLong counter_time;
	private final Dataset dataset;
	
	private final LinkedList<KNNGraph> knngs;
//	private final int nb_hash;
//	private final int nb_bits;
//	private final Set<Integer>[][] buckets;
//	private final LinkedList<Set<Integer>> knn_to_compute;
	private final HashMap<Long,ConcurrentLinkedQueue<IntIntPair>> nbSimToID;
	private final PriorityBlockingQueue<Long> nextClusterNbSim;
	private final HashSet<Integer>[][] buckets;
	
	
//	public HybridRunnableScan(ParametersHybrid params, LinkedList<KNNGraph> knngs, Counter counter, int loop, Set<Integer>[][] buckets, Dataset dataset, int nb_hash, int nb_bits) {
//	public HybridRunnableScan(ParametersHybrid_MH params, LinkedList<KNNGraph> knngs, Counter counter, CounterLong counter_time, LinkedList<Set<Integer>> knn_to_compute, Dataset dataset) {
	public HybridRunnableScan(ParametersHybrid_MH params, LinkedList<KNNGraph> knngs, Counter counter, CounterLong counter_time, HashMap<Long,ConcurrentLinkedQueue<IntIntPair>> nbSimToID, PriorityBlockingQueue<Long> nextClusterNbSim, HashSet<Integer>[][] buckets, Dataset dataset) {
		this.knngs = knngs;
		this.params = params;
//		this.loop = loop;
		this.counter_scanrate = counter;
		this.counter_time = counter_time;
//		this.buckets = buckets;
//		this.knn_to_compute = knn_to_compute;
		this.nbSimToID=nbSimToID;
		this.nextClusterNbSim=nextClusterNbSim;
		this.buckets=buckets;
		this.dataset = dataset;
//		this.nb_bits = nb_bits;
//		this.nb_hash = nb_hash;
	}


	@Override
	public void run() {

		long start=System.currentTimeMillis();
		
//		int nb_knng_computed = 0;
//		int aux;
//		for(int i=0; i<loop; i++) {
//			aux =(nb_bits * nb_hash) / params.nb_proc();
//			if ((params.nb_proc() % params.nb_proc()) > (params.nb_proc()  - (i+1))) {
//				aux++;
//			}
//			nb_knng_computed = nb_knng_computed + aux;
//		}
//		
//		int nb_knng_to_compute  = (nb_bits * nb_hash) / params.nb_proc();
//		if((params.nb_proc() % params.nb_proc()) > (params.nb_proc()  - (loop+1))) {
//			nb_knng_to_compute++;
//		}
//		int index_i;
//		int index_j;
		
//		Set<Integer> user_aux;
		Integer[] users_;
		int[] users;
		int index;
		
		Dataset local_dataset;
		KNNGraph knng;

		double sim;
		
		Long nbSimToCompute;
		IntIntPair iip;
		Set<Integer> user_aux;
		

//		int count;

//		for (int iter=0; iter < nb_knng_to_compute; iter++) {
//		for (Set<Integer> user_aux: knn_to_compute) {
		while ((nbSimToCompute=nextClusterNbSim.poll())!=null) {

			iip=(nbSimToID.get(nbSimToCompute)).poll();
			if (iip==null) {
				break;
			}

			user_aux=buckets[iip.first_element][iip.second_element];
			
			//Setting the indexes of the bucket
//			index_i = nb_knng_computed / params.nb_bits();
//			index_j = nb_knng_computed % params.nb_bits();
//			nb_knng_computed++;

			
			
			//Getting the corresponding users
//			user_aux = buckets[index_i][index_j];
			users_ = new Integer[user_aux.size()];
			users = new int[user_aux.size()];
			user_aux.toArray(users_);
			index = 0;
			for(int user: users_) {
				users[index] = user;
				index++;
			}
			
			//Creating the corresponding KNNGraph
			knng = new KNNGraph(params.k(),users.length,params.dataset().getInitValue());
			
			//Getting the corresponding dataset => needed?
			local_dataset = dataset.subDataset(users);

			//If 5 * k * k < n then perform Hyrec, else perform BruteForce

//			count=0;
			if(5*params.k()*params.k()<users.length) {
//			if(10*params.k()*params.k()<users.length) {
				//HYREC
				ParametersHyrec params_ = new ParametersHyrec(params.k(), params.ParametersToMap().get("Name"), local_dataset, 1);
				params_.set_r(0);
				params_.set_iterations(30);
				params_.set_delta(0.001);
				KNNGraph old_knng = new KNNGraph(params_.k(),users.length,local_dataset.getInitValue()-1);
				HyrecRunnableSamplingInit hyrecInit = new HyrecRunnableSamplingInit(params_, users, knng, old_knng, 0);
				hyrecInit.run();

				
				Counter counter = new Counter();
//				Counter counter_scanrate = new Counter();
				for (int iter_hyrec = 0; iter_hyrec < params_.iterations(); iter_hyrec++) {
					counter.init();
//					counter_scanrate.init();
//					HyrecRunnableScan hyrecRun = new HyrecRunnableScan(params_, users, knng, old_knng, counter, counter_scanrate, 0);
					HyrecRunnable hyrecRun = new HyrecRunnable(params_, users, knng, old_knng, counter, 0);
					hyrecRun.run();
					

//					
//					paramStats.increase_scanrate(counter_scanrate.getValue());
//					paramStats.add_iter();
//					paramStats.add_iter_changes(counter.getValue());
					
					counter_scanrate.inc(counter.getValue());
					if(counter.getValue() < params_.delta() * params_.k() * users.length) {
						break;
					}
					old_knng = knng.clone();
				}
			}
			else { //BRUTE FORCE
				for(int user_id: users) {
					knng.put(user_id, new KNN(params.k(), params.dataset().getInitValue()-1));
				}

				for(int user_id: users) {
					for(int user: users) {
						if(user < user_id) {
							sim = local_dataset.sim(user_id, user);
							knng.put(user_id,user,sim);
							knng.put(user, user_id,sim);
							counter_scanrate.inc();
						}
					}
				}
//				knngs.add(knng);
			}
//			count=0;
//			for(int user_id: users) {
//				for(int user: users) {
//					if(user < user_id) {
//						sim = local_dataset.sim(user_id, user);
////						sim = dataset.sim(user_id, user);
//						knng.put(user_id,user,sim);
//						knng.put(user, user_id,sim);
//						count++;
//					}
//				}
//			}
			knngs.add(knng);
//			counter.inc(count);
		}

//		long end = System.currentTimeMillis();
//		System.out.println(end-start);
//		counter_time.inc(end-start);
		counter_time.inc(System.currentTimeMillis()-start);
//		counter.inc(count);
//		System.out.println("proc: " + loop + " total users: " + users.length + " total comp: " + ((users.length * (users.length-1))/2) + " local users: " + user_ids.length + " lotal comp: " + count);
	}

}
