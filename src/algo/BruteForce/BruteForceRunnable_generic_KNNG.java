package algo.BruteForce;

import parameters.ParametersBruteForce;
import util.KNNG;

public final class BruteForceRunnable_generic_KNNG implements Runnable{

	private final KNNG knng;
	private final int[] users;
	private final ParametersBruteForce params;
	private final int loop;
	
	
	public BruteForceRunnable_generic_KNNG(ParametersBruteForce params, KNNG knng, int[] users, int loop) {
		this.knng = knng;
		this.users = users;
		this.params = params;
		this.loop = loop;
	}


	@Override
	public void run() {

//		int[] total_users = util.SetToArray.setToArray(params.dataset().getUsers());
		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, params.nb_proc(), loop);
		
		double sim;
		for(int user_id: user_ids) {
			for(int user: users) {
				if(user > user_id) {
					sim = params.dataset().sim(user_id, user);

//					if(ParametersStats.measure_scanrate) {
//						paramStats.inc();
//					}
					
					knng.put(user_id,user,sim);
					knng.put(user, user_id,sim);
//					knng.get(user_id).add(new IntDoublePair(user,sim));
//					knng.get(user).add(new IntDoublePair(user_id,sim));
				}
			}
		}
//		counter.inc(count);
//		System.out.println("proc: " + loop + " total users: " + users.length + " total comp: " + ((users.length * (users.length-1))/2) + " local users: " + user_ids.length + " lotal comp: " + count);
	}

}
