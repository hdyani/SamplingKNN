package algo.BruteForce;

import parameters.ParametersBruteForce;
import util.Counter;
import util.KNNGraph;

public final class BruteForceRunnableScan implements Runnable{

	private final KNNGraph knng;
	private final int[] users;
	private final ParametersBruteForce params;
	private final int loop;
	private final Counter counter;
	
	
	public BruteForceRunnableScan(ParametersBruteForce params, KNNGraph knng, int[] users, Counter counter, int loop) {
		this.knng = knng;
		this.users = users;
		this.params = params;
		this.loop = loop;
		this.counter = counter;
	}


	@Override
	public void run() {

//		int[] total_users = util.SetToArray.setToArray(params.dataset().getUsers());
		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, params.nb_proc(), loop);
		
		int count=0;
		double sim;
		for(int user_id: user_ids) {
			for(int user: users) {
				if(user > user_id) {
					sim = params.dataset().sim(user_id, user);

//					if(ParametersStats.measure_scanrate) {
//						paramStats.inc();
//					}
					
					knng.put(user_id,user,sim);
					knng.put(user, user_id,sim);
					count++;
//					knng.get(user_id).add(new IntDoublePair(user,sim));
//					knng.get(user).add(new IntDoublePair(user_id,sim));
				}
			}
		}
//			paramStats.inc();
		counter.inc(count);
		
//		counter.inc(count);
//		System.out.println("proc: " + loop + " total users: " + users.length + " total comp: " + ((users.length * (users.length-1))/2) + " local users: " + user_ids.length + " lotal comp: " + count);
	}

}
