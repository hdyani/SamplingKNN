package algo.BruteForce;

import java.util.Set;

import algo.Algo;
import parameters.Parameters;
import parameters.ParametersBruteForce;
import parameters.ParametersStats;
import util.Counter;
import util.KNN;
import util.KNNGraph;

public final class BruteForce implements Algo {

	private final ParametersBruteForce params;
	private final ParametersStats paramStats;
	
	public BruteForce(ParametersBruteForce params, ParametersStats paramStats) {
		this.params = params;
		this.paramStats = paramStats;
	}

	@Override
	public KNNGraph doKNN(){
		
		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		
		
		
//		KNNGraph knng = new KNNGraph();
		KNNGraph knng = new KNNGraph(params.k(),params.dataset().getUsers().size(),params.dataset().getInitValue());
		Set<Integer> user_aux = params.dataset().getUsers();
		Integer[] users_ = new Integer[user_aux.size()];
		int[] users = new int[user_aux.size()];
		user_aux.toArray(users_);
		Thread[] threads = new Thread[params.nb_proc()];
		Counter[] counters = new Counter[params.nb_proc()];
		
		
		
		int index = 0;
		for(int user: users_) {
			knng.put(user, new KNN(params.k(), params.dataset().getInitValue()-1));
			users[index] = user;
			index++;
		}
		
		
		for(int i = 0; i < params.nb_proc(); i++) {
			if(ParametersStats.measure_scanrate) {
				counters[i] = new Counter();
				threads[i] = new Thread(new BruteForceRunnableScan(params, knng, users, counters[i], i));
			}
			else {
				threads[i] = new Thread(new BruteForceRunnable(params, knng, users, i));
			}
			threads[i].start();
		}

		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}
		
		if(ParametersStats.measure_scanrate) {
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters[i].getValue();
			}
			paramStats.increase_scanrate(scanrate);
		}

		return knng;
	}

	
	/*
	 * doKNN(int[] users) will do the KNN of each user in users, using the users of ALL the dataset
	 * */
	
	@Override
	public KNNGraph doKNN(int[] users){
		
		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		
		
		
//		KNNGraph knng = new KNNGraph();
		KNNGraph knng = new KNNGraph(params.k(),params.dataset().getUsers().size(),params.dataset().getInitValue());
//		Set<Integer> user_aux = params.dataset().getUsers();
//		Integer[] users_ = new Integer[user_aux.size()];
//		int[] users = new int[user_aux.size()];
//		user_aux.toArray(users_);
		Thread[] threads = new Thread[params.nb_proc()];
		Counter[] counters = new Counter[params.nb_proc()];

		
		
		
		int index = 0;
		for(int user: users) {
			knng.put(user, new KNN(params.k(), params.dataset().getInitValue()-1));
			users[index] = user;
			index++;
		}
		
		
		for(int i = 0; i < params.nb_proc(); i++) {
			if(ParametersStats.measure_scanrate) {
				counters[i] = new Counter();
				threads[i] = new Thread(new BruteForceRunnableScan(params, knng, users, counters[i], i));
			}
			else {
				threads[i] = new Thread(new BruteForceRunnable(params, knng, users, i));
			}
			threads[i].start();
		}

		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}
		
		if(ParametersStats.measure_scanrate) {
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters[i].getValue();
			}
			paramStats.increase_scanrate(scanrate);
		}
		
//		if(ParametersStats.scanrate) {
//			paramStats.increase_scanrate();
//		}

		return knng;
	}

	
	
	
	
	
	
	
	@Override
	public KNNGraph doKNN(KNNGraph anng) {
		
		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		
		
		
//		KNNGraph knng = new KNNGraph();
		KNNGraph knng = new KNNGraph(params.k(),params.dataset().getUsers().size(),params.dataset().getInitValue());
//		Set<Integer> user_aux = params.dataset().getUsers();
		Set<Integer> user_aux = anng.get_users();
		int[] users = new int[user_aux.size()];
		Thread[] threads = new Thread[params.nb_proc()];
		Counter[] counters = new Counter[params.nb_proc()];

		
		
		
		int index = 0;
		for(int user: user_aux) {
			knng.put(user, new KNN(params.k(), params.dataset().getInitValue()-1));
			users[index] = user;
			index++;
		}
		
		
		for(int i = 0; i < params.nb_proc(); i++) {
//			Thread t = new Thread(new BruteForceRunnableANN(params, paramStats, knng, anng, users, i));
//			t.start();
//			threads[i] = t;
//			counters[i] = new Counter();
			if(ParametersStats.measure_scanrate) {
				counters[i] = new Counter();
				threads[i] = new Thread(new BruteForceRunnableANNScan(params, knng, anng, users, counters[i], i));
			}
			else {
				threads[i] = new Thread(new BruteForceRunnableANN(params, knng, anng, users, i));
			}
//			threads[i] = new Thread(new BruteForceRunnableANN(params, knng, anng, users, counters[i], i));
			threads[i].start();
		}

		
		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}
		
		if(ParametersStats.measure_scanrate) {
			long scanrate = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				scanrate = scanrate + counters[i].getValue();
			}
			paramStats.increase_scanrate(scanrate);
		}
		

		return knng;
	}



//	public void SetParameters(Parameters params) {
//		this.params = (ParametersBruteForce) params;
//	}

	@Override
	public Parameters GetParameters() {
		return params;
	}

	@Override
	public ParametersStats GetStatsParameters() {
		return paramStats;
	}
}
