package algo.HyrecFlagsCork;

import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import parameters.ParametersHyrecCork;
import parameters.ParametersStats;
import util.Counter;
import util.IntDoublePair;
import util.KNN;
import util.KNNGraphFlags;

public final class HyrecFlagsCorkRunnable implements Runnable{

	private final ParametersHyrecCork params;
	private final ParametersStats paramStats;
	private final KNNGraphFlags knng;
	private final Map<Integer, Set<Integer>> newneighbors;
	private final Map<Integer, Set<Integer>> oldneighbors;
	private final int loop;
	private final int[] users;
	private final Counter counter;



	public HyrecFlagsCorkRunnable(ParametersHyrecCork params, ParametersStats paramStats, int[] users, KNNGraphFlags knng, Map<Integer, Set<Integer>> newneighbors, Map<Integer, Set<Integer>> oldneighbors, Counter counter, int loop) {
		this.users = users;
		this.knng = knng;
		this.newneighbors = newneighbors;
		this.oldneighbors = oldneighbors;
		this.loop = loop;
		this.counter = counter;
		this.params = params;
		this.paramStats = paramStats;
	}


	@Override
	public void run() {
		Random randomGenerator = new Random();
		int nb_user = users.length;

		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, params.nb_proc(), loop);

		
		Set<Integer> candidates;
		int random_user;
		int B;
		KNN knn_approx;
		for(int user1 : user_ids) {
			candidates = new HashSet<Integer>();
			//Adding random neighbors.
			if(params.r()>0){
				for(int index = 0; index < params.r(); index++) {
					random_user = users[randomGenerator.nextInt(nb_user)];
					while(random_user == user1) { //really userful ?
						random_user = users[randomGenerator.nextInt(nb_user)];
					}
					candidates.add(random_user);
				}
			}


			for(int user2: newneighbors.get(user1)) {
				for(int user3: newneighbors.getOrDefault(user2, new HashSet<Integer>())) {
					candidates.add(user3);
				}
				for(int user3: oldneighbors.getOrDefault(user2, new HashSet<Integer>())) {
					candidates.add(user3);
				}
			}
			for(int user2: oldneighbors.get(user1)) {
				for(int user3: newneighbors.getOrDefault(user2, new HashSet<Integer>())) {
					candidates.add(user3);
				}
			}

			B = Math.min(params.B(), params.k() * params.k());
			
			knn_approx = new KNN(B,params.dataset_approx().getInitValue());
			for(int candidate: candidates) {
				if(candidate != -1) {
//					double sim = params.dataset_approx().sim(user1, candidate);
					if(ParametersStats.measure_scanrate) {
						paramStats.inc();
					}
//					IntDoublePair idp = new IntDoublePair(candidate,sim);
//					knn_approx.add(idp);
					knn_approx.add(new IntDoublePair(candidate,params.dataset_approx().sim(user1, candidate)));
				}
			}
			
			
			
			
			for(IntDoublePair candidate_idp: knn_approx.toArray()) {
				if(candidate_idp.integer != -1) {
//					double sim = params.dataset_exact().sim(user1, candidate_idp.integer);
					if(ParametersStats.measure_scanrate) {
						paramStats.inc();
					}
//					IntDoubleBoolTriplet idp = new IntDoubleBoolTriplet(candidate_idp.integer,sim,true);
//					boolean b = knng.put(user1,idp);
//					if(b) {
					if(knng.put(user1,candidate_idp.integer,params.dataset_exact().sim(user1, candidate_idp.integer),true)) {
						counter.inc();
					}
				}
			}
		}



	}

}
