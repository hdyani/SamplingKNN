package algo.HyrecFlagsCork;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import algo.Algo;
import parameters.Parameters;
import parameters.ParametersHyrecCork;
import parameters.ParametersStats;
import util.Counter;
import util.KNNGraph;
import util.KNNGraphFlags;

public final class HyrecFlagsCork implements Algo{
	
	private final ParametersHyrecCork params;
	private final ParametersStats paramStats;
	
	public HyrecFlagsCork(ParametersHyrecCork params, ParametersStats paramStats) {
		this.params = params;
		this.paramStats = paramStats;
	}

	@Override
	public KNNGraph doKNN() {
		
		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		
		Thread[] threads = new Thread[params.nb_proc()];
		Counter[] counters = new Counter[params.nb_proc()];
		KNNGraphFlags knng = new KNNGraphFlags();

		int nb_user = params.dataset_exact().getUsers().size();

		Object[] users_aux = params.dataset_exact().getUsers().toArray();
		
		int[] users = new int[nb_user];
		for(int index = 0; index < nb_user; index++) {
			users[index] = (int) users_aux[index];
		}
		
		
		for(int i = 0; i < params.nb_proc(); i++) {
//			Thread t = new Thread(new HyrecFlagsCorkRunnableSamplingInit(params, paramStats, users, knng, i));
//			t.start();
//			threads[i] = t;
			threads[i] = new Thread(new HyrecFlagsCorkRunnableSamplingInit(params, paramStats, users, knng, i));
			threads[i].start();
		}

		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if(ParametersStats.measure_scanrate) {
			paramStats.add_iter();
		}
		
		Map<Integer,Set<Integer>> new_neighbors;
		Map<Integer,Set<Integer>> old_neighbors;
		int nb_changes;
		for (int iter = 0; iter < params.iterations(); iter++) {
			
			new_neighbors = new ConcurrentHashMap<Integer,Set<Integer>>();
			old_neighbors = new ConcurrentHashMap<Integer,Set<Integer>>();
			for(int i = 0; i < params.nb_proc(); i++) {
//				Thread t = new Thread(new HyrecFlagsCorkRunnableOldNew(params, users, knng, new_neighbors, old_neighbors, i));
//				t.start();
//				threads[i] = t;
				threads[i] = new Thread(new HyrecFlagsCorkRunnableOldNew(params, users, knng, new_neighbors, old_neighbors, i));
				threads[i].start();
			}
			for(int i = 0; i < params.nb_proc(); i++) {
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			

			for(int i = 0; i < params.nb_proc(); i++) {
//				Counter counter = new Counter();
//				Thread t = new Thread(new HyrecFlagsCorkRunnable(params, paramStats, users, knng, new_neighbors, old_neighbors, counter, i));
//				t.start();
//				threads[i] = t;
//				counters[i] = counter;
				counters[i] = new Counter();
				threads[i] = new Thread(new HyrecFlagsCorkRunnable(params, paramStats, users, knng, new_neighbors, old_neighbors, counters[i], i));
				threads[i].start();
			}
			for(int i = 0; i < params.nb_proc(); i++) {
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			nb_changes = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				nb_changes = nb_changes + counters[i].getValue();
			}


			if(ParametersStats.measure_scanrate) {
				paramStats.add_iter();
				paramStats.add_iter_changes(nb_changes);
			}
			if(nb_changes < params.delta() * params.k() * nb_user) {
				break;
			}
			

		}
		
		KNNGraph result = knng.toKNNGraph();
		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}
		
		return result;
	}

	/*
	 * doKNN(int[] users) will do the KNN of the user in users, using ONLY the users of in users
	 * */
	@Override
	public KNNGraph doKNN(int[] users) {
		
		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}
		
		Thread[] threads = new Thread[params.nb_proc()];
		Counter[] counters = new Counter[params.nb_proc()];
		KNNGraphFlags knng = new KNNGraphFlags();

		int nb_user = params.dataset_exact().getUsers().size();

//		Object[] users_aux = params.dataset().getUsers().toArray();
		
//		int[] users = new int[nb_user];
//		for(int index = 0; index < nb_user; index++) {
//			users[index] = (int) users_aux[index];
//		}
		
		
		for(int i = 0; i < params.nb_proc(); i++) {
//			Thread t = new Thread(new HyrecFlagsCorkRunnableSamplingInit(params, paramStats, users, knng, i));
//			t.start();
//			threads[i] = t;
			threads[i] = new Thread(new HyrecFlagsCorkRunnableSamplingInit(params, paramStats, users, knng, i));
			threads[i].start();
		}

		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if(ParametersStats.measure_scanrate) {
			paramStats.add_iter();
		}
		
		Map<Integer,Set<Integer>> new_neighbors;
		Map<Integer,Set<Integer>> old_neighbors;
		int nb_changes;
		for (int iter = 0; iter < params.iterations(); iter++) {
			
			new_neighbors = new ConcurrentHashMap<Integer,Set<Integer>>();
			old_neighbors = new ConcurrentHashMap<Integer,Set<Integer>>();
			for(int i = 0; i < params.nb_proc(); i++) {
//				Thread t = new Thread(new HyrecFlagsCorkRunnableOldNew(params, users, knng, new_neighbors, old_neighbors, i));
//				t.start();
//				threads[i] = t;
				threads[i] = new Thread(new HyrecFlagsCorkRunnableOldNew(params, users, knng, new_neighbors, old_neighbors, i));
				threads[i].start();
			}
			for(int i = 0; i < params.nb_proc(); i++) {
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			

			for(int i = 0; i < params.nb_proc(); i++) {
//				Counter counter = new Counter();
//				Thread t = new Thread(new HyrecFlagsCorkRunnable(params, paramStats, users, knng, new_neighbors, old_neighbors, counter, i));
//				t.start();
//				threads[i] = t;
//				counters[i] = counter;
				counters[i] = new Counter();
				threads[i] = new Thread(new HyrecFlagsCorkRunnable(params, paramStats, users, knng, new_neighbors, old_neighbors, counters[i], i));
				threads[i].start();
			}
			for(int i = 0; i < params.nb_proc(); i++) {
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			nb_changes = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				nb_changes = nb_changes + counters[i].getValue();
			}


			if(ParametersStats.measure_scanrate) {
				paramStats.add_iter();
				paramStats.add_iter_changes(nb_changes);
			}
			if(nb_changes < params.delta() * params.k() * nb_user) {
				break;
			}
			

		}
		
		KNNGraph result = knng.toKNNGraph();
		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}
		
		return result;
	}
	
	
	
	
	
	

	@Override
	public KNNGraph doKNN(KNNGraph ann) {
		
		long start = 0;
		if(ParametersStats.measure_time) {
			start = System.currentTimeMillis();
		}

		KNNGraphFlags knng = new KNNGraphFlags();
		
		Thread[] threads = new Thread[params.nb_proc()];
		Counter[] counters = new Counter[params.nb_proc()];

		int nb_user = params.dataset_exact().getUsers().size();

//		Object[] users_aux = params.dataset().getUsers().toArray();
		Object[] users_aux = ann.get_users().toArray();
		
		int[] users = new int[nb_user];
		
		for(int index = 0; index < nb_user; index++) {
			users[index] = (int) users_aux[index];
		}
		
		for(int i = 0; i < params.nb_proc(); i++) {
//			Thread t = new Thread(new HyrecFlagsCorkRunnableSamplingInitANN(params, paramStats, users, ann, knng, i));
//			t.start();
//			threads[i] = t;
			threads[i] = new Thread(new HyrecFlagsCorkRunnableSamplingInitANN(params, paramStats, users, ann, knng, i));
			threads[i].start();
		}

		for(int i = 0; i < params.nb_proc(); i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		if(ParametersStats.measure_scanrate) {
			paramStats.add_iter();
		}

		Map<Integer,Set<Integer>> new_neighbors;
		Map<Integer,Set<Integer>> old_neighbors;
		int nb_changes;
		for (int iter = 0; iter < params.iterations(); iter++) {
			
			new_neighbors = new ConcurrentHashMap<Integer,Set<Integer>>();
			old_neighbors = new ConcurrentHashMap<Integer,Set<Integer>>();
			for(int i = 0; i < params.nb_proc(); i++) {
//				Thread t = new Thread(new HyrecFlagsCorkRunnableOldNew(params, users, knng, new_neighbors, old_neighbors, i));
//				t.start();
//				threads[i] = t;
				threads[i] = new Thread(new HyrecFlagsCorkRunnableOldNew(params, users, knng, new_neighbors, old_neighbors, i));
				threads[i].start();
			}
			for(int i = 0; i < params.nb_proc(); i++) {
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			

			for(int i = 0; i < params.nb_proc(); i++) {
//				Counter counter = new Counter();
//				Thread t = new Thread(new HyrecFlagsCorkRunnable(params, paramStats, users, knng, new_neighbors, old_neighbors, counter, i));
//				t.start();
//				threads[i] = t;
//				counters[i] = counter;
				counters[i] = new Counter();
				threads[i] = new Thread(new HyrecFlagsCorkRunnable(params, paramStats, users, knng, new_neighbors, old_neighbors, counters[i], i));
				threads[i].start();
			}
			for(int i = 0; i < params.nb_proc(); i++) {
				try {
					threads[i].join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			nb_changes = 0;
			for(int i = 0; i < params.nb_proc(); i++) {
				nb_changes = nb_changes + counters[i].getValue();
			}
			if(ParametersStats.measure_scanrate) {
				paramStats.add_iter();
				paramStats.add_iter_changes(nb_changes);
			}
			
			if(nb_changes < params.delta() * params.k() * nb_user) {
				break;
			}
		}

		KNNGraph result = knng.toKNNGraph();
		
		if(ParametersStats.measure_time) {
			long end = System.currentTimeMillis();
			paramStats.increase_time(end - start);
		}
		
		return result;
	}

	
//	public void SetParameters(Parameters params) {
//		this.params = (ParametersHyrec) params;
//	}

	@Override
	public Parameters GetParameters() {
		return params;
	}

	@Override
	public ParametersStats GetStatsParameters() {
		return paramStats;
	}

}
