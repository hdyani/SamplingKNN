package algo.HyrecFlagsCork;

import parameters.ParametersHyrecCork;
import parameters.ParametersStats;
//import util.IntDoubleBoolTriplet;
import util.IntDoublePair;
//import util.KNNFlags;
import util.KNNGraph;
import util.KNNGraphFlags;

public final class HyrecFlagsCorkRunnableSamplingInitANN implements Runnable{

	private final int[] users;
	private final KNNGraphFlags knng;
	private final KNNGraph anng;
	private final int loop;
	private final ParametersHyrecCork params;
	private final ParametersStats paramStats;
	
	public HyrecFlagsCorkRunnableSamplingInitANN(ParametersHyrecCork params, ParametersStats paramStats, int[] users, KNNGraph anng, KNNGraphFlags knng, int loop) {
		this.users = users;
		this.anng = anng;
		this.knng = knng;
		this.loop = loop;
		this.params = params;
		this.paramStats = paramStats;
	}
	
	
	@Override
	public void run() {
		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, params.nb_proc(), loop);


//		KNNFlags neighbors;
		for(int user: user_ids) {
			knng.init(user, params.k(), params.dataset_exact().getInitValue()-1);
//			neighbors = new KNNFlags(params.k(), params.dataset_exact().getInitValue());
			for(IntDoublePair neighbor_idp: anng.get_neighbors(user)) {
//				neighbors.add(new IntDoubleBoolTriplet(neighbor_idp.integer, params.dataset_exact().sim(user, neighbor_idp.integer),true));

				knng.put_AS(user,neighbor_idp.integer, params.dataset_exact().sim(user, neighbor_idp.integer),true);
				if(ParametersStats.measure_scanrate) {
					paramStats.inc();
				}
				
			}
//			knng.put(user, neighbors);
		}
		
	}

}
