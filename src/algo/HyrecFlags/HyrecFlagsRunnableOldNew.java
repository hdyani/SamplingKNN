package algo.HyrecFlags;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import parameters.ParametersHyrec;
import util.IntDoubleBoolTriplet;
import util.KNNGraphFlags;

public final class HyrecFlagsRunnableOldNew implements Runnable {

	private final int[] users;
	private final KNNGraphFlags knng;
	private final Map<Integer, Set<Integer>> newneighbors;
	private final Map<Integer, Set<Integer>> oldneighbors;
	private final ParametersHyrec params;
	private final int loop;
	
	
	public HyrecFlagsRunnableOldNew(ParametersHyrec params, int[] users, KNNGraphFlags knng, Map<Integer, Set<Integer>> newneighbors, Map<Integer, Set<Integer>> oldneighbors, int loop) {
		this.users = users;
		this.knng = knng;
		this.newneighbors = newneighbors;
		this.oldneighbors = oldneighbors;
		this.loop = loop;
		this.params = params;
	}
	
	
	@Override
	public void run() {


		int[] user_ids = util.ThreadRepartition.pivot_repartition(users, params.nb_proc(), loop);
		
		
		for(int user: user_ids) {
			oldneighbors.put(user, new HashSet<Integer>());
			newneighbors.put(user, new HashSet<Integer>());
			for(IntDoubleBoolTriplet idbt: knng.get_neighbors(user)){
//			for(IntDoubleBoolTriplet idbt: knng.get_KNN(user).toArray()){
				if(idbt.integer != -1) {
					if(idbt.bool) {
						idbt.bool = false;
						newneighbors.get(user).add(idbt.integer);
					}
					else {
						oldneighbors.get(user).add(idbt.integer);
					}
				}
			}
		}
		
	}

}
